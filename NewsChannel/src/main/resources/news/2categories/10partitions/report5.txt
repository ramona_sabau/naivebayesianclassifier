Confusion matrix
ConfusionMatrixCell{actual='rec.autos, prediction='rec.autos, value=49}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.med, value=8}
ConfusionMatrixCell{actual='sci.med, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=62}

Accuracy
Accuracy = 93.27731

Precision
Precision for target class rec.autos = 0.8596491
Precision for target class sci.med = 1.0

Recall
Recall for target class rec.autos = 1.0
Recall for target class sci.med = 0.8857143

F1 score
F1 score for target class rec.autos = 0.9245283
F1 score for target class sci.med = 0.93939394
