Confusion matrix
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.os.ms-windows.misc, value=68}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.autos, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.sport.baseball, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.med, value=6}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.autos, value=40}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.electronics, value=3}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.med, value=5}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.sport.baseball, value=61}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.os.ms-windows.misc, value=8}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.autos, value=5}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.electronics, value=36}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.med, value=4}
ConfusionMatrixCell{actual='sci.med, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=51}

Accuracy
Accuracy = 86.19528

Precision
Precision for target class comp.os.ms-windows.misc = 0.8607595
Precision for target class rec.autos = 0.8333333
Precision for target class rec.sport.baseball = 0.96825397
Precision for target class sci.electronics = 0.6666667
Precision for target class sci.med = 0.9622642

Recall
Recall for target class comp.os.ms-windows.misc = 0.8831169
Recall for target class rec.autos = 0.8333333
Recall for target class rec.sport.baseball = 0.953125
Recall for target class sci.electronics = 0.8780488
Recall for target class sci.med = 0.76119405

F1 score
F1 score for target class comp.os.ms-windows.misc = 0.8717949
F1 score for target class rec.autos = 0.8333333
F1 score for target class rec.sport.baseball = 0.96062994
F1 score for target class sci.electronics = 0.7578948
F1 score for target class sci.med = 0.85
