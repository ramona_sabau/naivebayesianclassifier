Confusion matrix
ConfusionMatrixCell{actual='alt.atheism, prediction='alt.atheism, value=125}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='alt.atheism, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.crypt, value=4}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.med, value=3}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='soc.religion.christian, value=8}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.politics.mideast, value=2}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.religion.misc, value=10}
ConfusionMatrixCell{actual='comp.graphics, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.graphics, value=128}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.os.ms-windows.misc, value=10}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.sys.ibm.pc.hardware, value=7}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.sys.mac.hardware, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.windows.x, value=18}
ConfusionMatrixCell{actual='comp.graphics, prediction='misc.forsale, value=4}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.sport.hockey, value=3}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.crypt, value=14}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.med, value=4}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.space, value=14}
ConfusionMatrixCell{actual='comp.graphics, prediction='soc.religion.christian, value=4}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.politics.guns, value=4}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.graphics, value=11}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.os.ms-windows.misc, value=138}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.sys.ibm.pc.hardware, value=10}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.sys.mac.hardware, value=4}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.windows.x, value=11}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='misc.forsale, value=4}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.crypt, value=7}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.med, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.space, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.politics.mideast, value=3}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.graphics, value=7}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.os.ms-windows.misc, value=36}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.sys.ibm.pc.hardware, value=100}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.sys.mac.hardware, value=6}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.windows.x, value=5}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='misc.forsale, value=8}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.sport.hockey, value=2}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.crypt, value=8}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.electronics, value=8}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.space, value=4}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.politics.guns, value=5}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.graphics, value=11}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.os.ms-windows.misc, value=7}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.sys.ibm.pc.hardware, value=8}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.sys.mac.hardware, value=113}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.windows.x, value=3}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='misc.forsale, value=6}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.autos, value=2}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.sport.hockey, value=5}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.crypt, value=2}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.space, value=5}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.graphics, value=12}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.os.ms-windows.misc, value=7}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.sys.mac.hardware, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.windows.x, value=146}
ConfusionMatrixCell{actual='comp.windows.x, prediction='misc.forsale, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.autos, value=4}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.crypt, value=8}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.space, value=5}
ConfusionMatrixCell{actual='comp.windows.x, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.graphics, value=11}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.os.ms-windows.misc, value=3}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.sys.ibm.pc.hardware, value=8}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.sys.mac.hardware, value=2}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.windows.x, value=3}
ConfusionMatrixCell{actual='misc.forsale, prediction='misc.forsale, value=97}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.autos, value=7}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.motorcycles, value=3}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.sport.hockey, value=4}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.crypt, value=5}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.electronics, value=24}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.med, value=3}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.space, value=12}
ConfusionMatrixCell{actual='misc.forsale, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.politics.guns, value=6}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.politics.mideast, value=2}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.graphics, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='rec.autos, prediction='misc.forsale, value=4}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.autos, value=167}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.motorcycles, value=8}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.sport.hockey, value=2}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.electronics, value=7}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.med, value=3}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.space, value=5}
ConfusionMatrixCell{actual='rec.autos, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.politics.guns, value=2}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.politics.mideast, value=4}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.graphics, value=2}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.autos, value=6}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.motorcycles, value=177}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.med, value=2}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.space, value=5}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='misc.forsale, value=2}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.sport.baseball, value=170}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.sport.hockey, value=11}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.med, value=9}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.space, value=2}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.politics.misc, value=3}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.os.ms-windows.misc, value=2}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.sys.mac.hardware, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='misc.forsale, value=4}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.sport.baseball, value=9}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.sport.hockey, value=173}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.space, value=4}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.politics.misc, value=3}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.graphics, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.sys.ibm.pc.hardware, value=2}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.windows.x, value=3}
ConfusionMatrixCell{actual='sci.crypt, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.crypt, value=167}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.med, value=2}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='soc.religion.christian, value=2}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.politics.guns, value=4}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.graphics, value=7}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.os.ms-windows.misc, value=5}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.sys.ibm.pc.hardware, value=6}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.sys.mac.hardware, value=4}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.windows.x, value=3}
ConfusionMatrixCell{actual='sci.electronics, prediction='misc.forsale, value=8}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.motorcycles, value=3}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.crypt, value=12}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.electronics, value=128}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.med, value=4}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.space, value=10}
ConfusionMatrixCell{actual='sci.electronics, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.graphics, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='sci.crypt, value=2}
ConfusionMatrixCell{actual='sci.med, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=185}
ConfusionMatrixCell{actual='sci.med, prediction='sci.space, value=3}
ConfusionMatrixCell{actual='sci.med, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='talk.politics.guns, value=2}
ConfusionMatrixCell{actual='sci.med, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='talk.politics.misc, value=3}
ConfusionMatrixCell{actual='sci.med, prediction='talk.religion.misc, value=2}
ConfusionMatrixCell{actual='sci.space, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='comp.graphics, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='misc.forsale, value=2}
ConfusionMatrixCell{actual='sci.space, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='sci.crypt, value=5}
ConfusionMatrixCell{actual='sci.space, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='sci.med, value=2}
ConfusionMatrixCell{actual='sci.space, prediction='sci.space, value=166}
ConfusionMatrixCell{actual='sci.space, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='talk.politics.misc, value=5}
ConfusionMatrixCell{actual='sci.space, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='alt.atheism, value=6}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.graphics, value=2}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.sys.mac.hardware, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.crypt, value=5}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.space, value=2}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='soc.religion.christian, value=161}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.politics.mideast, value=3}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.politics.misc, value=6}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.religion.misc, value=7}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='misc.forsale, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.politics.guns, value=158}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.politics.mideast, value=6}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.politics.misc, value=7}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.politics.mideast, value=179}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.politics.misc, value=3}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.crypt, value=6}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.space, value=3}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='soc.religion.christian, value=2}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.politics.guns, value=3}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.politics.mideast, value=6}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.politics.misc, value=124}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='alt.atheism, value=5}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.crypt, value=2}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='soc.religion.christian, value=21}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.politics.guns, value=6}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.politics.mideast, value=3}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.politics.misc, value=4}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.religion.misc, value=89}

Accuracy
Accuracy = 76.970184

Precision
Precision for target class alt.atheism = 0.80128205
Precision for target class comp.graphics = 0.58715594
Precision for target class comp.os.ms-windows.misc = 0.6969697
Precision for target class comp.sys.ibm.pc.hardware = 0.5154639
Precision for target class comp.sys.mac.hardware = 0.6647059
Precision for target class comp.windows.x = 0.7604167
Precision for target class misc.forsale = 0.4974359
Precision for target class rec.autos = 0.7952381
Precision for target class rec.motorcycles = 0.880597
Precision for target class rec.sport.baseball = 0.8333333
Precision for target class rec.sport.hockey = 0.8480392
Precision for target class sci.crypt = 0.89784944
Precision for target class sci.electronics = 0.6597938
Precision for target class sci.med = 0.90686274
Precision for target class sci.space = 0.88297874
Precision for target class soc.religion.christian = 0.80904526
Precision for target class talk.politics.guns = 0.89772725
Precision for target class talk.politics.mideast = 0.95721924
Precision for target class talk.politics.misc = 0.8378378
Precision for target class talk.religion.misc = 0.67424244

Recall
Recall for target class alt.atheism = 0.8802817
Recall for target class comp.graphics = 0.6564103
Recall for target class comp.os.ms-windows.misc = 0.6509434
Recall for target class comp.sys.ibm.pc.hardware = 0.6944444
Recall for target class comp.sys.mac.hardware = 0.84962404
Recall for target class comp.windows.x = 0.7227723
Recall for target class misc.forsale = 0.6879433
Recall for target class rec.autos = 0.8835979
Recall for target class rec.motorcycles = 0.880597
Recall for target class rec.sport.baseball = 0.9189189
Recall for target class rec.sport.hockey = 0.84390247
Recall for target class sci.crypt = 0.66533864
Recall for target class sci.electronics = 0.7111111
Recall for target class sci.med = 0.82222223
Recall for target class sci.space = 0.67479676
Recall for target class soc.religion.christian = 0.77403843
Recall for target class talk.politics.guns = 0.8102564
Recall for target class talk.politics.mideast = 0.83255816
Recall for target class talk.politics.misc = 0.7209302
Recall for target class talk.religion.misc = 0.773913

F1 score
F1 score for target class alt.atheism = 0.8389262
F1 score for target class comp.graphics = 0.6198547
F1 score for target class comp.os.ms-windows.misc = 0.67317075
F1 score for target class comp.sys.ibm.pc.hardware = 0.591716
F1 score for target class comp.sys.mac.hardware = 0.7458746
F1 score for target class comp.windows.x = 0.74111676
F1 score for target class misc.forsale = 0.57738096
F1 score for target class rec.autos = 0.8370927
F1 score for target class rec.motorcycles = 0.880597
F1 score for target class rec.sport.baseball = 0.87403595
F1 score for target class rec.sport.hockey = 0.84596574
F1 score for target class sci.crypt = 0.764302
F1 score for target class sci.electronics = 0.684492
F1 score for target class sci.med = 0.86247087
F1 score for target class sci.space = 0.764977
F1 score for target class soc.religion.christian = 0.79115486
F1 score for target class talk.politics.guns = 0.851752
F1 score for target class talk.politics.mideast = 0.8905473
F1 score for target class talk.politics.misc = 0.77500004
F1 score for target class talk.religion.misc = 0.72064775
