Confusion matrix
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.crypt, value=66}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.crypt, value=12}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.electronics, value=38}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.med, value=6}
ConfusionMatrixCell{actual='sci.med, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=52}

Accuracy
Accuracy = 88.1356

Precision
Precision for target class sci.crypt = 1.0
Precision for target class sci.electronics = 0.6785714
Precision for target class sci.med = 0.94545454

Recall
Recall for target class sci.crypt = 0.835443
Recall for target class sci.electronics = 0.95
Recall for target class sci.med = 0.8965517

F1 score
F1 score for target class sci.crypt = 0.91034484
F1 score for target class sci.electronics = 0.7916666
F1 score for target class sci.med = 0.920354
