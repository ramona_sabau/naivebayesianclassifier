Confusion matrix
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.crypt, value=57}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.med, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.crypt, value=12}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.electronics, value=48}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.med, value=6}
ConfusionMatrixCell{actual='sci.med, prediction='sci.crypt, value=3}
ConfusionMatrixCell{actual='sci.med, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=49}

Accuracy
Accuracy = 86.51685

Precision
Precision for target class sci.crypt = 0.9661017
Precision for target class sci.electronics = 0.72727275
Precision for target class sci.med = 0.9245283

Recall
Recall for target class sci.crypt = 0.7916667
Recall for target class sci.electronics = 0.97959185
Recall for target class sci.med = 0.8596491

F1 score
F1 score for target class sci.crypt = 0.870229
F1 score for target class sci.electronics = 0.83478266
F1 score for target class sci.med = 0.8909091
