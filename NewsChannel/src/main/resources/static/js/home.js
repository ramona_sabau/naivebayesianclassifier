
//Button toggle script

let rightContainer = $("#right_container");

rightContainer.slideUp();
rightContainer.toggleClass('hidden');

// Data fill script
/// take the id from user
let currentId = 1;

$.ajax({
    type: "GET",
    url: "http://localhost:8080/api/secured/recent-news/"  + currentId,
    dataType: "json",
    success: function (response) {
        let newsListContainer =  $("#myNewsList");
        let selectedNewsCategory = $("#selectedNewsCategory");
        let selectedNewsTitle = $("#selectedNewsTitle");
        let selectedNewsAuthor = $("#selectedNewsAuthor");
        let selectedNewsContent = $("#selectedNewsContent");

        selectedNewsCategory.text(response[0].channel.name);
        selectedNewsTitle.text(response[0].title);
        selectedNewsAuthor.text(response[0].author.firstName + response[0].author.lastName);
        selectedNewsContent.text(response[0].content);

        $.each(response, function (index, element) {
            let newsCategoryP = $("<p></p>").addClass("newsCategory").text("SCIENCE");
            let newsCategoryTitleH2 = $("<h2></h2>").addClass("title").text("This is a major discovery");
            let newsAuthorDiv = $("<div></div>").addClass("byline");
            let newsAuthorSpan =  $("<span></span>").text("Ramona Sabau");
            let newsContent = "Am prea multe caractere. Cred ca totul o sa fie bine. Maine va fi mai bine. Astept sa scap de licenta";
            let shortNewsContent = "";
            if (newsContent.length > 100) {
                shortNewsContent = newsContent.substr(0, newsContent.lastIndexOf(' ', 10)) + '...';
            }
            let newsContentP = $("<p></p>").addClass("excerpt").text(shortNewsContent);
            let newsBlockContentDiv = $("<div></div>").addClass("block_content");
            let newsBlockDiv = $("<div></div>").addClass("block");
            let li = $("<li></li>").addClass("pressable");

            newsCategoryP.appendTo(newsBlockContentDiv);
            newsCategoryTitleH2.appendTo(newsBlockContentDiv);
            newsAuthorSpan.appendTo(newsAuthorDiv);
            newsAuthorDiv.appendTo(newsBlockContentDiv);
            newsContentP.appendTo(newsBlockContentDiv);
            newsBlockContentDiv.appendTo(newsBlockDiv);
            newsBlockDiv.appendTo(li);
            li.appendTo(newsListContainer);
        });

        newsListContainer.children('li.pressable').each(function (index, element) {
            $(this).click(function () {
                selectedNewsCategory.text(response[0].channel.name);
                selectedNewsTitle.text(response[0].title);
                selectedNewsAuthor.text(response[0].author.firstName + response[0].author.lastName);
                selectedNewsContent.text(response[0].content);

                //window.location.replace("#");
                history.pushState('', document.title, window.location.pathname);
            });

        });
        rightContainer.slideDown();

    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
        //alert("You are unauthorized!");
    }
});