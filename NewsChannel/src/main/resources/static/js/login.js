let username = $("#usernameBox");
let password = $("#passwordBox");



$('#addSubmitLogin').click(function() {
    let data = {
        "username" : username.val(),
        "password" : password.val()
    };

    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8'
        },
        type: "POST",
        url: "http://localhost:8080/api/public/login",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (response){
            document.cookie = "token=" + response.token;
            document.cookie = "username=" + response.user.username;
            document.cookie = "firstName=" + response.user.firstName;
            document.cookie = "lastName=" + response.user.lastName;
            document.cookie = "userId=" + response.user.id;
            window.location.replace("http://localhost:8080/home.html");
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            var data = XMLHttpRequest.responseJSON;
            let errorField = $("#errorField");
            errorField.text(data.message);
            errorField.removeClass("hidden");
            errorField.slideDown();

            setTimeout(function(){ errorField.slideUp() }, 5000);
        }

    });
});


