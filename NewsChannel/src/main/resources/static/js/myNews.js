//Button toggle script

let rightContainer = $("#right_container");

let addNewsForm = $("#myNewsAddForm");

let editNewsForm = $("#myNewsEditForm");
let addButton = $("#addForm");

let editButton = $("#editForm");
editNewsForm.hide();

addNewsForm.hide();
rightContainer.slideUp();

rightContainer.toggleClass('hidden');
rightContainer.slideDown();

let listWithCategories = ["SCIENCE", "MEDICINE", "FOOTBALL"];
let categoryContainer = $("#categorySelector");
for (var i = 0; i < listWithCategories.length; i++) {
    let input = "<input type='radio' name='categoryOptions' class='flat' value='" + i +
        "'>  " + listWithCategories[i] + "<BR>";
    let label = $("<label></label>");
    let div = $("<div></div>").addClass("radio");

    label.html(input);
    label.appendTo(div);
    div.appendTo(categoryContainer);
}

addButton.click(function() {
    if(editNewsForm.is(":visible")) {
        editNewsForm.slideUp(function() {
            addNewsForm.slideDown();
        });
    } else if(addNewsForm.is(":visible")) {
        addNewsForm.slideUp();
    } else {
        addNewsForm.slideDown();
    }
});

editButton.click(function(){
   if(addNewsForm.is(":visible")) {
       addNewsForm.slideUp(function() {
           editNewsForm.slideDown();
        });
   } else if(editNewsForm.is(":visible")) {
       editNewsForm.slideUp();
   } else {
       editNewsForm.slideDown();
   }
});

// Data fill script 

let currentId = 0;

let newsListContainer =  $("#myNewsList");
let detailTitle = $("#newsTitle");
let detailContent = $("#newsContent");
let detailCategory = $("#newsCategory");
let editTitle2 = $("#editTitleBox");
let editContent2 = $("#editContentBox");

let editCategory = $("#editCategoryBox");
var response = [
    { title: "Too much salt may double your chances of heart failure", content: "Sodium helps the body perform a range of biological functions, so a little salt in our diet can be healthful. But too much salt is known to be bad for our cardiovascular system. In fact, a new study suggests it may even double our risk of heart failure.New research - presented at the European Society of Cardiology (ESC) Congress in Barcelona, Spain - examines the link between a high intake of salt and the risk of heart failure.The first author of the study is Pekka Jousilahti, a research professor at the National Institute for Health and Welfare in Helsinki, Finland. You should check the study", id: 1, channel:"MEDICINE" },
    { title: "Cocoa compound could 'delay or prevent' type 2 diabetes", content: "With diabetes reaching epidemic proportions, the search is on for innovative ways to reduce the burden. Breaking research finds hope in the most surprising of places - chocolate.Today, there are an estimated 29 million Americans living with diabetes, with the vast majority of cases being type 2 diabetes. Globally, by 2035, there could be 592 million people with diabetes. This is no small problem.", id: 2, channel:"MEDICINE" }
];

detailTitle.text(response[0].title);
detailContent.text(response[0].content);
detailCategory.text(response[0].channel);

editTitle2.val(response[0].title);
editContent2.val(response[0].content);


currentId = response[0].id;

$.each(response, function (index, element) {
    let h3 = $("<h3></h3>").text(element.title);
    let div1 = $("<div></div>").addClass("right");
    let div2 = $("<div></div>").addClass("mail_list");
    let a = $("<a></a>").attr("href", "#");

    h3.appendTo(div1);
    div1.appendTo(div2);
    div2.appendTo(a);
    a.appendTo(newsListContainer);
});

newsListContainer.children('a').each(function (index, element) {
    $(this).click(function () {
        detailTitle.text(response[index].title);
        detailContent.text(response[index].content);
        currentId = response[index].id;

        editTitle.val(response[index].title);
        editContent.val(response[index].content);

        //window.location.replace("#");
        history.pushState('', document.title, window.location.pathname);
    });

});
$.ajax({
    type: "GET",
    url: "http://localhost:8080/api/secured/my-news",
    dataType: "json",
    success: function (response) {
        let newsListContainer =  $("#myNewsList");
        let detailTitle = $("#newsTitle");
        let detailContent = $("#newsContent");
        let detailCategory = $("#newsCategory");

        let editTitle = $("#editTitleBox");
        let editContent = $("#editContentBox");
        let editCategory = $("#editCategoryBox");

        detailTitle.text(response[0].title);
        detailContent.text(response[0].content);
        detailCategory.text(response[0].channel.name);

        editTitle.val(response[0].title);
        editContent.val(response[0].content);


        currentId = response[0].id;

        $.each(response, function (index, element) {
            let h3 = $("<h3></h3>").text(element.name);
            let div1 = $("<div></div>").addClass("right");
            let div2 = $("<div></div>").addClass("mail_list");
            let a = $("<a></a>").attr("href", "#");

            h3.appendTo(div1);
            div1.appendTo(div2);
            div2.appendTo(a);
            a.appendTo(newsListContainer);
        });

        newsListContainer.children('a').each(function (index, element) {
            $(this).click(function () {
                detailTitle.text(response[index].title);
                detailContent.text(response[index].content);
                currentId = response[index].id;

                editTitle.val(response[index].title);
                editContent.val(response[index].content);

                //window.location.replace("#");
                history.pushState('', document.title, window.location.pathname);
            });

        });
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
        //alert("You are unauthorized!");
    }
});

//Add news

let addTitle = $("#addTitleBox");
let addContent = $("#addContentBox");

let addTitleErr = $("#addTitleError");
let addContentErr =$("#addContentError");

addTitleErr.slideUp();
addContentErr.slideUp();

//submitBtn.click(
function addNews() {
    let data = {
        "title" : addTitle.val(),
        "content" : addContent.val(),
        "userId": 1
    };

    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8'
        },
        type: "POST",
        url: "http://localhost:8080/api/secured/my-news",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (response){
            window.location.reload(true);
            addNewsForm.slideUp();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            var data = XMLHttpRequest.responseJSON;

            if(!(typeof data.title === 'undefined')) {
                addTitleErr.removeClass("hidden");
                addTitleErr.text(data.title);
                addTitleErr.slideDown();
            }
            if(!(typeof data.content === 'undefined')) {
                addContentErr.removeClass("hidden");
                addContentErr.text(data.content);
                addContentErr.slideDown();
            }
            setTimeout(function(){ addTitleErr.slideUp();addContentErr.slideUp() }, 5000);
        }
    });
}

//Delete news

let deleteBtn = $("#deleteNews");

deleteBtn.click(function() {
    $.ajax({
        type: "DELETE",
        url: "http://localhost:8080/api/secured/my-news/" + currentId,
        dataType: "json",
        success: function (response) {
        //window.location.reload(true);
        }
    });
    window.location.reload(true);
});

//Update news

let editTitle = $("#editTitleBox");
let editContent = $("#editContentBox");

let editTitleErr = $("#editTitleError");
let editContentErr =$("#editContentError");

editTitleErr.slideUp();
editContentErr.slideUp();

function updateNews(){

    let data = {
        "id" : currentId,
        "title" : editTitle.val(),
        "content" : editContent.val(),
        "userId": 1
    };

    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8'
        },
        type: "PUT",
        url: "http://localhost:8080/api/secured/my-news",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (response) {
            window.location.reload(true);

            editNewsForm.slideUp();
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            var data = XMLHttpRequest.responseJSON;

            if(!(typeof data.title === 'undefined')) {
                editTitleErr.text(data.title);
                editTitleErr.removeClass("hidden");
                editTitleErr.slideDown();
            }

            if(!(typeof data.content === 'undefined')) {
                editContentErr.text(data.content);
                editContentErr.removeClass("hidden");
                editContentErr.slideDown();
            }

            setTimeout(function(){
                editTitleErr.slideUp();
                editContentErr.slideUp()
            }, 5000);
        }
    });
}