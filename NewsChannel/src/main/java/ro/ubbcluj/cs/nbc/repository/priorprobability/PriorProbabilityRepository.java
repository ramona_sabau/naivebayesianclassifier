package ro.ubbcluj.cs.nbc.repository.priorprobability;

import ro.ubbcluj.cs.nbc.model.PriorProbability;

import java.util.Optional;

/**
 * Created by sabau on 14/06/2017.
 */
public interface PriorProbabilityRepository {
    void save(PriorProbability priorProbability);

    Optional<PriorProbability> findPriorProbabilityForTargetClass(String targetClass);
}
