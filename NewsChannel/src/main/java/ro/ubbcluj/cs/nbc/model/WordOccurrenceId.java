package ro.ubbcluj.cs.nbc.model;

import java.io.Serializable;

public class WordOccurrenceId implements Serializable {

    private Long word;

    private Long document;

    public WordOccurrenceId() {
    }

    public Long getWord() {
        return word;
    }

    public void setWord(Long word) {
        this.word = word;
    }

    public Long getDocument() {
        return document;
    }

    public void setDocument(Long document) {
        this.document = document;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordOccurrenceId that = (WordOccurrenceId) o;

        if (word != null ? !word.equals(that.word) : that.word != null) return false;
        return document != null ? document.equals(that.document) : that.document == null;
    }

    @Override
    public int hashCode() {
        int result = word != null ? word.hashCode() : 0;
        result = 31 * result + (document != null ? document.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WordOccurrenceId{" +
                "word=" + word +
                ", document=" + document +
                '}';
    }
}
