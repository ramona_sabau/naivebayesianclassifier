package ro.ubbcluj.cs.nbc.common.io.reader;

import org.codemonkey.simplejavamail.email.Email;
import org.codemonkey.simplejavamail.email.Recipient;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractEmlReader;
import ro.ubbcluj.cs.nbc.model.EmailMessage;

import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ramona on 14.05.2017.
 */
public class EmailMessageEmlReader extends AbstractEmlReader<EmailMessage> {

    private static final Logger logger = LoggerFactory.getLogger(EmailMessageEmlReader.class);

    @Override
    public EmailMessage createEntity(MimeMessage mimeMessage) throws ReaderException {
        logger.info("Start creating MailMessage entity based on received MimeMessage");
        try {
            Email email = new Email(mimeMessage);
            EmailMessage emailMessage = new EmailMessage();

            Recipient emailSender = email.getFromRecipient();
            emailMessage.setSender(emailSender.getAddress());

            List<Recipient> emailRecipients = email.getRecipients();
            List<String> emailReceivers = emailRecipients.stream()
                    .map(Recipient::getAddress)
                    .collect(Collectors.toList());
            emailMessage.setReceivers(emailReceivers);

            emailMessage.setSubject(email.getSubject());

            String emailText = email.getText();
            String emailHtmlText = email.getTextHTML();
            String content = "";
            if (emailText == null && emailHtmlText == null) {
                logger.info("Current e-mail does not have content.");
                return null;
            }

            if (emailText != null) {
                logger.info("Current e-mail contains text content");
                content = emailText;
            }

            if (emailHtmlText != null) {
                logger.info("Current email contains html text content.");
                Document jsoupDocument = Jsoup.parse(emailHtmlText);
                content = jsoupDocument.text();
            }
            emailMessage.setContent(content);
            return emailMessage;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }

    }
}
