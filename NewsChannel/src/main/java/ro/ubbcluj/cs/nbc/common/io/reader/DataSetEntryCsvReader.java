package ro.ubbcluj.cs.nbc.common.io.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractFileReader;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ramona on 14.05.2017.
 */
public class DataSetEntryCsvReader extends AbstractFileReader<DataSetEntry> {

    private static final Logger logger = LoggerFactory.getLogger(DataSetEntryCsvReader.class);

    private static final int numberOfAttributesPerLine = 3;

    @Override
    public DataSetEntry createEntity(String line) throws ReaderException {

        if (line == null || line.isEmpty()) {
            String errorMessage = "The line does not contain any values.";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        List<String> attributesFromCurrentLine = Arrays.asList(line.split(getSeparator()));

        if (numberOfAttributesPerLine != attributesFromCurrentLine.size()) {
            String errorMessage = "The line does not contain the proper number of attributes.";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        DataSetEntry dataSetEntry = new DataSetEntry();
        dataSetEntry.setEmailFileName(attributesFromCurrentLine.get(2));
        dataSetEntry.setEmailPath(attributesFromCurrentLine.get(1));
        dataSetEntry.setTargetClass(attributesFromCurrentLine.get(0));

        return dataSetEntry;
    }

    @Override
    public String getSeparator() {
        return ",";
    }
}
