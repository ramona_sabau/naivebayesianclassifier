package ro.ubbcluj.cs.nbc.repository.word;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.ubbcluj.cs.nbc.model.Word;
import ro.ubbcluj.cs.nbc.model.dto.WordWithFrequency;

import java.util.List;
import java.util.Optional;

/**
 * Created by ramona on 14.05.2017.
 */
@Repository
public class WordDaoImpl implements WordDao {

    private static final Logger logger = LoggerFactory.getLogger(WordDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public WordDaoImpl() {
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Word> findAllFromVocabularyWithTargetClass(String vocabulary, String targetClass) {
        logger.info("Start fetching all words");

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class, "word")
                .setFetchMode("word.vocabulary", FetchMode.JOIN)
                .createAlias("word.vocabulary", "vocabulary")
                .setFetchMode("word.wordOccurrences", FetchMode.JOIN)
                .createAlias("word.wordOccurrences", "wordOccurrences")
                .setFetchMode("wordOccurrences.document", FetchMode.JOIN)
                .createAlias("wordOccurrences.document", "document")
                .setFetchMode("document.targetClass", FetchMode.JOIN)
                .createAlias("document.targetClass", "targetClass")
                .add(Restrictions.eq("targetClass.name", targetClass))
                .add(Restrictions.eq("vocabulary.name", vocabulary))
                .setProjection(Projections.projectionList()
                        .add(Projections.distinct(Projections.property("name")))
                        .add(Projections.property("word.id"),"id")
                        .add(Projections.property("word.name"),"name"))
                .setResultTransformer(Transformers.aliasToBean(Word.class));
        List<Word> words = criteria.list();
        session.close();

        return words;
    }

    @Override
    public Optional<Word> findOneByName(String name) {
        logger.info("Start fetching word with name = " + name);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class)
                .add(Restrictions.eq("name", name))
                .setMaxResults(1);
        Word word = (Word) criteria.uniqueResult();
        session.close();

        if (word == null) {
            logger.info("No word found with name " + name);
            return Optional.empty();
        }
        else {
            logger.info("Word found with name " + name + ": " + word);
            return Optional.of(word);
        }
    }

    @Override
    public List<WordWithFrequency> findAllWordsWithFrequencies() {
        logger.info("Start fetching all words with frequencies");

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class, "word")
                .setFetchMode("word.wordOccurrences", FetchMode.JOIN)
                .createAlias("word.wordOccurrences", "wordOccurrences")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("word.id"), "wordId")
                        .add(Projections.property("word.name"), "name")
                        .add(Projections.sum("wordOccurrences.numberOfOccurrences"), "frequency")
                        .add(Projections.groupProperty("word.name"))
                        .add(Projections.groupProperty("word.id")))
                .addOrder(Order.desc("frequency"))
                .setResultTransformer(Transformers.aliasToBean(WordWithFrequency.class));
        List<WordWithFrequency> wordWithFrequencies = criteria.list();
        session.close();

        return wordWithFrequencies;
    }

    @Override
    public Long countAllFromVocabulary(String vocabulary) {
        logger.info("Start counting all words from  " + vocabulary + " vocabulary");

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class, "word")
                .setFetchMode("word.vocabulary", FetchMode.JOIN)
                .createAlias("word.vocabulary", "vocabulary")
                .add(Restrictions.eq("vocabulary.name", vocabulary))
                .setProjection(Projections.count("word.name"));
        Long numberOfWords = (Long) criteria.uniqueResult();
        session.close();

        return numberOfWords;
    }

    @Override
    public Long countAllFromDocumentsWithTargetClass(String targetClass) {
        logger.info("Start counting all words from documents with target class" + targetClass);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class, "word")
                .setFetchMode("word.wordOccurrences", FetchMode.JOIN)
                .createAlias("word.wordOccurrences", "wordOccurrence")
                .setFetchMode("wordOccurrence.document", FetchMode.JOIN)
                .createAlias("wordOccurrence.document", "document")
                .setFetchMode("document.targetClass", FetchMode.JOIN)
                .createAlias("document.targetClass", "targetClass")
                .add(Restrictions.eq("targetClass.name", targetClass))
                .setProjection(Projections.sum("wordOccurrence.numberOfOccurrences"));
        Long numberOfWords = (Long) criteria.uniqueResult();
        session.close();

        return numberOfWords;
    }

    @Override
    public Long countWordAppearancesFromDocumentsWithTargetClass(String targetClass, String word) {
        logger.info("Start counting all appearances of word " + word + " in documents with target class " + targetClass);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class, "word")
                .setFetchMode("word.wordOccurrences", FetchMode.JOIN)
                .createAlias("word.wordOccurrences", "wordOccurrence")
                .setFetchMode("wordOccurrence.document", FetchMode.JOIN)
                .createAlias("wordOccurrence.document", "document")
                .setFetchMode("document.targetClass", FetchMode.JOIN)
                .createAlias("document.targetClass", "targetClass")
                .add(Restrictions.eq("targetClass.name", targetClass))
                .add(Restrictions.eq("word.name", word))
                .setProjection(Projections.sum("wordOccurrence.numberOfOccurrences"));
        Long numberOfWords = (Long) criteria.uniqueResult();
        session.close();

        return numberOfWords;
    }

    @Override
    public void delete(Long id) {
        logger.info("Start deleting word with id = " + id);

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(Word.class)
                .add(Restrictions.eq("id", id));
        Word wordToBeDeleted = (Word) criteria.uniqueResult();

        wordToBeDeleted.getWordOccurrences().forEach(session::delete);
        session.delete(wordToBeDeleted);
        session.getTransaction().commit();
        session.close();

        logger.info("Word with id = " + id + " is successfully deleted.");
    }
}
