package ro.ubbcluj.cs.nbc.common.io.reader;

import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractFileReader;

/**
 * Created by sabau on 20/06/2017.
 */
public class ContentFileReader extends AbstractFileReader<String> {

    @Override
    public String createEntity(String line) throws ReaderException {
        return line;
    }

    @Override
    public String getSeparator() {
        return null;
    }
}
