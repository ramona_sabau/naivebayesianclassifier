package ro.ubbcluj.cs.nbc.repository.likelihoodprobability;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.ubbcluj.cs.nbc.model.LikelihoodProbability;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.Word;

import java.util.Optional;

/**
 * Created by sabau on 14/06/2017.
 */
@Repository
public class LikelihoodProbabilityDaoImpl implements LikelihoodProbabilityDao {

    private static final Logger logger = LoggerFactory.getLogger(LikelihoodProbabilityDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public LikelihoodProbabilityDaoImpl() {
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<LikelihoodProbability> findOneWithWordAndTargetClass(String word, String targetClass) {
        logger.info("Start fetching likelihood probability for word " + word + " and target class " + targetClass);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(LikelihoodProbability.class, "likelihoodProbability")
                .setFetchMode("likelihoodProbability.word", FetchMode.JOIN)
                .createAlias("likelihoodProbability.word", "word")
                .setFetchMode("likelihoodProbability.targetClass", FetchMode.JOIN)
                .createAlias("likelihoodProbability.targetClass", "targetClass")
                .add(Restrictions.eq("word.name", word))
                .add(Restrictions.eq("targetClass.name", targetClass));
        LikelihoodProbability likelihoodProbability = (LikelihoodProbability) criteria.uniqueResult();
        session.close();

        if (likelihoodProbability == null) {
            return Optional.empty();
        } else {
            return Optional.of(likelihoodProbability);
        }
    }

    @Override
    public LikelihoodProbability save(LikelihoodProbability likelihoodProbability) {
        logger.info("Start saving new likelihood probability " + likelihoodProbability);

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        likelihoodProbability.setTargetClass((TargetClass) session.get(TargetClass.class,
                likelihoodProbability.getTargetClass().getId()));
        likelihoodProbability.setWord((Word) session.get(Word.class,
                likelihoodProbability.getWord().getId()));

        session.save(likelihoodProbability);
        session.getTransaction().commit();
        session.close();

        return likelihoodProbability;
    }
}
