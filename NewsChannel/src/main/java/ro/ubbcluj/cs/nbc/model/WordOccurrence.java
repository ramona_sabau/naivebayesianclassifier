package ro.ubbcluj.cs.nbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(WordOccurrenceId.class)
public class WordOccurrence {
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="word_id", referencedColumnName = "id")
    private Word word;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="document_id", referencedColumnName = "id")
    private Document document;

    @Column(nullable = false)
    private Long numberOfOccurrences;

    public WordOccurrence() {
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Long getNumberOfOccurrences() {
        return numberOfOccurrences;
    }

    public void setNumberOfOccurrences(Long numberOfOccurrences) {
        this.numberOfOccurrences = numberOfOccurrences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordOccurrence that = (WordOccurrence) o;

        if (word != null ? !word.equals(that.word) : that.word != null) return false;
        if (document != null ? !document.equals(that.document) : that.document != null) return false;
        return numberOfOccurrences != null ? numberOfOccurrences.equals(that.numberOfOccurrences) : that.numberOfOccurrences == null;
    }

    @Override
    public int hashCode() {
        int result = word != null ? word.hashCode() : 0;
        result = 31 * result + (document != null ? document.hashCode() : 0);
        result = 31 * result + (numberOfOccurrences != null ? numberOfOccurrences.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WordOccurrence{" +
                "word=" + word +
                ", document=" + document +
                ", numberOfOccurrences=" + numberOfOccurrences +
                '}';
    }
}
