package ro.ubbcluj.cs.nbc.common.io.writer;

import ro.ubbcluj.cs.common.io.writer.AbstractCsvWriter;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;

public class DataSetEntryCsvWriter extends AbstractCsvWriter<DataSetEntry> {

    @Override
    public String convertEntityToString(DataSetEntry entity) {

        final String SEPARATOR = getSeparator();

        StringBuilder sb = new StringBuilder();
        sb.append(entity.getTargetClass()).append(SEPARATOR)
                .append(entity.getEmailPath()).append(SEPARATOR)
                .append(entity.getEmailFileName());

        return sb.toString();
    }

    @Override
    public String getSeparator() {
        return ",";
    }
}
