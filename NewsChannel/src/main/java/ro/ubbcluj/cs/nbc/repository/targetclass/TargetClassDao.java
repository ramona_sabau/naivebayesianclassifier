package ro.ubbcluj.cs.nbc.repository.targetclass;

import ro.ubbcluj.cs.nbc.model.TargetClass;

import java.util.List;
import java.util.Optional;

/**
 * Created by ramona on 15.05.2017.
 */
public interface TargetClassDao {
    Optional<TargetClass> findOneByName(String name);

    List<TargetClass> findAllWithVocabulary(String vocabulary);

    TargetClass save(TargetClass currentClass);
}
