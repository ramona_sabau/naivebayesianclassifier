package ro.ubbcluj.cs.nbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by ramona on 14.05.2017.
 */
@Entity(name = "likelihood_probability")
@IdClass(LikelihoodProbabilityId.class)
public class LikelihoodProbability {

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="word_id", referencedColumnName = "id")
    private Word word;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="target_class_id", referencedColumnName = "id")
    private TargetClass targetClass;

    @Column(nullable = false)
    private Float likelihoodProbability;

    public LikelihoodProbability() {
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public TargetClass getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(TargetClass targetClass) {
        this.targetClass = targetClass;
    }

    public Float getLikelihoodProbability() {
        return likelihoodProbability;
    }

    public void setLikelihoodProbability(Float likelihoodProbability) {
        this.likelihoodProbability = likelihoodProbability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikelihoodProbability that = (LikelihoodProbability) o;

        if (word != null ? !word.equals(that.word) : that.word != null) return false;
        if (targetClass != null ? !targetClass.equals(that.targetClass) : that.targetClass != null) return false;
        return likelihoodProbability != null ? likelihoodProbability.equals(that.likelihoodProbability) : that.likelihoodProbability == null;

    }

    @Override
    public int hashCode() {
        int result = word != null ? word.hashCode() : 0;
        result = 31 * result + (targetClass != null ? targetClass.hashCode() : 0);
        result = 31 * result + (likelihoodProbability != null ? likelihoodProbability.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LikelihoodProbability{" +
                "word=" + word +
                ", targetClass=" + targetClass +
                ", likelihoodProbability=" + likelihoodProbability +
                '}';
    }
}
