package ro.ubbcluj.cs.nbc.service.learning;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.ExistingVocabulary;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.Word;

import java.util.List;

@Service
public class EnglishNaiveBayesLearningService extends AbstractNaiveBayesLearningService {

    private static final Logger logger = LoggerFactory.getLogger(EnglishNaiveBayesLearningService.class);

    private static final String ENGLISH_VOCABULARY = ExistingVocabulary.ENGLISH.toString();

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected List<TargetClass> getExistingTargetClassesFromVocabulary() throws ServiceException {
        logger.info("Start fetching all target classes for english vocabulary");

        List<TargetClass> targetClasses = targetClassDao.findAllWithVocabulary(ENGLISH_VOCABULARY);
        if (targetClasses == null || targetClasses.isEmpty()) {
            String errorMessage = "No target classes were found";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage);
        }

        return targetClasses;
    }

    @Override
    protected List<Word> getExistingWordsFromVocabularyWithTargetClass(String targetClass) throws ServiceException {
        logger.info("Start fetching existing words from english vocabulary that appear in documents with target class "
                + targetClass);

        List<Word> words = wordDao.findAllFromVocabularyWithTargetClass(ENGLISH_VOCABULARY, targetClass);
        if (words == null || words.isEmpty()) {
            String errorMessage = "No english words were found";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage);
        }

        return words;
    }

    @Override
    protected Long getNumberOfWordsFromVocabulary() throws ServiceException {
        logger.info("Start calculating the number of unique words from english vocabulary");

        Long numberOfWords = wordDao.countAllFromVocabulary(ENGLISH_VOCABULARY);
        if (numberOfWords == null || numberOfWords.equals(0L)) {
            String errorMessage = "No words in english vocabulary were found";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage);
        }

        return numberOfWords;
    }
}
