package ro.ubbcluj.cs.nbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbc.service.classifier.AbstractNaiveBayesClassificationService;
import ro.ubbcluj.cs.nbc.service.learning.AbstractNaiveBayesLearningService;
import ro.ubbcluj.cs.nbc.service.vocabulary.AbstractVocabularyCollectorService;
import ro.ubbcluj.cs.nbc.service.vocabulary.EnglishVocabularyCollectorService;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by ramona on 14.05.2017.
 */
public class EmailNaiveBayesianClassifierApplication {

    private static final Logger logger = LoggerFactory.getLogger(EmailNaiveBayesianClassifierApplication.class);

    @Autowired
    private AbstractVocabularyCollectorService englishVocabularyCollectorService;

    @Autowired
    private AbstractNaiveBayesLearningService englishNaiveBayesLearningService;

    @Autowired
    private AbstractNaiveBayesClassificationService englishNaiveBayesClassificationService;

    public EmailNaiveBayesianClassifierApplication() throws MessagingException, ReaderException, ServiceException, FileNotFoundException {
    }

    public void setUpClassifier() throws MessagingException, ReaderException, ServiceException, FileNotFoundException {
        englishVocabularyCollectorService.startBuildingVocabulary();
        logger.info("Vocabulary is successfully imported");

        englishNaiveBayesLearningService.startTrainingNaiveBayesClassifier();
        logger.info("Training process is finished successfully");
    }

    public String classifyNewInstance(DataSetEntry dataSetEntry) throws MessagingException, FileNotFoundException,
            ServiceException, ReaderException {
        return englishNaiveBayesClassificationService.startPredictingTargetClassForDataSetEntry(dataSetEntry);
    }

    public List<String> getAvailableTargetValues() throws ServiceException {
        return englishNaiveBayesClassificationService.determineTargetClassesForVocabulary();
    }
}
