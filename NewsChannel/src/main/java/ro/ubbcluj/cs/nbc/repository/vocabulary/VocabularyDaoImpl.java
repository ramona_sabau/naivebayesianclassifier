package ro.ubbcluj.cs.nbc.repository.vocabulary;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.ubbcluj.cs.nbc.model.Vocabulary;

import java.util.Optional;

/**
 * Created by ramona on 15.05.2017.
 */
@Repository
public class VocabularyDaoImpl implements VocabularyDao {

    private static final Logger logger = LoggerFactory.getLogger(VocabularyDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public VocabularyDaoImpl() {
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Vocabulary> findOneByName(String name) {
        logger.info("Start fetching Vocabulary with name = " + name);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Vocabulary.class)
                .add(Restrictions.eq("name", name));
        Vocabulary vocabulary = (Vocabulary) criteria.uniqueResult();
        session.close();

        if (vocabulary == null) {
            logger.info("No vocabulary found with name " + name);
            return Optional.empty();
        }
        else {
            logger.info("Vocabulary found with name " + name + ": " + vocabulary);
            return Optional.of(vocabulary);
        }
    }

    @Override
    public Vocabulary save(Vocabulary vocabulary) {
        logger.info("Start saving vocabulary entity " + vocabulary);

        Session session = sessionFactory.openSession();
        session.save(vocabulary);
        session.close();

        logger.info("Vocabulary successfully saved.");
        return vocabulary;
    }
}
