package ro.ubbcluj.cs.nbc.service.learning;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.LikelihoodProbability;
import ro.ubbcluj.cs.nbc.model.PriorProbability;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.Word;
import ro.ubbcluj.cs.nbc.repository.document.DocumentDao;
import ro.ubbcluj.cs.nbc.repository.likelihoodprobability.LikelihoodProbabilityDao;
import ro.ubbcluj.cs.nbc.repository.priorprobability.PriorProbabilityRepository;
import ro.ubbcluj.cs.nbc.repository.targetclass.TargetClassDao;
import ro.ubbcluj.cs.nbc.repository.word.WordDao;

import java.util.List;

public abstract class AbstractNaiveBayesLearningService {

    @Autowired
    protected DocumentDao documentDao;

    @Autowired
    protected TargetClassDao targetClassDao;

    @Autowired
    protected WordDao wordDao;

    @Autowired
    protected LikelihoodProbabilityDao likelihoodProbabilityDao;

    @Autowired
    protected PriorProbabilityRepository priorProbabilityRepository;

    public void startTrainingNaiveBayesClassifier() throws ServiceException {
        getLogger().info("Start training Naive Bayes Classifier");
        List<TargetClass> targetClasses = getExistingTargetClassesFromVocabulary();
        Long numberOfDocuments = documentDao.countAll();

        for (TargetClass targetClass : targetClasses) {
            calculatePriorProbability(targetClass, numberOfDocuments);
            calculateLikelihoodProbabilities(targetClass);
        }
    }

    private void calculateLikelihoodProbabilities(TargetClass targetClass) throws ServiceException {
        getLogger().info("Start calculating likelihood probabilities for target class " + targetClass);

        Long numberOfWordsFromDocumentsWithTargetClass = wordDao.countAllFromDocumentsWithTargetClass(
                targetClass.getName());

        Long numberOfWordsFromVocabulary = getNumberOfWordsFromVocabulary();

        List<Word> words = getExistingWordsFromVocabularyWithTargetClass(targetClass.getName());
        for (Word word : words) {
            Long appearanceNumberOfWord = wordDao.countWordAppearancesFromDocumentsWithTargetClass(targetClass.getName(),
                    word.getName());

            LikelihoodProbability likelihoodProbability = new LikelihoodProbability();
            likelihoodProbability.setTargetClass(targetClass);
            likelihoodProbability.setWord(word);

            appearanceNumberOfWord = appearanceNumberOfWord + 1;
            Float likelihoodProbabilityValue = appearanceNumberOfWord.floatValue() /
                    (numberOfWordsFromDocumentsWithTargetClass + numberOfWordsFromVocabulary);
            likelihoodProbability.setLikelihoodProbability(likelihoodProbabilityValue);

            likelihoodProbabilityDao.save(likelihoodProbability);
        }
    }

    private void calculatePriorProbability(TargetClass targetClass, Long numberOfDocuments) {
        getLogger().info("Start calculating prior probability for target class " + targetClass);

        Float numberOfDocumentsWithTargetClass = documentDao.countAllWithTargetClass(targetClass.getId()).floatValue();

        Float priorProbability = numberOfDocumentsWithTargetClass / numberOfDocuments;
        PriorProbability priorProbabilityForTargetClass = new PriorProbability();
        priorProbabilityForTargetClass.setTargetClass(targetClass.getName());
        priorProbabilityForTargetClass.setPriorProbability(priorProbability);
        getLogger().info("Prior probability calculated for current target class " + priorProbabilityForTargetClass);

        priorProbabilityRepository.save(priorProbabilityForTargetClass);
    }

    protected abstract Logger getLogger();

    protected abstract List<TargetClass> getExistingTargetClassesFromVocabulary() throws ServiceException;

    protected abstract List<Word> getExistingWordsFromVocabularyWithTargetClass(String targetClass) throws ServiceException;

    protected abstract Long getNumberOfWordsFromVocabulary() throws ServiceException;
}
