package ro.ubbcluj.cs.nbc.service.vocabulary;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.common.configuration.properties.NBCConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.common.io.reader.AbstractFileReader;
import ro.ubbcluj.cs.common.processor.LemmatizerProcessor;
import ro.ubbcluj.cs.common.processor.Processor;
import ro.ubbcluj.cs.common.utils.text.TextUtils;
import ro.ubbcluj.cs.nbc.model.Document;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.Vocabulary;
import ro.ubbcluj.cs.nbc.model.Word;
import ro.ubbcluj.cs.nbc.model.WordOccurrence;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbc.model.dto.WordWithFrequency;
import ro.ubbcluj.cs.nbc.repository.document.DocumentDao;
import ro.ubbcluj.cs.nbc.repository.targetclass.TargetClassDao;
import ro.ubbcluj.cs.nbc.repository.vocabulary.VocabularyDao;
import ro.ubbcluj.cs.nbc.repository.word.WordDao;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public abstract class AbstractVocabularyCollectorService {

    private static final Long MIN_WORD_OCCURRENCE = 3L;
    private static final Long MAX_WORD_OCCURRENCE = 100L;

    @Autowired
    protected AbstractFileReader<DataSetEntry> dataSetEntryCsvReader;

    @Autowired
    private AbstractFileReader<String> messageFileReader;

    @Autowired
    protected VocabularyDao vocabularyDao;

    @Autowired
    protected TargetClassDao targetClassDao;

    @Autowired
    protected DocumentDao documentDao;

    @Autowired
    protected WordDao wordDao;

    public AbstractVocabularyCollectorService() {
    }

    public void startBuildingVocabulary() throws ReaderException, FileNotFoundException,
            MessagingException, ServiceException {
        getLogger().info("Start building vocabulary");
        String pathForTrainingSetFile = NBCConfigurationManager.getTrainingSetFilePath();
        List<DataSetEntry> trainingSetEntries = dataSetEntryCsvReader.readFromFile(pathForTrainingSetFile);
        for (DataSetEntry trainingSetEntry : trainingSetEntries) {
            boolean mustBeSaved = importWordsFromDocument(trainingSetEntry);
            if (mustBeSaved) {
                saveWordsFromDocumentInVocabulary(trainingSetEntry);
            }
        }
        clearIrrelevantWordsForClassification();
    }

    private boolean importWordsFromDocument(DataSetEntry dataSetEntry) throws FileNotFoundException,
            ReaderException, MessagingException {
        getLogger().info("Start importing words for each document form training set");

        Processor<String, List<String>> emailContentProcessor = new LemmatizerProcessor();

        List<String> messageContent = messageFileReader.readFromFile(
                dataSetEntry.getEmailPath() + dataSetEntry.getEmailFileName());

        if (messageContent == null || messageContent.isEmpty()) {
            String errorMessage = "E-mail could not be extracted from eml file " + dataSetEntry.getEmailPath() +
                    dataSetEntry.getEmailFileName();
            getLogger().info(errorMessage);
            return false;
        }

        StringBuilder sb = new StringBuilder();
        final String separator = System.getProperty("line.separator");
        messageContent.forEach(line -> sb.append(line).append(separator));

        List<String> wordsFromEmailContent = emailContentProcessor.process(sb.toString());
        wordsFromEmailContent = TextUtils.removePunctuationMarks(wordsFromEmailContent);
        dataSetEntry.setListOfWords(wordsFromEmailContent);
        return true;
    }

    private void saveWordsFromDocumentInVocabulary(DataSetEntry dataSetEntry) {
        getLogger().info("Start saving all new words and documents in vocabulary");

        Vocabulary vocabulary = createVocabularyForLanguage();

        Document document = new Document();
        document.setName(dataSetEntry.getTargetClass() + dataSetEntry.getEmailFileName());
        document.setPath(dataSetEntry.getEmailPath());

        Optional<TargetClass> foundTargetClass = targetClassDao.findOneByName(dataSetEntry.getTargetClass());
        TargetClass targetClass;
        if (foundTargetClass.isPresent()) {
            targetClass = foundTargetClass.get();
        } else {
            targetClass = new TargetClass();
            targetClass.setName(dataSetEntry.getTargetClass());
        }

        List<WordOccurrence> wordOccurrences = createWordsForDocument(
                dataSetEntry.getListOfWords(),
                vocabulary);

        documentDao.save(document, wordOccurrences, targetClass);
    }

    private ArrayList<WordOccurrence> createWordsForDocument(List<String> listOfWords,
                                                             Vocabulary vocabulary) {
        getLogger().info("Start creating list of words for current document");

        Map<String, WordOccurrence> wordOccurrences = new HashMap<>();
        for (String wordFromDocument : listOfWords) {

            wordFromDocument = wordFromDocument.toUpperCase().replaceAll("[^A-Z0-9]", "");

            if (!isCommonWordForVocabulary(wordFromDocument) && wordFromDocument.length() < 255) {

                Optional<Word> existingWord = wordDao.findOneByName(wordFromDocument);
                Word word;
                if (existingWord.isPresent()) {
                    word = existingWord.get();
                } else {
                    word = new Word();
                    word.setName(wordFromDocument);
                    word.setVocabulary(vocabulary);
                }

                WordOccurrence existingWordOccurrence = wordOccurrences.get(wordFromDocument);
                if (existingWordOccurrence == null) {

                    WordOccurrence wordOccurrence = new WordOccurrence();
                    wordOccurrence.setNumberOfOccurrences(1L);
                    wordOccurrence.setWord(word);
                    wordOccurrences.put(wordFromDocument, wordOccurrence);
                } else {
                    Long occurrences = existingWordOccurrence.getNumberOfOccurrences();
                    existingWordOccurrence.setNumberOfOccurrences(occurrences + 1);
                }
            }
        }
        return new ArrayList<>(wordOccurrences.values());
    }

    private void clearIrrelevantWordsForClassification() {
        getLogger().info("Start removing irrelevant words for classification");

        List<WordWithFrequency> wordWithFrequencies = wordDao.findAllWordsWithFrequencies();
        if (wordWithFrequencies == null || wordWithFrequencies.isEmpty()) {
            getLogger().warn("Words could not be eliminated. No words were found");
            return;
        }

        List<WordWithFrequency> filteredWords = wordWithFrequencies.stream()
                .filter(wordWithFrequency -> (wordWithFrequency.getFrequency().compareTo(MIN_WORD_OCCURRENCE) <= 0 ||
                        wordWithFrequency.getFrequency().compareTo(MAX_WORD_OCCURRENCE) >= 0))
                .collect(Collectors.toList());

        filteredWords.forEach( word -> {
            getLogger().info("Delete word with id = " + word.getWordId() + " because its frequency is " + word.getFrequency());
            wordDao.delete(word.getWordId());
        });
    }

    protected abstract boolean isCommonWordForVocabulary(String word);

    protected abstract Vocabulary createVocabularyForLanguage();

    protected abstract Logger getLogger();
}
