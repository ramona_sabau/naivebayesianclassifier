package ro.ubbcluj.cs.nbc.service.classifier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.EnglishCommonWord;
import ro.ubbcluj.cs.nbc.model.ExistingVocabulary;
import ro.ubbcluj.cs.nbc.model.TargetClass;

import java.util.ArrayList;
import java.util.List;

@Service
public class EnglishNaiveBayesClassificatorService extends AbstractNaiveBayesClassificationService {

    private static final Logger logger = LoggerFactory.getLogger(EnglishNaiveBayesClassificatorService.class);

    private static final String ENGLISH_VOCABULARY = ExistingVocabulary.ENGLISH.toString();

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public List<String> determineTargetClassesForVocabulary() throws ServiceException {
        logger.info("Start fetching all target classes from english vocabulary");

        List<TargetClass> targetClasses = targetClassDao.findAllWithVocabulary(ENGLISH_VOCABULARY);
        if (targetClasses == null || targetClasses.isEmpty()) {
            String errorMessage = "No target classes are configured";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage);
        }

        List<String> convertedTargetClasses = new ArrayList<>();
        targetClasses.forEach(targetClass -> convertedTargetClasses.add(targetClass.getName()));

        return convertedTargetClasses;
    }


    @Override
    protected boolean isCommonWordForVocabulary(String word) {
        logger.info("Find if word " + word + " is common word in english vocabulary");

        try {
            String existingWord = EnglishCommonWord.valueOf(word).toString();
            logger.info("Word " + word + " will not be considered for classification because it is a common word");
            return true;
        } catch (IllegalArgumentException e) {
            logger.info("Word " + word + " is not a common word");
            return false;
        }
    }
}
