package ro.ubbcluj.cs.nbc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by ramona on 14.05.2017.
 */
@Entity(name = "target_class")
public class TargetClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "targetClass")
    private List<LikelihoodProbability> likelihoodProbabilities;

    public TargetClass() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<LikelihoodProbability> getLikelihoodProbabilities() {
        return likelihoodProbabilities;
    }

    public void setLikelihoodProbabilities(List<LikelihoodProbability> likelihoodProbabilities) {
        this.likelihoodProbabilities = likelihoodProbabilities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TargetClass that = (TargetClass) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TargetClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
