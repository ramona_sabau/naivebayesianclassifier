package ro.ubbcluj.cs.nbc.repository.targetclass;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.Word;

import java.util.List;
import java.util.Optional;

/**
 * Created by ramona on 15.05.2017.
 */
@Repository
public class TargetClassDaoImpl implements TargetClassDao {

    private static final Logger logger = LoggerFactory.getLogger(TargetClassDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public TargetClassDaoImpl() {
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<TargetClass> findOneByName(String name) {
        logger.info("Start fetching class with name = " + name);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(TargetClass.class)
                .add(Restrictions.eq("name", name));
        TargetClass targetClass = (TargetClass) criteria.uniqueResult();
        session.close();

        if (targetClass == null) {
            logger.info("No target class found with name = " + name);
            return Optional.empty();
        }
        else {
            logger.info("Target class found with name " + name + ": " + targetClass);
            return Optional.of(targetClass);
        }
    }

    @Override
    public List<TargetClass> findAllWithVocabulary(String vocabulary) {
        logger.info("Start fetching all target classes registered for " + vocabulary + " vocabulary");

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Word.class, "word")
                .setFetchMode("word.document", FetchMode.JOIN)
                .createAlias("word.vocabulary", "vocabulary")
                .setFetchMode("word.wordOccurrences", FetchMode.JOIN)
                .createAlias("word.wordOccurrences", "wordOccurrences")
                .setFetchMode("wordOccurrences.document", FetchMode.JOIN)
                .createAlias("wordOccurrences.document", "document")
                .setFetchMode("document.targetClass", FetchMode.JOIN)
                .createAlias("document.targetClass", "targetClass")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("targetClass.id"), "id")
                        .add(Projections.property("targetClass.name"), "name")
                        .add(Projections.groupProperty("targetClass.name"))
                        .add(Projections.groupProperty("targetClass.id")))
                .add(Restrictions.eq("vocabulary.name", vocabulary))
                .setResultTransformer(Transformers.aliasToBean(TargetClass.class));
        List<TargetClass> targetClasses = criteria.list();
        session.close();

        return targetClasses;
    }

    @Override
    public TargetClass save(TargetClass currentClass) {
        logger.info("Start saving class entity " + currentClass);

        Session session = sessionFactory.openSession();
        session.save(currentClass);
        session.close();

        logger.info("Attribute successfully saved.");
        return currentClass;
    }
}
