package ro.ubbcluj.cs.nbc.repository.document;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.ubbcluj.cs.nbc.model.Document;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.Word;
import ro.ubbcluj.cs.nbc.model.WordOccurrence;

import java.util.List;

@Repository
public class DocumentDaoImpl implements DocumentDao {

    private static final Logger logger = LoggerFactory.getLogger(DocumentDaoImpl.class);

    @Autowired
    private SessionFactory sessionFactory;

    public DocumentDaoImpl() {
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Document findOneById(Long id) {
        logger.info("Start fetching all existing documents");

        Session session = sessionFactory.openSession();
        Object entity = session.get(Document.class, id);
        Document document = (Document) entity;
        document.setWordOccurrences(((Document) entity).getWordOccurrences());
        Hibernate.initialize(document.getWordOccurrences());
        session.close();

        return document;
    }

    @Override
    public Document save(Document document,
                         List<WordOccurrence> wordOccurrenceList,
                         TargetClass targetClass) {
        logger.info("Start saving new document " + document);

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.persist(document);
        document.setTargetClass((TargetClass) session.merge(targetClass));
        wordOccurrenceList.forEach(wordOccurrence ->  {
            // attach document
            wordOccurrence.setDocument(document);

            //attach word
            Word attachedWord = (Word) session.merge(wordOccurrence.getWord());
            wordOccurrence.setWord(attachedWord);
        });
        document.setWordOccurrences(wordOccurrenceList);
        session.getTransaction().commit();
        session.close();

        return document;
    }

    @Override
    public Long countAll() {
        logger.info("Start counting all documents");

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Document.class, "document")
                .setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        session.close();

        return count;
    }

    @Override
    public Long countAllWithTargetClass(Long targetClassId) {
        logger.info("Start counting all documents with targetClass id = " + targetClassId);

        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(Document.class, "document")
                .setFetchMode("document.targetClass", FetchMode.JOIN)
                .createAlias("document.targetClass", "targetClass")
                .add(Restrictions.eq("targetClass.id", targetClassId))
                .setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        session.close();

        return count;
    }

}
