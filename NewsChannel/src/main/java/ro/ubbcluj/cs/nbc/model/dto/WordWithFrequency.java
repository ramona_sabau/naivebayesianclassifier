package ro.ubbcluj.cs.nbc.model.dto;

/**
 * Created by ramona on 04.06.2017.
 */
public class WordWithFrequency {

    private Long wordId;

    private String name;

    private Long frequency;

    public WordWithFrequency() {
    }

    public Long getWordId() {
        return wordId;
    }

    public void setWordId(Long wordId) {
        this.wordId = wordId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFrequency() {
        return frequency;
    }

    public void setFrequency(Long frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WordWithFrequency that = (WordWithFrequency) o;

        if (wordId != null ? !wordId.equals(that.wordId) : that.wordId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return frequency != null ? frequency.equals(that.frequency) : that.frequency == null;
    }

    @Override
    public int hashCode() {
        int result = wordId != null ? wordId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (frequency != null ? frequency.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "WordWithFrequency{" +
                "wordId=" + wordId +
                ", name='" + name + '\'' +
                ", frequency=" + frequency +
                '}';
    }
}
