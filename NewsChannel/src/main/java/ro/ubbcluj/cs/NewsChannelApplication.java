package ro.ubbcluj.cs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.EmailNaiveBayesianClassifierApplication;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;

@SpringBootApplication
@EnableJpaRepositories
public class NewsChannelApplication {

	public static void main(String[] args) throws MessagingException, ReaderException, ServiceException, FileNotFoundException {
		ConfigurableApplicationContext context = SpringApplication.run(NewsChannelApplication.class, args);

//		context.getBean(EmailNaiveBayesianClassifierApplication.class).setUpClassifier();
	}
}
