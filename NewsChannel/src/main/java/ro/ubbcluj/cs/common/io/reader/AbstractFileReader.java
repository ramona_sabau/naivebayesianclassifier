package ro.ubbcluj.cs.common.io.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractFileReader<T> implements Reader<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractFileReader.class);

    @Override
    public List<T> readFromFile(String fileName) throws ReaderException {
        List<T> readObjects = new ArrayList<>();

        if (fileName == null) {
            throw new ReaderException("The given file name is not valid.");
        }

        fileName = fileName.trim();

        if (fileName.isEmpty()) {
            throw new ReaderException("The given file name must be provided.");
        }

        List<String> readFileLineByLine;
        try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName), Charset.defaultCharset())) {

            readFileLineByLine = br.lines().collect(Collectors.toList());

            for (String line : readFileLineByLine) {
                readObjects.add(createEntity(line));
            }
        } catch (UncheckedIOException e) {
            return new ArrayList<>();
        } catch (IOException e) {
            if (e instanceof NoSuchFileException) {
                logger.warn("File " + fileName + " could not be found");
            } else {
                throw new ReaderException("Error occurred! " + e.getClass() + ": " + e.getMessage(),
                        e.getCause());
            }
        }

        return readObjects;
    }

    public abstract T createEntity(String line) throws ReaderException;

    public abstract String getSeparator();
}
