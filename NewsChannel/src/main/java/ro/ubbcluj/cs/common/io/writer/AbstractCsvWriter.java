package ro.ubbcluj.cs.common.io.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by ramona on 23.03.2017.
 */
public abstract class AbstractCsvWriter<T> implements Writer<T> {

    private static final Logger log = LoggerFactory.getLogger(AbstractCsvWriter.class);

    @Override
    public void writeInFile(String fileName, List<T> entities, boolean mustOverride) throws WriterException {

        if (fileName == null) {
            String errorMessage = "The given file name is not valid.";
            log.warn(errorMessage);
            throw new WriterException(errorMessage);
        }

        fileName = fileName.trim();

        if (fileName.isEmpty()) {
            String errorMessage = "The given file name must be provided.";
            log.warn(errorMessage);
            throw new WriterException(errorMessage);
        }

        if (entities == null || entities.isEmpty()) {
            log.warn("The list with entities is empty.");
            return;
        }

        List<String> lines = entities.stream()
                .map(this::convertEntityToString)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        Path path = Paths.get(fileName);
        try {
            if (!Files.exists(path)) {
                log.info("Create new file with path: " + path);
                Files.createFile(path);
            }

            if (mustOverride) {
                Files.write(path, lines, Charset.forName("UTF-8"));
            } else {
                Files.write(path, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            throw new WriterException("Error occurred!" + e.getMessage(), e.getCause());
        }
    }

    public abstract String convertEntityToString(T entity);

    public abstract String getSeparator();
}
