package ro.ubbcluj.cs.common.configuration.properties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Created by ramona on 19.04.2017.
 */
public class NBCConfigurationManager {

    private static final String DATA_SET_PATH = "data.set.path";
    private static final String DATA_SET_NAMES_FILENAME = "data.set.names.filename";
    private static final String DATA_SET_FILENAME = "data.set.filename";
    private static final String TRAINING_SET_FILENAME = "training.set.filename";
    private static final String TESTING_SET_FILENAME = "testing.set.filename";
    private static final String DATA_SET_FILE_FORMAT = "data.set.file.format";
    private static final String REPORT_FILENAME = "report.filename";
    private static final String REPORT_FILE_FORMAT = "report.file.format";

    public static final String CONFIG_PROPERTIES_FILE = "training-set.properties";

    private static Properties properties;

    private static void loadProperties() {
        InputStream inputStream = NBCConfigurationManager.class.getClassLoader()
                .getResourceAsStream(CONFIG_PROPERTIES_FILE);
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void storeProperties() {
        File f = new File(CONFIG_PROPERTIES_FILE);
        OutputStream out = null;
        try {
            out = new FileOutputStream(f);
            properties.store(out, "Writing properties");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String getDataSetFileFormat() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_FILE_FORMAT);
    }

    public static String getDataSetFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(DATA_SET_FILENAME);
    }

    public static String getDataSetPath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH);
    }

    public static String getTrainingSetFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(TRAINING_SET_FILENAME);
    }

    public static String getTrainingSetFilename() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TRAINING_SET_FILENAME);
    }

    public static void setTrainingSetFilename(String filename) {
        if (properties == null) {
            loadProperties();
        }

        properties.setProperty(TRAINING_SET_FILENAME, filename);
        storeProperties();
    }

    public static String getTestingSetFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(TESTING_SET_FILENAME);
    }

    public static String getTestingSetFilename() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TESTING_SET_FILENAME);
    }

    public static void setTestingSetFilename(String filename) {
        if (properties == null) {
            loadProperties();
        }

        properties.setProperty(TESTING_SET_FILENAME, filename);
        storeProperties();
    }

    public static String getDataSetNamesFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(DATA_SET_NAMES_FILENAME);
    }

    public static String getReportFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(REPORT_FILENAME);
    }

    public static String getReportFilename() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(REPORT_FILENAME);
    }

    public static void setReportFilename(String filename) {
        if (properties == null) {
            loadProperties();
        }

        properties.setProperty(REPORT_FILENAME, filename);
        storeProperties();
    }

    public static String getReportFileFormat() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(REPORT_FILE_FORMAT);
    }

}