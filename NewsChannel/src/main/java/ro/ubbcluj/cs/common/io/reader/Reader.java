package ro.ubbcluj.cs.common.io.reader;

import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.List;

public interface Reader<T> {
    List<T> readFromFile(String fileName) throws ReaderException, FileNotFoundException, MessagingException;
}
