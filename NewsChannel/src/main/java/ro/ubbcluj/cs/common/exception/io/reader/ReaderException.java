package ro.ubbcluj.cs.common.exception.io.reader;

/**
 * Created by Ramona Sabau on 08.11.2016.
 */
public class ReaderException extends Exception {
    public ReaderException(Exception e) {
        super(e);
    }

    public ReaderException(String message) {
        super(message);
    }

    public ReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
