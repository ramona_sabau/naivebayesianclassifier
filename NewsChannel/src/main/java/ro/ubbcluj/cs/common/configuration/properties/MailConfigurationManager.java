package ro.ubbcluj.cs.common.configuration.properties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by ramona on 14.05.2017.
 */
public class MailConfigurationManager {

    private static final String MAIL_HOST = "mail.host";
    private static final String MAIL_PORT = "mail.port";
    private static final String MAIL_USERNAME = "mail.username";
    private static final String MAIL_PASSWORD = "mail.password";
    private static final String MAIL_PROTOCOL = "mail.protocol";

    public static final String CONFIG_PROPERTIES_FILE = "application.properties";

    private static Properties properties;

    private static void loadProperties() {
        InputStream inputStream = NBCConfigurationManager.class.getClassLoader()
                .getResourceAsStream(CONFIG_PROPERTIES_FILE);
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getMailHost() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(MAIL_HOST);
    }

    public static String getMailPort() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(MAIL_PORT);
    }

    public static String getMailUsername() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(MAIL_USERNAME);
    }

    public static String getMailPassword() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(MAIL_PASSWORD);
    }

    public static String getMailProtocol() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(MAIL_PROTOCOL);
    }

}
