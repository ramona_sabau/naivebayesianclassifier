package ro.ubbcluj.cs.common.processor;

/**
 * Created by ramona on 15.05.2017.
 */
public interface Processor<I, O> {

    O process(I input);
}
