package ro.ubbcluj.cs.news.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull(message = "Username parameter is requested.")
    @NotEmpty(message = "Username cannot be empty.")
    @Length(min = 3, max = 50, message = "Username can have between 3 and 50 characters.")
    private String username;

    @NotNull(message = "Password parameter is requested.")
    @NotEmpty(message = "Password field is missing.")
    @Length(min = 8, max = 20, message = "Password can have between 8 and 20 characters.")
    @JsonIgnore
    private String password;

    @NotNull(message = "First name parameter is requested.")
    @NotEmpty(message = "First name field is missing.")
    @Length(min = 3, max = 50, message = "Street can have between 3 and 100 characters.")
    private String firstName;

    @NotNull(message = "Last name parameter is requested.")
    @NotEmpty(message = "Last name field is missing.")
    @Length(min = 3, max = 50, message = "Last name can have between 3 and 100 characters.")
    private String lastName;

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        return username != null ? username.equals(user.username) : user.username == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
