package ro.ubbcluj.cs.news.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.ubbcluj.cs.news.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByUsername(String username);
}
