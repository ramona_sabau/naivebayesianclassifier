package ro.ubbcluj.cs.news.service.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.news.model.User;
import ro.ubbcluj.cs.news.repository.user.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public User findUserByUsername(String username) throws ServiceException {
        User foundUser = userRepository.findOneByUsername(username);

        if (foundUser == null) {
            String errorMessage = "User with username = " + username + " could not be found.";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage + " Please make sure your username is correct.");
        }

        return foundUser;
    }

    @Override
    public User authenticateUser(String username, String password) throws ServiceException {
        User user = findUserByUsername(username);

        if (user.getPassword().equals(password)) {
            return user;
        } else {
            String errorMessage = "Authentication for user with username " + username + " failed due to incorrect password.";
            logger.warn(errorMessage);
            throw new ServiceException("Password is not correct. Please insert the correct password");
        }
    }
}
