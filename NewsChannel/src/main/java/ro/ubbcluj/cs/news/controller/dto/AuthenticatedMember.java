package ro.ubbcluj.cs.news.controller.dto;

import ro.ubbcluj.cs.news.model.User;

public class AuthenticatedMember {
    private String token;
    private User user;

    public AuthenticatedMember() {
    }

    public AuthenticatedMember(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
