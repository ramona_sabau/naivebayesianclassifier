package ro.ubbcluj.cs.news.service.user;

import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.news.model.User;

public interface UserService {

    User findUserByUsername(String username) throws ServiceException;

    User authenticateUser(String username, String password) throws ServiceException;
}
