package ro.ubbcluj.cs.news.controller;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ro.ubbcluj.cs.common.exception.repository.RepositoryException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;

import javax.xml.ws.Service;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, String> processValidationError(MethodArgumentNotValidException exception) {
        BindingResult bindingResult = exception.getBindingResult();
        Map<String, String> fieldErrors = new HashMap<>();
        for (FieldError error : bindingResult.getFieldErrors()) {
            String currentFieldError = error.getField();
            if (fieldErrors.containsKey(currentFieldError)) {
                StringBuilder messagesForCurrentField = new StringBuilder();
                messagesForCurrentField.append(fieldErrors.get(currentFieldError));
                messagesForCurrentField.append(System.getProperty("line.separator"));
                messagesForCurrentField.append(error.getDefaultMessage());
                fieldErrors.replace(currentFieldError, fieldErrors.get(currentFieldError), messagesForCurrentField.toString());
            } else {
                fieldErrors.put(error.getField(), error.getDefaultMessage());
            }
        }
        return fieldErrors;
    }

    @ExceptionHandler({IllegalArgumentException.class, DataIntegrityViolationException.class, ParseException.class,
            ServiceException.class, RepositoryException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> processExceptions(Exception exception) {
        Map<String, String> errors = new HashMap<>();
        errors.put("error", exception.getMessage());
        return  errors;
    }
}
