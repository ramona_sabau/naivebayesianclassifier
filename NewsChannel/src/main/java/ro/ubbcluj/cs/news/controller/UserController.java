package ro.ubbcluj.cs.news.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.news.controller.dto.AuthenticatedMember;
import ro.ubbcluj.cs.news.controller.dto.PossibleMember;
import ro.ubbcluj.cs.news.model.JWTUser;
import ro.ubbcluj.cs.news.model.User;
import ro.ubbcluj.cs.news.service.security.JWTService;
import ro.ubbcluj.cs.news.service.user.UserService;

@RestController
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private JWTService jwtService;

    @PostMapping(value = "/api/public/login")
    public AuthenticatedMember logIn(@RequestBody PossibleMember possibleMember) throws ServiceException {
        String username = possibleMember.getUsername();
        String password = possibleMember.getPassword();
        log.info("Log in operation initiated for user with username " + username);

        User user = userService.authenticateUser(username, password);
        JWTUser jwtUser = new JWTUser(username);
        String token = jwtService.getToken(jwtUser);
        return new AuthenticatedMember(token, user);
    }
}
