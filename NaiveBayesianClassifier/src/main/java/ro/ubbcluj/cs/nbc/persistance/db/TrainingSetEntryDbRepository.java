package ro.ubbcluj.cs.nbc.persistance.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.model.AttributeValue;
import ro.ubbcluj.cs.nbc.model.TrainingSetEntry;

import java.util.List;

public class TrainingSetEntryDbRepository {

    private static final Logger log = LoggerFactory.getLogger(TrainingSetEntryDbRepository.class);

    private SessionFactory sessionFactory;

    public TrainingSetEntryDbRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<TrainingSetEntry> findAll() {
        log.info("Start fetching all entries from training set");

        Session session = sessionFactory.openSession();
        List<TrainingSetEntry> entries = session.createQuery("FROM TrainingSetEntry ").list();
        session.close();

        return entries;
    }

    public List<TrainingSetEntry> findByTarget(String target) {
        log.info("Start fetching all entries from training set with target = " + target);

        Session session = sessionFactory.openSession();
        String query = "FROM TrainingSetEntry WHERE target = :target";
        List<TrainingSetEntry> entries = session.createQuery(query)
                .setParameter("target", target)
                .list();
        session.close();

        return entries;
    }

    public Long getNumberOfEntities() {
        log.info("Start counting number of entries from training set");

        Session session = sessionFactory.openSession();
        Long count = (Long) session.createQuery("SELECT count(*) FROM TrainingSetEntry")
                .uniqueResult();
        session.close();

        log.info(count + " entries found");
        return count;
    }

    public Long getNumberOfEntities(String target) {
        log.info("Start counting number of entries from training set with target = " + target);

        Session session = sessionFactory.openSession();
        String query = "SELECT count(*) FROM TrainingSetEntry tse WHERE tse.target = :target";
        Long count = (Long) session.createQuery(query)
                .setParameter("target", target)
                .uniqueResult();
        session.close();

        log.info(count + " entries found");
        return count;
    }

    public Long getNumberOfEntries(String target, Long attributeId, String attributeValue) {
        log.info("Start counting number of entries from training set with target = " + target +
                " attributeId = " + attributeId + " value = " + attributeValue);

        Session session = sessionFactory.openSession();
        String query = "SELECT count(*) from TrainingSetEntry t join t.attributeValues av " +
                "WHERE av.value = :attributeValue AND t.target = :target AND av.attribute.id = :attributeId";
        Long count = (Long) session.createQuery(query)
                .setParameter("attributeValue", attributeValue)
                .setParameter("target", target)
                .setParameter("attributeId", attributeId)
                .uniqueResult();
        session.close();

        log.info(count + " entries found");
        return count;
    }

    public void save(TrainingSetEntry entry) {
        log.info("Start saving given entry: " + entry);

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        for (AttributeValue attributeValue : entry.getAttributeValues()) {
            session.save(attributeValue);
        }

        session.save(entry);

        session.getTransaction().commit();
        session.close();

        log.info("Training set entry successfully saved.");
    }


    public List<String> getTargetValues() {
        log.info("Start fetching all unique target values from training set");

        Session session = sessionFactory.openSession();
        String query = "SELECT target FROM TrainingSetEntry group by target";
        List<String> entries = session.createQuery(query).list();
        session.close();

        return entries;
    }
}