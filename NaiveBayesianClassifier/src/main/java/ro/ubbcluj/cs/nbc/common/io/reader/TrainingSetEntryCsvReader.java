package ro.ubbcluj.cs.nbc.common.io.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractCsvReader;

import java.util.Arrays;
import java.util.List;

public class TrainingSetEntryCsvReader extends AbstractCsvReader<List<String>> {

    private static final Logger logger = LoggerFactory.getLogger(TrainingSetEntryCsvReader.class);

    private int numberOfAttributesPerLine;

    public TrainingSetEntryCsvReader() {
        super();
        numberOfAttributesPerLine = 0;
    }

    public int getNumberOfAttributesPerLine() {
        return numberOfAttributesPerLine;
    }

    @Override
    public List<String> createEntity(String line) throws ReaderException {

        if (line == null || line.isEmpty()) {
            String errorMessage = "The line does not contain any values.";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        List<String> attributesFromCurrentLine = Arrays.asList(line.split(SEPARATOR));

        if (numberOfAttributesPerLine == 0) {
            logger.debug("The number of attributes per line is " + attributesFromCurrentLine.size());
            numberOfAttributesPerLine = attributesFromCurrentLine.size();
        }

        if (numberOfAttributesPerLine != attributesFromCurrentLine.size()) {
            String errorMessage = "The line does not contain the proper number of attributes.";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        return attributesFromCurrentLine;
    }
}
