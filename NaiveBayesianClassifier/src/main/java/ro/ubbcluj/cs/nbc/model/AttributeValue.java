package ro.ubbcluj.cs.nbc.model;

import javax.persistence.*;

@Entity
public class AttributeValue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="attribute_id", referencedColumnName = "id")
    private Attribute attribute;

    @Column(nullable = false)
    private String value;

    public AttributeValue() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "AttributeValue{" +
                "id=" + id +
                ", attribute=" + attribute.getName() +
                ", value='" + value + '\'' +
                '}';
    }
}
