package ro.ubbcluj.cs.nbc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.model.Attribute;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeValueDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.TrainingSetEntryDbRepository;
import ro.ubbcluj.cs.nbc.persistance.memory.TrainingFrequencyRepository;

import java.util.HashMap;
import java.util.List;

public class NaiveBayesianClassifierService {

    private static final Logger log = LoggerFactory.getLogger(NaiveBayesianClassifierService.class);

    private TrainingSetEntryDbRepository trainingSetRepository;

    private AttributeDbRepository attributeRepository;

    private AttributeValueDbRepository attributeValueRepository;

    private TrainingFrequencyRepository trainingFrequencyRepository;

    public NaiveBayesianClassifierService(TrainingSetEntryDbRepository trainingSetRepository,
                                          AttributeDbRepository attributeRepository,
                                          AttributeValueDbRepository attributeValueRepository,
                                          TrainingFrequencyRepository trainingFrequencyRepository) {
        this.trainingSetRepository = trainingSetRepository;
        this.attributeRepository = attributeRepository;
        this.attributeValueRepository = attributeValueRepository;
        this.trainingFrequencyRepository = trainingFrequencyRepository;
    }

    public String predictTargetValueForNewInstance(HashMap<String, String> instance) {
        log.info("Get all target attributes.");
        List<String> targetAttributeValues = getTargetAttributeValues();

        log.info("Determine frequency foreach target value of target attribute.");
        HashMap<String, Long> targetAttributeValuesFrequency =
                determineFrequencyForTargetAttributeValues(targetAttributeValues);

        log.info("Determine likelihood probability foreach target value");
        HashMap<String, Float> likelihoodProbabilities = calculateLikelihoodProbabilities(instance,
                targetAttributeValuesFrequency);

        log.info("Determine class prior probability foreach target value");
        HashMap<String, Float> classPriorProbabilities =
                determineClassPriorProbabilities(targetAttributeValuesFrequency);

        log.info("Determine maximum probability");
        return determineMaxProbability(likelihoodProbabilities, classPriorProbabilities);
    }

    public String determineMaxProbability(HashMap<String, Float> likelihoodProbabilities,
                                         HashMap<String, Float> classPriorProbabilities) {

        Float maxProbability = 0F;
        String targetValueFound = "";
        for (String targetValue : likelihoodProbabilities.keySet()) {
            Float likelihoodProbability = likelihoodProbabilities.get(targetValue);
            Float classPriorProbability = classPriorProbabilities.get(targetValue);
            Float probability = likelihoodProbability * classPriorProbability;
            if (probability > maxProbability) {
                maxProbability = probability;
                targetValueFound = targetValue;
            }
        }

        return targetValueFound;
    }

    /**
     * Calculates the prior probability for each target class.
     * The prior probability of a class is calculated based on the following formula:
     * P(C) = number of appearance of class C in training set / total entries from training set;
     * @param targetAttributeValuesFrequency - a list with tuple containing <target_value, target_value_frequency>
     * @return a HashMap containing every target class with prior probability calculated
     */
    public HashMap<String, Float> determineClassPriorProbabilities(HashMap<String, Long> targetAttributeValuesFrequency) {

        Long numberOfEntries = trainingSetRepository.getNumberOfEntities();

        HashMap<String, Float> classPriorProbability = new HashMap<>();
        for(String targetValue : targetAttributeValuesFrequency.keySet()) {
            String frequencyString = targetAttributeValuesFrequency.get(targetValue).toString();
            Float frequency = Float.parseFloat(frequencyString);
            Float classPriorProbabilityForTarget = frequency / numberOfEntries;
            classPriorProbability.put(targetValue, classPriorProbabilityForTarget);
        }

        return classPriorProbability;
    }

    public HashMap<String, Long> determineFrequencyForTargetAttributeValues(List<String> targetAttributeValues) {
        HashMap<String, Long> targetAttributeValuesFrequency = new HashMap<>();

        for (String targetAttributeValue : targetAttributeValues) {
            Long frequency = trainingSetRepository.getNumberOfEntities(targetAttributeValue);
            targetAttributeValuesFrequency.put(targetAttributeValue, frequency);
        }

        return targetAttributeValuesFrequency;
    }

    /**
     * Calculates likelihood probabilities for all target classes for given instance.
     * @param instance - map with tuple <attribute_name, attribute_value> that describe the instance for which target
     *                 class must be predicted
     * @param targetAttributeValuesFrequency - map with tuple <target_value, target_value_frequency_in_training_set>
     * @return map with tuple <target_value, likelihood_probability_for_target_value>
     */
    public HashMap<String, Float> calculateLikelihoodProbabilities(HashMap<String, String> instance,
                                                                    HashMap<String, Long> targetAttributeValuesFrequency) {

        HashMap<String, Float> likelihoodProbabilities = new HashMap<>();

        for (String targetValue : targetAttributeValuesFrequency.keySet()) {

            Long targetFrequency = targetAttributeValuesFrequency.get(targetValue);
            Float likelihoodProbabilityForClass = calculateLikelihoodProbabilityForClass(targetValue, targetFrequency,instance);
            likelihoodProbabilities.put(targetValue, likelihoodProbabilityForClass);
        }

        return likelihoodProbabilities;
    }

    /**
     * Calculates the likelihood probability for given instance of properties having a specific class.
     * Formula: P(X|C) ~= P(x1|C) * P(x2|C) * ... * P(xk|C), where:
     *  - C is the target value for which we calculate the likelihood probability;
     *  - X is the set of pairs attribute-value for which the Naive bayesian classifier needs to classify
     *  - x1, x2, ..., xk - attributes from instance X
     *                 needs to be classified
     * @param targetValue - the class for which the likelihood probability will pe calculated;
     * @param targetFrequency - the number of entries from training set with given class
     * @param instance - map with tuple <attribute_name, attribute_value> that describe the instance for which target
     *                 class must be predicted
     * @return calculated likelihood probability for given target class
     */
    public Float calculateLikelihoodProbabilityForClass(String targetValue,
                                                        Long targetFrequency,
                                                        HashMap<String, String> instance) {
        Float likelihoodProbability = 1F;
        Long mEstimate = 0L;

        for (String attributeName : instance.keySet()) {
            Attribute attribute = attributeRepository.findOneByName(attributeName);
            if (attribute == null) {
                String errorMessage = "No attribute found for given name = " + attributeName;
                log.warn(errorMessage);
                throw new IllegalArgumentException(errorMessage);
            }

            String requestedAttributeValue = instance.get(attributeName);
            float frequency = 0;

            TrainingFrequency cachedTrainingFrequency = trainingFrequencyRepository.findOneWithTargetValueAndAttributeNameAndAttributeValue(
                    targetValue,
                    attribute.getName(),
                    requestedAttributeValue
            );

            if (cachedTrainingFrequency != null) {
                log.info("Frequency for target value = " + targetValue + " and attribute value = " + requestedAttributeValue +
                        " could be determined from cached data");
                frequency = cachedTrainingFrequency.getFrequency();
            } else {
                log.info("Frequency is calculated based on searching in training set.");
                frequency = trainingSetRepository.getNumberOfEntries(targetValue,
                        attribute.getId(),
                        instance.get(attributeName));

                Attribute targetAttribute = attributeRepository.findOneByIsTargetAttribute(true);

                TrainingFrequency trainingFrequency = new TrainingFrequency();
                trainingFrequency.setTarget(targetAttribute);
                trainingFrequency.setTargetValue(targetValue);
                trainingFrequency.setAttribute(attribute);
                trainingFrequency.setAttributeValue(requestedAttributeValue);
                trainingFrequency.setFrequency(frequency);
                trainingFrequencyRepository.save(trainingFrequency);

            }

            if (frequency == 0) {
                mEstimate = attributeValueRepository.getNumberOfAttributeValues(attribute.getId());
                frequency = 1;
            }

            likelihoodProbability = likelihoodProbability * (frequency / (targetFrequency + mEstimate));
            mEstimate = 0L;
        }

        return likelihoodProbability;
    }

    public List<String> getTargetAttributeValues() {
        log.info("Start fetching all target values");
        return trainingSetRepository.getTargetValues();
    }
}
