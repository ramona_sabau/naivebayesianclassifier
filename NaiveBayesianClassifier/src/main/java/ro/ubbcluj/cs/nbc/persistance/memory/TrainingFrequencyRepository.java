package ro.ubbcluj.cs.nbc.persistance.memory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramona on 20.03.2017.
 */
public class TrainingFrequencyRepository {

    private static final Logger log = LoggerFactory.getLogger(TrainingFrequencyRepository.class);

    private List<TrainingFrequency> frequencies;

    public TrainingFrequencyRepository() {
        this.frequencies = new ArrayList<>();
    }

    public void save(TrainingFrequency trainingFrequency) {
        log.info("Start saving new TrainingFrequency object " + trainingFrequency);
        frequencies.add(trainingFrequency);
    }

    public List<TrainingFrequency> findAll() {
        log.info("Start fetching all TrainingFrequencies entities");
        return frequencies;
    }

    public TrainingFrequency findOneWithTargetValueAndAttributeNameAndAttributeValue(String targetValue,
                                                                         String attributeName,
                                                                         String attributeValue) {
        log.info("Start searching for TrainingFrequency with target = " + targetValue
                + " and attribute name = " + attributeName + " and attribute value = "
                + attributeValue);

        return frequencies.stream()
                .filter(trainingFrequency -> trainingFrequency.getTargetValue().equals(targetValue) &&
                        trainingFrequency.getAttribute().getName().equals(attributeName) &&
                        trainingFrequency.getAttributeValue().equals(attributeValue))
                .findFirst()
                .orElse(null);
    }
}
