package ro.ubbcluj.cs.nbc.persistance.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AttributeValueDbRepository {

    private static final Logger log = LoggerFactory.getLogger(AttributeValueDbRepository.class);

    private SessionFactory sessionFactory;

    public AttributeValueDbRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Long getNumberOfAttributeValues(Long attributeId) {
        log.info("Start counting attribute values with id = " + attributeId);

        Session session = sessionFactory.openSession();
        String query = "SELECT value from AttributeValue av " +
        "WHERE av.attribute.id = :attributeId group by av.value";
        List<String> results = session.createQuery(query)
                .setParameter("attributeId", attributeId)
                .list();
        session.close();

        log.info(results.size() + " entries found");
        return (long) results.size();
    }
}
