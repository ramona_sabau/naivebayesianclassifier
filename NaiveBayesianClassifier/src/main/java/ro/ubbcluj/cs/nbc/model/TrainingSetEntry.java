package ro.ubbcluj.cs.nbc.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class TrainingSetEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String target;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="entry_id", referencedColumnName = "id")
    private List<AttributeValue> attributeValues;

    public TrainingSetEntry() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public List<AttributeValue> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<AttributeValue> attributeValues) {
        this.attributeValues = attributeValues;
    }
}
