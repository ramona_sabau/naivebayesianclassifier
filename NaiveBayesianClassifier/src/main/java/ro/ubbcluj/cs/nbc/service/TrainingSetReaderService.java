package ro.ubbcluj.cs.nbc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.nbc.common.io.reader.TrainingSetEntryCsvReader;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.Attribute;
import ro.ubbcluj.cs.nbc.model.AttributeValue;
import ro.ubbcluj.cs.nbc.model.TrainingSetEntry;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.TrainingSetEntryDbRepository;

import java.util.ArrayList;
import java.util.List;

public class TrainingSetReaderService {

    private static final Logger log = LoggerFactory.getLogger(TrainingSetReaderService.class);

    private String attributeValuesFileName;

    private String attributeNameFilename;

    private TrainingSetEntryDbRepository trainingSetEntryDbRepository;

    private AttributeDbRepository attributeDbRepository;

    private Reader<List<String>> reader;

    public TrainingSetReaderService(String attributeNameFilename,
                                    String attributeValuesFileName,
                                    TrainingSetEntryDbRepository trainingSetEntryDbRepository,
                                    AttributeDbRepository attributeDbRepository) {
        this.attributeNameFilename = attributeNameFilename;
        this.attributeValuesFileName = attributeValuesFileName;
        this.trainingSetEntryDbRepository = trainingSetEntryDbRepository;
        this.attributeDbRepository = attributeDbRepository;
        this.reader = new TrainingSetEntryCsvReader();
    }

    public void startImportingTrainingSet() throws ReaderException, ServiceException {
        List<List<String>> attributeNames = reader.readFromFile(attributeNameFilename);
        storeAttributes(attributeNames.get(0));
        List<List<String>> importedTrainingSet = reader.readFromFile(attributeValuesFileName);
        storeAttributeValuesForTrainingData(importedTrainingSet);
    }

    private void storeAttributes(List<String> attributes) {
        log.info("Start adding all attributes from imported data.");

        for (String importedAttribute : attributes) {
            Attribute attribute = new Attribute();
            attribute.setName(importedAttribute);
            attribute.setTargetAttribute(false);

            if (importedAttribute.equals(attributes.get(attributes.size() - 1))) {
                attribute.setTargetAttribute(true);
            }

            attributeDbRepository.save(attribute);
        }
    }

    private void storeAttributeValuesForTrainingData(List<List<String>> importedTrainingSet) throws ServiceException {
        log.info("Start adding all entries from imported data.");

        List<Attribute> attributes = attributeDbRepository.findAll();
        //delete first row that contains only the name of the attributes
        importedTrainingSet.remove(0);

        for (List<String> entry : importedTrainingSet) {
            List<AttributeValue> attributeValues = new ArrayList<>();

            if (entry.size() != attributes.size()) {
                String errorMessage = "Training data entry does not contain all the requested parameters.";
                log.warn(errorMessage);
                throw new ServiceException(errorMessage);
            }

            String target = entry.get(entry.size() - 1);
            for (int i = 0; i < attributes.size() - 1; i++) {
                AttributeValue attributeValue = new AttributeValue();
                attributeValue.setAttribute(attributes.get(i));
                attributeValue.setValue(entry.get(i));

                attributeValues.add(attributeValue);
            }

            TrainingSetEntry trainingSetEntry = new TrainingSetEntry();
            trainingSetEntry.setTarget(target);
            trainingSetEntry.setAttributeValues(attributeValues);
            trainingSetEntryDbRepository.save(trainingSetEntry);
        }
    }
}
