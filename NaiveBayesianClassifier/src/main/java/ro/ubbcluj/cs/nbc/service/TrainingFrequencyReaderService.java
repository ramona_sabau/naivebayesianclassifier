package ro.ubbcluj.cs.nbc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.nbc.common.io.reader.TrainingFrequencyCsvReader;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.Attribute;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;
import ro.ubbcluj.cs.nbc.model.dto.TrainingFrequencyDto;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeDbRepository;
import ro.ubbcluj.cs.nbc.persistance.memory.TrainingFrequencyRepository;

import java.util.List;

/**
 * Created by ramona on 06.04.2017.
 */
public class TrainingFrequencyReaderService {

    private static final Logger log = LoggerFactory.getLogger(TrainingFrequencyReaderService.class);

    private String filename;

    private AttributeDbRepository attributeDbRepository;

    private TrainingFrequencyRepository trainingFrequencyRepository;

    private Reader reader;

    public TrainingFrequencyReaderService(AttributeDbRepository attributeDbRepository,
                                          TrainingFrequencyRepository trainingFrequencyRepository,
                                          String filename) {
        this.attributeDbRepository = attributeDbRepository;
        this.trainingFrequencyRepository = trainingFrequencyRepository;
        this.filename = filename;
        this.reader = new TrainingFrequencyCsvReader();
    }

    public void startReadingTrainingFrequencies() throws ReaderException {
        List<TrainingFrequencyDto> importedTrainingFrequencies = reader.readFromFile(filename);
        storeTrainingFrequencies(importedTrainingFrequencies);
    }

    private void storeTrainingFrequencies(List<TrainingFrequencyDto> importedTrainingFrequencies) {

        if (importedTrainingFrequencies == null || importedTrainingFrequencies.isEmpty()) {
            log.info("No training frequencies imported");
            return;
        }

        for (TrainingFrequencyDto trainingFrequencyDto : importedTrainingFrequencies) {
            TrainingFrequency trainingFrequency = new TrainingFrequency();

            try {
                Attribute targetAttribute = searchAttributeWithName(trainingFrequencyDto.getTargetName());
                trainingFrequency.setTarget(targetAttribute);
                trainingFrequency.setTargetValue(trainingFrequencyDto.getTargetValue());
                Attribute attribute = searchAttributeWithName(trainingFrequencyDto.getAttributeName());
                trainingFrequency.setAttribute(attribute);
                trainingFrequency.setAttributeValue(trainingFrequencyDto.getAttributeValue());
                trainingFrequency.setFrequency(trainingFrequencyDto.getFrequency());

                trainingFrequencyRepository.save(trainingFrequency);
            } catch (ServiceException e) {
                log.warn(e.getMessage());
            }
        }
    }

    private Attribute searchAttributeWithName(String name) throws ServiceException {
        Attribute attribute = attributeDbRepository.findOneByName(name);

        if (attribute == null) {
            throw new ServiceException("No attribute found with name = " + name);
        }

        return attribute;
    }
}
