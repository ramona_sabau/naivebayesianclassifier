package ro.ubbcluj.cs.nbc.common.io.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.io.writer.AbstractCsvWriter;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;

public class TrainingFrequencyCsvWriter extends AbstractCsvWriter<TrainingFrequency> {

    private static final Logger logger = LoggerFactory.getLogger(TrainingFrequencyCsvWriter.class);

    public TrainingFrequencyCsvWriter() {
        super();
    }

    @Override
    public String convertEntityToString(TrainingFrequency entity) {

        if (entity == null) {
            logger.warn("Given TrainingFrequency object cannot be converted to String because it is null");
            return null;
        }

        StringBuilder stringBuilder = new StringBuilder();

        if (entity.getTarget() == null || entity.getTarget().getName() == null) {
            logger.warn("Given TrainingFrequency object cannot be converted to String because target property" +
                    " is null.");
            return null;
        }

        if (entity.getTargetValue() == null) {
            logger.warn("Given TrainingFrequency object cannot be converted to String because target value property" +
                    " is null.");
            return null;
        }

        if (entity.getAttribute() == null || entity.getAttribute().getName() == null) {
            logger.warn("Given TrainingFrequency object cannot be converted to String because attribute property" +
                    " is null.");
            return null;
        }

        if (entity.getAttributeValue() == null) {
            logger.warn("Given TrainingFrequency object cannot be converted to String because attribute value " +
                    "property is null.");
            return null;
        }

        stringBuilder.append(entity.getTarget().getName()).append(SEPARATOR)
                .append(entity.getTargetValue()).append(SEPARATOR)
                .append(entity.getAttribute().getName()).append(SEPARATOR)
                .append(entity.getAttributeValue()).append(SEPARATOR)
                .append(entity.getFrequency());

        return stringBuilder.toString();
    }
}
