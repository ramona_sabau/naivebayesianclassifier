package ro.ubbcluj.cs.nbc.common.io.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractCsvReader;
import ro.ubbcluj.cs.nbc.model.dto.TrainingFrequencyDto;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ramona on 06.04.2017.
 */
public class TrainingFrequencyCsvReader extends AbstractCsvReader<TrainingFrequencyDto> {

    private static final Logger logger = LoggerFactory.getLogger(TrainingFrequencyCsvReader.class);

    private int numberOfAttributesPerLine;

    public TrainingFrequencyCsvReader() {
        super();
        this.numberOfAttributesPerLine = TrainingFrequencyDto.class.getDeclaredFields().length;
    }

    @Override
    public TrainingFrequencyDto createEntity(String line) throws ReaderException {

        if (line == null || line.isEmpty()) {
            String errorMessage = "The line does not contain any values.";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        List<String> attributesFromCurrentLine = Arrays.asList(line.split(SEPARATOR));

        if (numberOfAttributesPerLine != attributesFromCurrentLine.size()) {
            String errorMessage = "The line does not contain the proper number of attributes.";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        TrainingFrequencyDto trainingFrequency = new TrainingFrequencyDto();
        trainingFrequency.setTargetName(attributesFromCurrentLine.get(0));
        trainingFrequency.setTargetValue(attributesFromCurrentLine.get(1));
        trainingFrequency.setAttributeName(attributesFromCurrentLine.get(2));
        trainingFrequency.setAttributeValue(attributesFromCurrentLine.get(3));
        trainingFrequency.setFrequency(Float.parseFloat(attributesFromCurrentLine.get(4)));

        return trainingFrequency;
    }
}
