package ro.ubbcluj.cs.nbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.configuration.db.HibernateUtil;
import ro.ubbcluj.cs.common.configuration.properties.ConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeValueDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.TrainingSetEntryDbRepository;
import ro.ubbcluj.cs.nbc.persistance.memory.TrainingFrequencyRepository;
import ro.ubbcluj.cs.nbc.service.NaiveBayesianClassifierService;
import ro.ubbcluj.cs.nbc.service.TrainingFrequencyReaderService;
import ro.ubbcluj.cs.nbc.service.TrainingFrequencyWriterService;
import ro.ubbcluj.cs.nbc.service.TrainingSetReaderService;

import java.util.HashMap;
import java.util.List;

public class NaiveBayesianClassifierApplication {

    private static final Logger log = LoggerFactory.getLogger(NaiveBayesianClassifierApplication.class);

    private NaiveBayesianClassifierService classifierService;

    private TrainingFrequencyReaderService trainingFrequencyReaderService;

    private TrainingFrequencyWriterService trainingFrequencyWriterService;

    private TrainingSetReaderService trainingSetReaderService;

    public NaiveBayesianClassifierApplication() throws ServiceException, ReaderException {
        setUpClassifier();
        prepareTrainingData();
    }

    private void setUpClassifier() {

        TrainingSetEntryDbRepository trainingSetEntryDbRepository = new TrainingSetEntryDbRepository(
                HibernateUtil.getSessionFactory());

        AttributeDbRepository attributeDbRepository = new AttributeDbRepository(
                HibernateUtil.getSessionFactory());

        AttributeValueDbRepository attributeValueDbRepository = new AttributeValueDbRepository(
                HibernateUtil.getSessionFactory()
        );

        TrainingFrequencyRepository trainingFrequencyRepository = new TrainingFrequencyRepository();


        String trainingSetAttributeNamesPath = ConfigurationManager.getDataSetNamesFilePath();
        String trainingSetFilePath = ConfigurationManager.getTrainingSetFilePath();

        this.trainingSetReaderService = new TrainingSetReaderService(
                trainingSetAttributeNamesPath,
                trainingSetFilePath,
                trainingSetEntryDbRepository,
                attributeDbRepository);

        String trainingFrequenciesFilename = ConfigurationManager.getTrainingSetFrequenciesFilePath();

        this.trainingFrequencyReaderService = new TrainingFrequencyReaderService(
                attributeDbRepository,
                trainingFrequencyRepository,
                trainingFrequenciesFilename
        );

        this.trainingFrequencyWriterService = new TrainingFrequencyWriterService(
                trainingFrequencyRepository,
                trainingFrequenciesFilename
        );

        this.classifierService = new NaiveBayesianClassifierService(
                trainingSetEntryDbRepository,
                attributeDbRepository,
                attributeValueDbRepository,
                trainingFrequencyRepository
        );
    }

    private void prepareTrainingData() throws ServiceException, ReaderException {
        log.info("Start preparing training set");
        trainingSetReaderService.startImportingTrainingSet();
        trainingFrequencyReaderService.startReadingTrainingFrequencies();
    }

    public String classifyInstance(HashMap<String, String> instance) {
        log.info("Start classify instance");
        String predictedClass = classifierService.predictTargetValueForNewInstance(instance);

        log.info("Class predicted: " + predictedClass);
        return predictedClass;
    }

    public List<String> getAvailableTargetValues() {
        log.info("Retrieving all existing target values");
        return classifierService.getTargetAttributeValues();
    }

    public void saveAndCloseClassifier() throws WriterException {
        log.info("Start saving all used data for training");
        trainingFrequencyWriterService.storeTrainingFrequencies();

        log.info("Close session for database");
        HibernateUtil.shutDown();
    }
}
