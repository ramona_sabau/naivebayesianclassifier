package ro.ubbcluj.cs.nbc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.common.io.writer.TrainingFrequencyCsvWriter;
import ro.ubbcluj.cs.common.io.writer.Writer;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;
import ro.ubbcluj.cs.nbc.persistance.memory.TrainingFrequencyRepository;

import java.util.List;

/**
 * Created by ramona on 06.04.2017.
 */
public class TrainingFrequencyWriterService {

    private static final Logger log = LoggerFactory.getLogger(TrainingFrequencyWriterService.class);

    private String filename;

    private TrainingFrequencyRepository trainingFrequencyRepository;

    private Writer writer;

    public TrainingFrequencyWriterService(TrainingFrequencyRepository trainingFrequencyRepository,
                                          String filename) {
        this.trainingFrequencyRepository = trainingFrequencyRepository;
        this.filename = filename;
        this.writer = new TrainingFrequencyCsvWriter();
    }

    public void storeTrainingFrequencies() throws WriterException {
        log.info("Start storing all training frequencies in file = " + filename);

        List<TrainingFrequency> trainingFrequencies = trainingFrequencyRepository.findAll();

        if (trainingFrequencies == null || trainingFrequencies.isEmpty()) {
            log.info("No data to write");
        } else {
            writer.writeInFile(filename, trainingFrequencies, true);
            log.info("All training frequencies were exported in file = " + filename);
        }
    }
}
