package ro.ubbcluj.cs.nbc.model.dto;

/**
 * Created by ramona on 06.04.2017.
 */
public class TrainingFrequencyDto {

    private String targetName;
    private String targetValue;
    private String attributeName;
    private String attributeValue;
    private float frequency;

    public TrainingFrequencyDto() {
    }

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrainingFrequencyDto that = (TrainingFrequencyDto) o;

        if (Float.compare(that.frequency, frequency) != 0) return false;
        if (targetName != null ? !targetName.equals(that.targetName) : that.targetName != null) return false;
        if (targetValue != null ? !targetValue.equals(that.targetValue) : that.targetValue != null) return false;
        if (attributeName != null ? !attributeName.equals(that.attributeName) : that.attributeName != null)
            return false;
        return attributeValue != null ? attributeValue.equals(that.attributeValue) : that.attributeValue == null;
    }

    @Override
    public int hashCode() {
        int result = targetName != null ? targetName.hashCode() : 0;
        result = 31 * result + (targetValue != null ? targetValue.hashCode() : 0);
        result = 31 * result + (attributeName != null ? attributeName.hashCode() : 0);
        result = 31 * result + (attributeValue != null ? attributeValue.hashCode() : 0);
        result = 31 * result + (frequency != +0.0f ? Float.floatToIntBits(frequency) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TrainingFrequencyDto{" +
                "targetName='" + targetName + '\'' +
                ", targetValue='" + targetValue + '\'' +
                ", attributeName='" + attributeName + '\'' +
                ", attributeValue='" + attributeValue + '\'' +
                ", frequency=" + frequency +
                '}';
    }
}
