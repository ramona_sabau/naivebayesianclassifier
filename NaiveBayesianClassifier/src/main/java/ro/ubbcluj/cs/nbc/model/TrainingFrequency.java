package ro.ubbcluj.cs.nbc.model;

public class TrainingFrequency {

    private Attribute target;
    private String targetValue;
    private Attribute attribute;
    private String attributeValue;
    private float frequency;

    public TrainingFrequency() {
    }

    public Attribute getTarget() {
        return target;
    }

    public void setTarget(Attribute target) {
        this.target = target;
    }

    public String getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrainingFrequency that = (TrainingFrequency) o;

        if (Float.compare(that.frequency, frequency) != 0) return false;
        if (target != null ? !target.equals(that.target) : that.target != null) return false;
        if (targetValue != null ? !targetValue.equals(that.targetValue) : that.targetValue != null) return false;
        if (attribute != null ? !attribute.equals(that.attribute) : that.attribute != null) return false;
        return attributeValue != null ? attributeValue.equals(that.attributeValue) : that.attributeValue == null;
    }

    @Override
    public int hashCode() {
        int result = target != null ? target.hashCode() : 0;
        result = 31 * result + (targetValue != null ? targetValue.hashCode() : 0);
        result = 31 * result + (attribute != null ? attribute.hashCode() : 0);
        result = 31 * result + (attributeValue != null ? attributeValue.hashCode() : 0);
        result = 31 * result + (frequency != +0.0f ? Float.floatToIntBits(frequency) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TrainingFrequency{" +
                "target=" + target +
                ", targetValue='" + targetValue + '\'' +
                ", attribute=" + attribute +
                ", attributeValue='" + attributeValue + '\'' +
                ", frequency=" + frequency +
                '}';
    }
}
