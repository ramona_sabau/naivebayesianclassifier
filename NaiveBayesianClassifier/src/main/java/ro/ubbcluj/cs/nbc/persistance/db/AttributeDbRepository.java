package ro.ubbcluj.cs.nbc.persistance.db;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.model.Attribute;

import java.util.List;

public class AttributeDbRepository {

    private static final Logger log = LoggerFactory.getLogger(AttributeDbRepository.class);

    private SessionFactory sessionFactory;

    public AttributeDbRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Attribute> findAll() {
        log.info("Start fetching all attributes");

        Session session = sessionFactory.openSession();
        List<Attribute> attributes = session.createQuery("FROM Attribute").list();
        session.close();

        return attributes;
    }

    public Attribute findOneByName(String name) {
        log.info("Start finding attribute with given name = " + name);

        Session session = sessionFactory.openSession();
        String query = "FROM Attribute a WHERE a.name = :name";
        List<Attribute> attribute = session.createQuery(query)
                .setParameter("name", name)
                .setMaxResults(1)
                .list();
        session.close();

        if (attribute == null || attribute.isEmpty())
            return null;
        else
            return attribute.get(0);
    }

    public Attribute findOneByIsTargetAttribute(boolean isTargetAttribute) {
        log.info("Start finding attribute by target attribute");

        Session session = sessionFactory.openSession();
        String query = "FROM Attribute a WHERE a.isTargetAttribute = " + isTargetAttribute;
        List<Attribute> attribute = session.createQuery(query).setMaxResults(1).list();
        session.close();

        if (attribute == null || attribute.isEmpty())
            return null;
        else
            return attribute.get(0);
    }

    public void save(Attribute attribute) {
        log.info("Start saving given attribute: " + attribute);

        Session session = sessionFactory.openSession();
        session.save(attribute);
        session.close();

        log.info("Attribute successfully saved.");
    }
}
