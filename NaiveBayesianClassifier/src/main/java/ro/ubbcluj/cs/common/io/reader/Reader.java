package ro.ubbcluj.cs.common.io.reader;

import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;

import java.util.List;

public interface Reader<T> {
    List<T> readFromFile(String fileName) throws ReaderException;
}
