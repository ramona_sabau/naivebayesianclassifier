package ro.ubbcluj.cs.common.exception.repository;

/**
 * Created by Ramona Sabau on 08.11.2016.
 */
public class RepositoryException extends Exception {
    public RepositoryException(Exception e) {
        super(e);
    }

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }
}
