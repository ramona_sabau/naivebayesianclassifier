package ro.ubbcluj.cs.common.io.writer;

import ro.ubbcluj.cs.common.exception.io.writer.WriterException;

import java.util.List;

/**
 * Created by ramona on 20.03.2017.
 */
public interface Writer<T> {
    void writeInFile(String fileName, List<T> entities, boolean mustOverride) throws WriterException;
}
