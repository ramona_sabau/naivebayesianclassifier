package ro.ubbcluj.cs.common.configuration.properties;

import java.io.*;
import java.util.Properties;

/**
 * Created by ramona on 19.04.2017.
 */
public class ConfigurationManager {

    private static final String DATA_SET_NAME = "data.set.name";
    private static final String FILES_FORMAT = "data.set.file.format";
    private static final String DATA_SET_PATH = "data.set.path";
    private static final String DATA_SET_NAMES_FILENAME = "data.set.names.filename";
    private static final String DATA_SET_FILENAME = "data.set.filename";

    private static final String TRAINING_SET_PATH = "training.set.path";
    private static final String TRAINING_SET_FILENAME = "training.set.filename";

    private static final String TRAINING_SET_FREQUECIES_PATH = "training.set.frequencies.path";
    private static final String TRAINING_SET_FREQUENCIES_FILENAME = "training.set.frequencies.filename";

    private static final String TESTING_SET_PATH = "testing.set.path";
    private static final String TESTING_SET_FILENAME = "testing.set.filename";

    private static final String REPORT_PATH = "report.path";
    public static final String CONFIG_PROPERTIES_FILE = "config.properties";

    private static Properties properties;

    private static void loadProperties() {
        InputStream inputStream = ConfigurationManager.class.getClassLoader()
                .getResourceAsStream(CONFIG_PROPERTIES_FILE);
        properties = new Properties();
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void storeProperties() {
        File f = new File(CONFIG_PROPERTIES_FILE);
        OutputStream out = null;
        try {
            out = new FileOutputStream(f);
            properties.store(out, "Writing properties");
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String getDataSetName() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_NAME);
    }

    public static String getFilesFormat() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(FILES_FORMAT);
    }

    public static String getDataSetFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(DATA_SET_FILENAME);
    }

    public static String getDataSetNamesFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(DATA_SET_PATH) + properties.get(DATA_SET_NAMES_FILENAME);
    }

    public static String getTrainingSetFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TRAINING_SET_PATH) + properties.get(TRAINING_SET_FILENAME);
    }

    public static String getTrainingSetPath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TRAINING_SET_PATH);
    }

    public static String getTrainingSetFilename() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TRAINING_SET_FILENAME);
    }

    public static void setTrainingSetFilename(String filename) {
        if (properties == null) {
            loadProperties();
        }

        properties.setProperty(TRAINING_SET_FILENAME, filename);
        storeProperties();
    }

    public static String getTrainingSetFrequenciesFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TRAINING_SET_FREQUECIES_PATH) + properties.get(TRAINING_SET_FREQUENCIES_FILENAME);
    }

    public static void setTrainingSetFrequenciesFilename(String filename) {
        if (properties == null) {
            loadProperties();
        }

        properties.setProperty(TRAINING_SET_FREQUENCIES_FILENAME, filename);
        storeProperties();
    }

    public static String getTestingSetFilePath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TESTING_SET_PATH) + properties.get(TESTING_SET_FILENAME);
    }

    public static String getTestingSetPath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TESTING_SET_PATH);
    }

    public static String getTestingSetFilename() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(TESTING_SET_FILENAME);
    }

    public static void setTestingSetFilename(String filename) {
        if (properties == null) {
            loadProperties();
        }

        properties.setProperty(TESTING_SET_FILENAME, filename);
        storeProperties();
    }

    public static String getReportPath() {
        if (properties == null) {
            loadProperties();
        }

        return (String) properties.get(REPORT_PATH);
    }

}
