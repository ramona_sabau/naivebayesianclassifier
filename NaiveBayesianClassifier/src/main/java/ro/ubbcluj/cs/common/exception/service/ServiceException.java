package ro.ubbcluj.cs.common.exception.service;

/**
 * Created by Ramona Sabau on 08.11.2016.
 */
public class ServiceException extends Exception {
    public ServiceException(Exception e) {
        super(e);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
