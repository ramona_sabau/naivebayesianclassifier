package ro.ubbcluj.cs.nbctest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbctest.persistance.AttributeNamesRepository;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;
import ro.ubbcluj.cs.nbctest.service.ClassificationMetricsService;
import ro.ubbcluj.cs.nbctest.service.DataSetSplitterService;
import ro.ubbcluj.cs.nbctest.service.ReportEntriesWriterService;
import ro.ubbcluj.cs.nbctest.service.TestingSetEntryReaderService;

/**
 * Created by ramona on 19.04.2017.
 */
public class NaiveBayesianClassifierTestApplication {

    private static final Logger log = LoggerFactory.getLogger(NaiveBayesianClassifierTestApplication.class);

    public static void main(String[] args) {
        NaiveBayesianClassifierTestApplication testApplication = new NaiveBayesianClassifierTestApplication();
        testApplication.startTestingNaiveBayesianClassifier();
    }

    public void startTestingNaiveBayesianClassifier() {

        log.info("Start testing k partitions of initial data set");
        AttributeNamesRepository attributeNamesRepository = new AttributeNamesRepository();
        DataSetSplitterService dataSetSplitterService = new DataSetSplitterService();
        TestingSetEntryReaderService testingSetEntryReaderService =
                new TestingSetEntryReaderService(attributeNamesRepository);
        try {
            int kCrossValidationNumber = 10;
            dataSetSplitterService.prepareTrainingAndTestingDataSets(kCrossValidationNumber);
            testingSetEntryReaderService.importAttributeNames();

//            for (int i = 1; i <= kCrossValidationNumber; i++) {
                ReportEntriesRepository reportEntriesRepository = new ReportEntriesRepository();
                ClassificationMetricsService service = new ClassificationMetricsService(testingSetEntryReaderService,
                        reportEntriesRepository, attributeNamesRepository);
                service.generateReportForTestPartition(3);
                ReportEntriesWriterService reportEntriesWriterService = new ReportEntriesWriterService(reportEntriesRepository);
                reportEntriesWriterService.writeReport(3);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
