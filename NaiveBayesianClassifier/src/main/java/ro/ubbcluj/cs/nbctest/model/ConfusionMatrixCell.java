package ro.ubbcluj.cs.nbctest.model;

/**
 * Created by ramona on 19.04.2017.
 */
public class ConfusionMatrixCell {

    private String actual;
    private String prediction;
    private int value;

    public ConfusionMatrixCell() {
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getPrediction() {
        return prediction;
    }

    public void setPrediction(String prediction) {
        this.prediction = prediction;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ConfusionMatrixCell{" +
                "actual='" + actual +
                ", prediction='" + prediction +
                ", value=" + value +
                '}';
    }
}
