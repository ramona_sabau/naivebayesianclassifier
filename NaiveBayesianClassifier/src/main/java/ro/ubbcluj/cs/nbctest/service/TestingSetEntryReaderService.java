package ro.ubbcluj.cs.nbctest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.configuration.properties.ConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.nbctest.common.io.reader.TestingSetEntryCsvReader;
import ro.ubbcluj.cs.nbctest.persistance.AttributeNamesRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ramona on 27.04.2017.
 */
public class TestingSetEntryReaderService {

    private static final Logger logger = LoggerFactory.getLogger(TestingSetEntryReaderService.class);

    private AttributeNamesRepository attributeNamesRepository;

    private Reader<List<String>> reader;

    public TestingSetEntryReaderService(AttributeNamesRepository attributeNamesRepository) {
        this.attributeNamesRepository = attributeNamesRepository;
        this.reader = new TestingSetEntryCsvReader();
    }

    public void importAttributeNames() throws ReaderException, ServiceException {
        logger.info("Start importing attribute names for training set");
        String filename = ConfigurationManager.getDataSetNamesFilePath();
        List<List<String>> attributeNames = reader.readFromFile(filename);

        attributeNames.get(0).forEach(attributeName -> attributeNamesRepository.save(attributeName));
    }

    public List<HashMap<String, String>> importInstances() throws ReaderException, ServiceException {
        logger.info("Start importing attribute values and create instances for testing set");
        String filename = ConfigurationManager.getTestingSetFilePath();
        List<List<String>> attributeValues = reader.readFromFile(filename);
        if (attributeValues == null || attributeValues.isEmpty()) {
            String errorMessage = "The file = " + filename + " does not contain the attribute names for data set";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage);
        }

        List<String> attributeNames = attributeNamesRepository.getAttributeNames();
        if (attributeNames == null) {
            logger.warn("Attribute names are not read. Start reading all attribute names");
            importAttributeNames();
            attributeNames = attributeNamesRepository.getAttributeNames();
        }

        List<HashMap<String, String>> instances = new ArrayList<>();

        for (List<String> attributeValuesForInstance : attributeValues) {
            instances.add(buildInstance(attributeNames, attributeValuesForInstance));
        }

        return instances;
    }

    private HashMap<String, String> buildInstance(List<String> attributeNames, List<String> attributeValues) {
        HashMap<String, String> instance = new HashMap<>();
        int index = 0;
        for (String attributeName : attributeNames) {
            String attributeValue = attributeValues.get(index);
            instance.put(attributeName, attributeValue);
            index++;
        }
        return instance;
    }
}
