package ro.ubbcluj.cs.nbctest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.configuration.properties.ConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.io.writer.Writer;
import ro.ubbcluj.cs.nbctest.common.io.writer.ReportCsvWriter;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;

/**
 * Created by ramona on 27.04.2017.
 */
public class ReportEntriesWriterService {

    private static final Logger log = LoggerFactory.getLogger(ReportEntriesWriterService.class);

    private ReportEntriesRepository reportEntriesRepository;

    private Writer<String> reportWriter;

    public ReportEntriesWriterService(ReportEntriesRepository reportEntriesRepository) {
        this.reportEntriesRepository = reportEntriesRepository;
        this.reportWriter = new ReportCsvWriter();
    }

    public void writeReport(int partitionNumber) throws WriterException {
        log.info("Start writing report for test partition " + partitionNumber);
        String filename = ConfigurationManager.getReportPath() + ConfigurationManager.getDataSetName() + partitionNumber
                + ConfigurationManager.getFilesFormat();
        reportWriter.writeInFile(filename, reportEntriesRepository.getReport(), true);
    }
}
