package ro.ubbcluj.cs.nbctest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.configuration.properties.ConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.NaiveBayesianClassifierApplication;
import ro.ubbcluj.cs.nbctest.model.ConfusionMatrixCell;
import ro.ubbcluj.cs.nbctest.persistance.AttributeNamesRepository;
import ro.ubbcluj.cs.nbctest.persistance.ConfusionMatrixRepository;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ramona on 19.04.2017.
 */
public class ClassificationMetricsService {

    private static final Logger log = LoggerFactory.getLogger(ClassificationMetricsService.class);

    private TestingSetEntryReaderService testingSetEntryReaderService;

    private ConfusionMatrixRepository confusionMatrixRepository;

    private ReportEntriesRepository reportEntriesRepository;

    private AttributeNamesRepository attributeNamesRepository;

    private String reportFileName;

    private NaiveBayesianClassifierApplication application;

    public ClassificationMetricsService(TestingSetEntryReaderService testingSetEntryReaderService,
                                        ReportEntriesRepository reportEntriesRepository,
                                        AttributeNamesRepository attributeNamesRepository) {
        this.testingSetEntryReaderService = testingSetEntryReaderService;
        this.confusionMatrixRepository = new ConfusionMatrixRepository();
        this.reportEntriesRepository = reportEntriesRepository;
        this.attributeNamesRepository = attributeNamesRepository;
    }

    public void generateReportForTestPartition(int partitionNumber) throws ServiceException,
            ReaderException, WriterException {
        log.info("Start generating report for current partition");
        setUpNaiveBayesianClassifier(partitionNumber);
        List<HashMap<String, String>> instances = prepareTestInstances();
        generateConfusionMatrix();
        startCalculatingConfusionMatrixForTestPartition(instances);
        createReportForTestPartition();
        application.saveAndCloseClassifier();
    }

    private void setUpNaiveBayesianClassifier(int partitionIdentifier) throws ServiceException, ReaderException {
        log.info("Start making configurations for bayesian ");

        String dataSetName = ConfigurationManager.getDataSetName();
        String fileFormat = ConfigurationManager.getFilesFormat();

        log.info("Set filename for training set");
        ConfigurationManager.setTrainingSetFilename(dataSetName + partitionIdentifier + fileFormat);

        log.info("Set filename for training frequencies");
        ConfigurationManager.setTrainingSetFrequenciesFilename(dataSetName + partitionIdentifier + fileFormat);

        log.info("Set filename for testing set");
        ConfigurationManager.setTestingSetFilename(dataSetName + partitionIdentifier + fileFormat);

        log.info("Create the file path for report file");
        this.reportFileName = ConfigurationManager.getReportPath() + dataSetName + partitionIdentifier
                + fileFormat;

        log.info("Set up the NBC application");
        this.application = new NaiveBayesianClassifierApplication();
    }

    private List<HashMap<String, String>> prepareTestInstances() throws ReaderException, ServiceException {
        log.info("Start reading all test instances");
        return testingSetEntryReaderService.importInstances();
    }

    private void generateConfusionMatrix() {
        log.info("Start generating cells for confusion matrix");
        List<String> attributeValues = application.getAvailableTargetValues();
        for (String actual : attributeValues) {
            for (String prediction : attributeValues) {
                ConfusionMatrixCell cell = new ConfusionMatrixCell();
                cell.setActual(actual);
                cell.setPrediction(prediction);
                cell.setValue(0);

                confusionMatrixRepository.save(cell);
            }
        }
    }

    private void startCalculatingConfusionMatrixForTestPartition(List<HashMap<String, String>> instances) {
        log.info("Start calculating confusion matrix for test partition");
        for (HashMap<String, String> instance : instances) {
            String targetAttributeName = attributeNamesRepository.getTargetAttributeName();
            String actualTargetValue = instance.get(targetAttributeName);
            instance.remove(targetAttributeName);

            String predictedTargetClass = application.classifyInstance(instance);
            if (predictedTargetClass.equals(actualTargetValue)) {
                log.info("Predicted class is correct. Predicted class is " + predictedTargetClass);
            } else {
                log.info("Predicted class is not correct. Predicted class is " + predictedTargetClass
                        + " and actual class is " + actualTargetValue);
            }

            ConfusionMatrixCell cell = confusionMatrixRepository.findOneWithActualAndPrediction(actualTargetValue,
                    predictedTargetClass);
            if (cell != null) {
                cell.setValue(cell.getValue() + 1);
            }
        }
    }

    private void createReportForTestPartition() {
        log.info("Start creating report for current partition");

        log.info("Write confusion matrix");
        reportEntriesRepository.save("Confusion matrix" + System.getProperty("line.separator"));
        confusionMatrixRepository.getConfusionMatrix().forEach(confusionMatrixCell ->
                reportEntriesRepository.save(confusionMatrixCell.toString()));

    }

    private double calculatePrecisionForEachClass() {
        return 0;
    }

    private double calculateAccuracy() {
        return 0;
    }

    public double calculateRecallForEachClass() {
        return 0;
    }
}
