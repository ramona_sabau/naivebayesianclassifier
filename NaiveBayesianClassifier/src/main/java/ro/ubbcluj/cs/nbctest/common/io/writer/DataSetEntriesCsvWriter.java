package ro.ubbcluj.cs.nbctest.common.io.writer;

import ro.ubbcluj.cs.common.io.writer.AbstractCsvWriter;

/**
 * Created by ramona on 19.04.2017.
 */
public class DataSetEntriesCsvWriter extends AbstractCsvWriter<String> {

    public DataSetEntriesCsvWriter() {
        super();
    }

    @Override
    public String convertEntityToString(String entity) {
        return entity;
    }
}
