package ro.ubbcluj.cs.nbctest.persistance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramona on 27.04.2017.
 */
public class AttributeNamesRepository {

    private static final Logger log = LoggerFactory.getLogger(AttributeNamesRepository.class);

    private List<String> attributeNames;

    public AttributeNamesRepository() {
        this.attributeNames = new ArrayList<>();
    }

    public List<String> getAttributeNames() {
        log.info("Start fetching attribute names");
        return attributeNames;
    }

    public String getTargetAttributeName() {
        log.info("Start fetching the name for target attribute");
        return attributeNames.get(attributeNames.size() - 1);
    }

    public void save(String attributeName) {
        log.info("Start adding a attribute name");
        attributeNames.add(attributeName);
    }
}
