package ro.ubbcluj.cs.nbctest.common.io.reader;

import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractCsvReader;

/**
 * Created by ramona on 19.04.2017.
 */
public class DataSetEntriesCsvReader extends AbstractCsvReader<String> {

    public DataSetEntriesCsvReader() {
        super();
    }

    @Override
    public String createEntity(String line) throws ReaderException {
        return line;
    }
}
