package ro.ubbcluj.cs.nbctest.service;

import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.configuration.properties.ConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.common.io.writer.Writer;
import ro.ubbcluj.cs.nbctest.common.io.reader.DataSetEntriesCsvReader;
import ro.ubbcluj.cs.nbctest.common.io.writer.DataSetEntriesCsvWriter;

import java.util.List;

/**
 * Created by ramona on 19.04.2017.
 */
public class DataSetSplitterService {

    private static final Logger log = LoggerFactory.getLogger(DataSetSplitterService.class);

    private Reader<String> dataSetReader;

    private Writer<String> dataSetWriter;

    public DataSetSplitterService() {
        this.dataSetReader = new DataSetEntriesCsvReader();
        this.dataSetWriter = new DataSetEntriesCsvWriter();
    }

    public void prepareTrainingAndTestingDataSets(int numberOfPartitions) throws ReaderException, WriterException {
        log.info("Start importing all entries from data set");
        String dataSetFilePath = ConfigurationManager.getDataSetFilePath();
        List<String> dataSet = dataSetReader.readFromFile(dataSetFilePath);

        int partitionSize;
        if (dataSet.size() % numberOfPartitions == 0)
            partitionSize = dataSet.size() / numberOfPartitions;
        else
            partitionSize = dataSet.size() / numberOfPartitions + 1;

        log.info("Start splitting data set into k partitions");
        List<List<String>> partitions = ListUtils.partition(dataSet, partitionSize);

        String dataSetName = ConfigurationManager.getDataSetName();
        String fileFormat = ConfigurationManager.getFilesFormat();
        String trainingSetPath = ConfigurationManager.getTrainingSetPath();
        String testingSetPath = ConfigurationManager.getTestingSetPath();
        int currentPartition = 1;
        for (List<String> partition : partitions) {

            log.info("Start writing partition in corresponding files");
            for (int i = 1; i <= numberOfPartitions; i++) {
                if (currentPartition == i) {
                    log.info("Start writing partition " + currentPartition + " in test set file");
                    String testingFilename = dataSetName + currentPartition + fileFormat;
                    dataSetWriter.writeInFile(testingSetPath + testingFilename,
                            partition, true);
                } else {
                    log.info("Start writing partition " + currentPartition + " in training set number " + i);
                    String trainingFilename = dataSetName + i + fileFormat;
                    dataSetWriter.writeInFile(trainingSetPath + trainingFilename,
                            partition, false);
                }
            }

            currentPartition++;
        }
    }


}
