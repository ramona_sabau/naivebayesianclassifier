package ro.ubbcluj.cs.common.io.reader;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.io.reader.AbstractCsvReader;
import ro.ubbcluj.cs.nbc.common.io.reader.TrainingSetEntryCsvReader;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class AbstractCsvReaderTest {

    private AbstractCsvReader csvReader;

    @Before
    public void setUp() {
        csvReader = new TrainingSetEntryCsvReader();
    }

    @Test
    public void testReadFromFile_fileNameIsNull_exceptionThrown() {
        // Set up
        String fileName = null;
        String expectedErrorMessage = "The given file name is not valid.";

        // Execution
        try {
            List<List<String>> actualTrainingSet = csvReader.readFromFile(fileName);
            fail("Exception is not thrown with message: " + expectedErrorMessage);
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof ReaderException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testReadFromFile_fileNameIsEmpty_exceptionThrown() {
        // Set up
        String fileName = " ";
        String expectedErrorMessage = "The given file name must be provided.";

        // Execution
        try {
            List<List<String>> actualTrainingSet = csvReader.readFromFile(fileName);
            fail("Exception is not thrown.");
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof ReaderException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testReadFromFile_fileExists_dataSetRetrieved() throws ReaderException {
        // Set up
        String fileName = "src/test/resources/playTennisTest.csv";
        List<String> firstList = Arrays.asList("PlayTennis", "Outlook", "Temperature", "Humidity", "Windy");
        List<String> secondList = Arrays.asList("No", "Sunny", "Hot","High", "False");
        List<List<String>> expectedTrainingSet = Arrays.asList(firstList, secondList);

        // Execution
        List<List<String>> actualTrainingSet = csvReader.readFromFile(fileName);

        // Verification
        Assert.assertEquals(expectedTrainingSet.size(), actualTrainingSet.size());
        Assert.assertTrue(EqualsBuilder.reflectionEquals(expectedTrainingSet.get(0), actualTrainingSet.get(0)));
        Assert.assertTrue(EqualsBuilder.reflectionEquals(expectedTrainingSet.get(0), actualTrainingSet.get(0)));
    }
}
