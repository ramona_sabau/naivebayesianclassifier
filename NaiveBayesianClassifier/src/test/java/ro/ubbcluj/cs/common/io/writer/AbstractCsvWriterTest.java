package ro.ubbcluj.cs.common.io.writer;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.nbc.common.io.writer.TrainingFrequencyCsvWriter;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;
import ro.ubbcluj.cs.nbc.model.builder.TrainingFrequencyBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by ramona on 06.04.2017.
 */
public class AbstractCsvWriterTest {

    private AbstractCsvWriter csvWriter;
    private String fileName = "src/test/resources/playTennisTestTrainingFrequencies.csv";

    @Before
    public void setUp() {
        csvWriter = new TrainingFrequencyCsvWriter();
    }

    @Test
    public void testReadFromFile_fileNameIsNull_exceptionThrown() {
        // Set up
        String fileName = null;
        List<TrainingFrequency> entities = new ArrayList<>();
        String expectedErrorMessage = "The given file name is not valid.";

        // Execution
        try {
            csvWriter.writeInFile(fileName, entities, true);
            fail("Exception is not thrown with message: " + expectedErrorMessage);
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof WriterException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testReadFromFile_fileNameIsEmpty_exceptionThrown() {
        // Set up
        String fileName = "    ";
        List<TrainingFrequency> entities = new ArrayList<>();
        String expectedErrorMessage = "The given file name must be provided.";

        // Execution
        try {
            csvWriter.writeInFile(fileName, entities, true);
            fail("Exception is not thrown.");
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof WriterException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testReadFromFile_listProvided_fileCreated() throws ReaderException, WriterException, IOException {
        // Set up
        List<TrainingFrequency> entities = new ArrayList<>();
        TrainingFrequency trainingFrequency = TrainingFrequencyBuilder.defaultTrainingFrequency().build();
        entities.add(trainingFrequency);
        String expectedString = csvWriter.convertEntityToString(trainingFrequency);

        // Execution
        csvWriter.writeInFile(fileName, entities, true);

        // Verification
        File createdFile = new File(fileName);
        assertTrue(createdFile.exists());
        assertTrue(FileUtils.readLines(createdFile, Charset.forName("UTF-8")).contains(expectedString));
    }

    @After
    public void tearDown(){
        File createdFile = new File(fileName);

        if (createdFile.exists() && createdFile.isFile()) {
            createdFile.delete();
        }
    }
}
