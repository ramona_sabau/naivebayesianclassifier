package ro.ubbcluj.cs.nbc.model.builder;

import ro.ubbcluj.cs.nbc.model.Attribute;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;

/**
 * Created by ramona on 06.04.2017.
 */
public class TrainingFrequencyBuilder {

    private static final Attribute TARGET_ATTRIBUTE = AttributeBuilder.defaultTargetAttribute().build();
    private static final String TARGET_VALUE = "Yes";
    private static final Attribute ATTRIBUTE = AttributeBuilder.defaultAttribute().build();
    private static final String ATTRIBUTE_VALUE = "High";
    private static final float FREQUENCY = 5;


    private TrainingFrequency trainingFrequency;

    private TrainingFrequencyBuilder() {
        this.trainingFrequency = new TrainingFrequency();
    }

    public static TrainingFrequencyBuilder defaultTrainingFrequency() {
        TrainingFrequencyBuilder trainingFrequencyBuilder = new TrainingFrequencyBuilder();

        return trainingFrequencyBuilder.withTargetAttribute(TARGET_ATTRIBUTE)
                .withTargetValue(TARGET_VALUE)
                .withAttribute(ATTRIBUTE)
                .withAttributeValue(ATTRIBUTE_VALUE)
                .withFrequency(FREQUENCY);
    }

    public TrainingFrequencyBuilder withTargetAttribute(Attribute targetAttribute) {
        this.trainingFrequency.setTarget(targetAttribute);
        return this;
    }

    public TrainingFrequencyBuilder withTargetValue(String targetValue) {
        this.trainingFrequency.setTargetValue(targetValue);
        return this;
    }

    public TrainingFrequencyBuilder withAttribute(Attribute attribute) {
        this.trainingFrequency.setAttribute(attribute);
        return this;
    }

    public TrainingFrequencyBuilder withAttributeValue(String attributeValue) {
        this.trainingFrequency.setAttributeValue(attributeValue);
        return this;
    }

    public TrainingFrequencyBuilder withFrequency(float frequency) {
        this.trainingFrequency.setFrequency(frequency);
        return this;
    }

    public TrainingFrequency build() {
        return this.trainingFrequency;
    }
}
