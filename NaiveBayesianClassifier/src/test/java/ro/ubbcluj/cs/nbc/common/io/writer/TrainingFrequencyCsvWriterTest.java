package ro.ubbcluj.cs.nbc.common.io.writer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.ubbcluj.cs.nbc.model.Attribute;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;
import ro.ubbcluj.cs.nbc.model.builder.AttributeBuilder;
import ro.ubbcluj.cs.nbc.model.builder.TrainingFrequencyBuilder;

/**
 * Created by ramona on 06.04.2017.
 */
public class TrainingFrequencyCsvWriterTest {

    private TrainingFrequencyCsvWriter trainingFrequencyCsvWriter;

    @Before
    public void setUp() {
        this.trainingFrequencyCsvWriter = new TrainingFrequencyCsvWriter();
    }

    @Test
    public void testConvertToFile_entityIsNull_nullIsRetrieved() {
        // Setup
        TrainingFrequency entity = null;
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_entityWithNullTarget_nullIsRetrieved() {
        // Setup
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withTargetAttribute(null)
                .build();
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_entityWithNullTargetName_nullIsRetrieved() {
        // Setup
        Attribute targetAttribute = AttributeBuilder.defaultTargetAttribute()
                .withName(null)
                .build();
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withTargetAttribute(targetAttribute)
                .build();
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_entityWithNullTargetValue_nullIsRetrieved() {
        // Setup
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withTargetValue(null)
                .build();
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_entityWithNullAttribute_nullIsRetrieved() {
        // Setup
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withAttribute(null)
                .build();
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_entityWithNullAttributeName_nullIsRetrieved() {
        // Setup
        Attribute attribute = AttributeBuilder.defaultAttribute()
                .withName(null)
                .build();
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withAttribute(attribute)
                .build();
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_entityWithNullAttributeValue_nullIsRetrieved() {
        // Setup
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withAttributeValue(null)
                .build();
        String expectedResult = null;

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testConvertToFile_validEntity_stringIsRetrieved() {
        // Setup
        TrainingFrequency entity = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .build();
        String expectedResult = "PlayTennis,Yes,Humidity,High,5.0";

        // Execution
        String actualResult = trainingFrequencyCsvWriter.convertEntityToString(entity);

        // Verification
        Assert.assertEquals(expectedResult, actualResult);
    }
}
