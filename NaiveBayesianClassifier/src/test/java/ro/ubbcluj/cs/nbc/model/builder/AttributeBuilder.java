package ro.ubbcluj.cs.nbc.model.builder;

import ro.ubbcluj.cs.nbc.model.Attribute;

/**
 * Created by ramona on 06.04.2017.
 */
public class AttributeBuilder {

    private static final Long ID = 1L;
    private static final String NAME = "Humidity";
    private static final boolean IS_TARGET = false;

    private static final Long TARGET_ID = 2L;
    private static final String TARGET_NAME = "PlayTennis";
    private static final boolean TARGET_IS_TARGET = true;

    private Attribute attribute;

    private AttributeBuilder() {
        this.attribute = new Attribute();
    }

    public static AttributeBuilder defaultAttribute() {
        AttributeBuilder attributeBuilder = new AttributeBuilder();
        return attributeBuilder.withId(ID)
                .withName(NAME)
                .withIsTarget(IS_TARGET);
    }

    public static AttributeBuilder defaultTargetAttribute() {
        AttributeBuilder attributeBuilder = defaultAttribute();
        return attributeBuilder.withId(TARGET_ID)
                .withName(TARGET_NAME)
                .withIsTarget(TARGET_IS_TARGET);
    }

    public AttributeBuilder withId(Long id) {
        this.attribute.setId(id);
        return this;
    }

    public AttributeBuilder withName(String name) {
        this.attribute.setName(name);
        return this;
    }

    public AttributeBuilder withIsTarget(boolean isTarget) {
        this.attribute.setTargetAttribute(isTarget);
        return this;
    }

    public Attribute build() {
        return this.attribute;
    }
}
