package ro.ubbcluj.cs.nbc.common.io.reader;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.fail;

public class TrainingSetEntryCsvReaderTest {

    public TrainingSetEntryCsvReader reader;

    @Before
    public void setUp() {
        reader = new TrainingSetEntryCsvReader();
    }

    @Test(expected = ReaderException.class)
    public void testCreateEntity_lineIsNull_exceptionThrown() throws ReaderException {
        // Set up
        String line = null;

        //Execution
        reader.createEntity(line);
    }

    @Test(expected = ReaderException.class)
    public void testCreateEntity_emptyLine_exceptionThrown() throws ReaderException {
        // Set up
        String line = "";

        //Execution
        reader.createEntity(line);
    }

    @Test
    public void testCreateEntity_lineWith3Arguments_numberOfAttributesPerLineIs3()
            throws ReaderException {
        // Set up

        String line = "test,test,test";
        int expectedNumberOfAttributes = 3;

        // Execution
        List<String> actualArguments = reader.createEntity(line);

        // Verification
        int actualNumberOfAttributes = reader.getNumberOfAttributesPerLine();
        Assert.assertEquals(expectedNumberOfAttributes, actualNumberOfAttributes);
    }

    @Test
    public void testCreateEntity_lineWith3Arguments_listWith3ArgumentRetrieved() throws ReaderException {
        // Set up
        String line = "test,test,test";
        List<String> expectedArguments = Arrays.asList("test", "test", "test");

        // Execution
        List<String> actualArguments = reader.createEntity(line);

        // Verification
        Assert.assertTrue(EqualsBuilder.reflectionEquals(expectedArguments, actualArguments));
    }

    @Test
    public void testCreateEntity_lineHasMoreArguments_exceptionThrown() {
        // Set up
        String lineWithArguments = "test,test,test";
        String lineWithArgumentValues = "test,test,test,test";
        String expectedErrorMessage = "The line does not contain the proper number of attributes.";

        // Execution
        try {
            reader.createEntity(lineWithArguments);
            reader.createEntity(lineWithArgumentValues);
            fail("Exception is not thrown.");
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof ReaderException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }
}
