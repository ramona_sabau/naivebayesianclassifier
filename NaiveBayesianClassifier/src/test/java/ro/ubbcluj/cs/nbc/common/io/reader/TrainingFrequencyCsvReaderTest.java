package ro.ubbcluj.cs.nbc.common.io.reader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.nbc.model.dto.TrainingFrequencyDto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by ramona on 08.04.2017.
 */
public class TrainingFrequencyCsvReaderTest {

    private static final String TARGET_NAME = "PlayTennis";
    private static final String TARGET_VALUE = "Yes";
    private static final String ATTRIBUTE_NAME = "Humidity";
    private static final String ATTRIBUTE_VALUE = "High";
    private static final float FREQUENCY = 1;
    private static final String SEPARATOR = ",";

    private static final String LINE;

    static {
        StringBuilder lineStringBuilder = new StringBuilder();
        lineStringBuilder.append(TARGET_NAME).append(SEPARATOR)
                .append(TARGET_VALUE).append(SEPARATOR)
                .append(ATTRIBUTE_NAME).append(SEPARATOR)
                .append(ATTRIBUTE_VALUE).append(SEPARATOR)
                .append(FREQUENCY);

        LINE = lineStringBuilder.toString();
    }

    private TrainingFrequencyCsvReader reader;

    @Before
    public void setUp() {
        this.reader = new TrainingFrequencyCsvReader();
    }

    @Test(expected = ReaderException.class)
    public void testCreateEntity_lineIsNull_exceptionThrown() throws ReaderException {
        // Set up
        String line = null;

        // Execution
        reader.createEntity(line);
    }


    @Test(expected = ReaderException.class)
    public void testCreateEntity_emptyLine_exceptionThrown() throws ReaderException {
        // Set up
        String line = "";

        // Execution
        reader.createEntity(line);
    }

    @Test
    public void testCreateEntity_lineHasMoreArguments_exceptionThrown() {
        // Set up
        String line = LINE + SEPARATOR + "test";
        String expectedErrorMessage = "The line does not contain the proper number of attributes.";

        // Execution
        try {
            TrainingFrequencyDto actualTrainingFrequencyDto = reader.createEntity(line);
            fail("Exception is not thrown.");
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof ReaderException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testCreateEntity_lineHasLessArguments_exceptionThrown() {
        // Set up
        String line = LINE.replaceFirst(",", "");
        String expectedErrorMessage = "The line does not contain the proper number of attributes.";

        // Execution
        try {
           reader.createEntity(line);
            fail("Exception is not thrown.");
        } catch (Exception e) {
            // Verification
            Assert.assertTrue(e instanceof ReaderException);
            Assert.assertEquals(expectedErrorMessage, e.getMessage());
        }
    }

    @Test
    public void testCreateEntity_lineWith5Arguments_entityCreated() throws ReaderException {
        // Set up
        TrainingFrequencyDto expectedTrainingFrequencyDto = new TrainingFrequencyDto();
        expectedTrainingFrequencyDto.setTargetName(TARGET_NAME);
        expectedTrainingFrequencyDto.setTargetValue(TARGET_VALUE);
        expectedTrainingFrequencyDto.setAttributeName(ATTRIBUTE_NAME);
        expectedTrainingFrequencyDto.setAttributeValue(ATTRIBUTE_VALUE);
        expectedTrainingFrequencyDto.setFrequency(FREQUENCY);

        // Execution
        TrainingFrequencyDto actualTrainingFrequencyDto = reader.createEntity(LINE);

        // Verification
        assertEquals(expectedTrainingFrequencyDto, actualTrainingFrequencyDto);
    }
}
