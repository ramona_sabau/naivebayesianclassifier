package ro.ubbcluj.cs.nbc.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ro.ubbcluj.cs.nbc.model.Attribute;
import ro.ubbcluj.cs.nbc.model.TrainingFrequency;
import ro.ubbcluj.cs.nbc.model.builder.AttributeBuilder;
import ro.ubbcluj.cs.nbc.model.builder.TrainingFrequencyBuilder;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.AttributeValueDbRepository;
import ro.ubbcluj.cs.nbc.persistance.db.TrainingSetEntryDbRepository;
import ro.ubbcluj.cs.nbc.persistance.memory.TrainingFrequencyRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class NaiveBayesianClassifierServiceTest {

    private static final String TARGET_VALUE_NO = "No";
    private static final String TARGET_VALUE_YES = "Yes";
    private static final Long TARGET_NO_FREQUENCY = 5L;
    private static final Long TARGET_YES_FREQUENCY = 9L;
    private static final Long NUMBER_OF_ENTRIES = 14L;
    private static final Attribute ATTRIBUTE = AttributeBuilder.defaultAttribute().build();
    private static final Attribute TARGET_ATTRIBUTE = AttributeBuilder.defaultTargetAttribute().build();
    private static final TrainingFrequency TRAINING_FREQUENCY = TrainingFrequencyBuilder.defaultTrainingFrequency().build();

    private NaiveBayesianClassifierService service;

    @Mock
    private TrainingSetEntryDbRepository trainingSetRepository = mock(TrainingSetEntryDbRepository.class);

    @Mock
    private AttributeDbRepository attributeRepository;

    @Mock
    private AttributeValueDbRepository attributeValueRepository;

    @Mock
    private TrainingFrequencyRepository trainingFrequencyRepository;

    @Captor
    private ArgumentCaptor<TrainingFrequency> trainingFrequencyCaptor;

    @Before
    public void setUp() {
        service = new NaiveBayesianClassifierService(trainingSetRepository,
                attributeRepository,
                attributeValueRepository,
                trainingFrequencyRepository);
        initMocks(service);
    }

    @Test
    public void testDetermineFrequencyForTargetAttributeValues_noExistingTargetValues_emptyHashMapRetrieved() {
        // Set up
        List<String> targetAttributeValues = new ArrayList<>();

        // Execution
        HashMap<String, Long> actualResult = service.determineFrequencyForTargetAttributeValues(targetAttributeValues);

        // Verification
        Assert.assertEquals(0, actualResult.size());
        verifyZeroInteractions(trainingSetRepository);
    }

    @Test
    public void testDetermineFrequencyForTargetAttributeValues_2ExistingTargetValues_2FrequenciesRetrieved() {
        // Set up
        List<String> targetAttributeValues = Arrays.asList("No", "Yes");
        HashMap<String, Long> expectedResult = new HashMap<>();
        expectedResult.put(TARGET_VALUE_NO, TARGET_NO_FREQUENCY);
        expectedResult.put(TARGET_VALUE_YES, TARGET_YES_FREQUENCY);

        when(trainingSetRepository.getNumberOfEntities(anyString()))
                .thenReturn(5L)
                .thenReturn(9L);

        // Execution
        HashMap<String, Long> actualResult = service.determineFrequencyForTargetAttributeValues(targetAttributeValues);

        // Verification
        Assert.assertEquals(expectedResult.size(), actualResult.size());
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testDetermineClassPriorProbabilities_noTargetValuesWithFrequencies_emptyHashMapRetrieved() {
        // Set up
        HashMap<String, Long> targetAttributeValuesFrequencies = new HashMap<>();

        // Execution
        HashMap<String, Float> actualResult = service.determineClassPriorProbabilities(targetAttributeValuesFrequencies);

        // Verification
        Assert.assertEquals(0, actualResult.size());
    }

    @Test
    public void testDetermineClassPriorProbabilities_2TargetValues_2PriorProbabilitiesCalculated() {
        // Set up
        when(trainingSetRepository.getNumberOfEntities())
                .thenReturn(NUMBER_OF_ENTRIES);

        HashMap<String, Long> targetValuesWithFrequencies = new HashMap<>();
        targetValuesWithFrequencies.put(TARGET_VALUE_NO, TARGET_NO_FREQUENCY);
        targetValuesWithFrequencies.put(TARGET_VALUE_YES, TARGET_YES_FREQUENCY);

        HashMap<String, Float> expectedResult = new HashMap<>();
        expectedResult.put(TARGET_VALUE_NO, Float.parseFloat(TARGET_NO_FREQUENCY.toString()) / NUMBER_OF_ENTRIES);
        expectedResult.put(TARGET_VALUE_YES, Float.parseFloat(TARGET_YES_FREQUENCY.toString()) / NUMBER_OF_ENTRIES);

        // Execution
        HashMap<String, Float> actualResult = service.determineClassPriorProbabilities(targetValuesWithFrequencies);

        // Verification
        Assert.assertEquals(expectedResult.size(), actualResult.size());
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateLikelihoodProbabilityForClass_invalidAttributeInInstance_exceptionThrown() {
        // Set up
        HashMap<String, String> instance = new HashMap<>();
        instance.put("Test","Test");

        when(attributeRepository.findOneByName(anyString())).thenReturn(null);

        // Execution
        service.calculateLikelihoodProbabilityForClass(TARGET_VALUE_NO, TARGET_NO_FREQUENCY, instance);
    }

    @Test
    public void testCalculateLikelihoodProbabilityForClass_frequencyCached_cachedValueUsed() {
        // Set up
        HashMap<String, String> instance = new HashMap<>();
        instance.put(ATTRIBUTE.getName(), TRAINING_FREQUENCY.getAttributeValue());

        when(attributeRepository.findOneByName(anyString())).thenReturn(ATTRIBUTE);
        when(trainingFrequencyRepository.findOneWithTargetValueAndAttributeNameAndAttributeValue(anyString(),
                anyString(), any())).thenReturn(TRAINING_FREQUENCY);

        // Execution
        service.calculateLikelihoodProbabilityForClass(TARGET_VALUE_YES,
                TARGET_YES_FREQUENCY,
                instance);

        // Verification
        verifyZeroInteractions(trainingSetRepository);
        verify(trainingFrequencyRepository, times(0)).save(any());
    }

    @Test
    public void testCalculateLikelihoodProbabilityForClass_frequencyNotCached_frequencyIsCalculatedAndCached() {
        // Set up
        HashMap<String, String> instance = new HashMap<>();
        instance.put(ATTRIBUTE.getName(), TRAINING_FREQUENCY.getAttributeValue());

        when(attributeRepository.findOneByName(anyString())).thenReturn(ATTRIBUTE);
        when(trainingFrequencyRepository.findOneWithTargetValueAndAttributeNameAndAttributeValue(anyString(),
                anyString(), any())).thenReturn(null);
        when(trainingSetRepository.getNumberOfEntries(anyString(), anyLong(), anyString()))
                .thenReturn((long) TRAINING_FREQUENCY.getFrequency());
        when(attributeRepository.findOneByIsTargetAttribute(true)).thenReturn(TARGET_ATTRIBUTE);


        // Execution
        service.calculateLikelihoodProbabilityForClass(TARGET_VALUE_YES,
                TARGET_YES_FREQUENCY,
                instance);

        // Verification
        verify(trainingFrequencyRepository, times(1)).save(trainingFrequencyCaptor.capture());
        Assert.assertEquals(TRAINING_FREQUENCY, trainingFrequencyCaptor.getValue());
    }

    @Test
    public void testCalculateLikelihoodProbabilityForClass_frequencyIsNot0_probabilityCalculated() {
        // Set up
        HashMap<String, String> instance = new HashMap<>();
        instance.put(ATTRIBUTE.getName(), TRAINING_FREQUENCY.getAttributeValue());

        when(attributeRepository.findOneByName(anyString())).thenReturn(ATTRIBUTE);
        when(trainingFrequencyRepository.findOneWithTargetValueAndAttributeNameAndAttributeValue(anyString(),
                anyString(), any())).thenReturn(TRAINING_FREQUENCY);

        Float expectedLikelihoodProbability = TRAINING_FREQUENCY.getFrequency() / TARGET_YES_FREQUENCY;

        // Execution
        Float actualLikelihoodProbability = service.calculateLikelihoodProbabilityForClass(TARGET_VALUE_YES,
                TARGET_YES_FREQUENCY,
                instance);

        // Verification
        Assert.assertEquals(expectedLikelihoodProbability, actualLikelihoodProbability);
    }

    @Test
    public void testCalculateLikelihoodProbabilityForClass_frequencyIs0_probabilityCalculatedWithMEstimate() {
        // Set up
        HashMap<String, String> instance = new HashMap<>();
        instance.put(ATTRIBUTE.getName(), TRAINING_FREQUENCY.getAttributeValue());
        Long mEstimate = 3L;

        TrainingFrequency trainingFrequency = TrainingFrequencyBuilder.defaultTrainingFrequency()
                .withFrequency(0)
                .build();

        when(attributeRepository.findOneByName(anyString())).thenReturn(ATTRIBUTE);
        when(trainingFrequencyRepository.findOneWithTargetValueAndAttributeNameAndAttributeValue(anyString(),
                anyString(), any())).thenReturn(trainingFrequency);
        when(attributeValueRepository.getNumberOfAttributeValues(anyLong())).thenReturn(mEstimate);

        Float expectedLikelihoodProbability = 1F / (TARGET_YES_FREQUENCY + mEstimate);

        // Execution
        Float actualLikelihoodProbability = service.calculateLikelihoodProbabilityForClass(TARGET_VALUE_YES,
                TARGET_YES_FREQUENCY,
                instance);

        // Verification
        Assert.assertEquals(expectedLikelihoodProbability, actualLikelihoodProbability);
    }
}
