package ro.ubbcluj.cs.nbc.model;

import java.io.Serializable;

/**
 * Created by ramona on 14.05.2017.
 */
public class LikelihoodProbabilityId implements Serializable {

    private Long word;

    private Long targetClass;

    public LikelihoodProbabilityId() {
    }

    public Long getWord() {
        return word;
    }

    public void setWord(Long word) {
        this.word = word;
    }

    public Long getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(Long targetClass) {
        this.targetClass = targetClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LikelihoodProbabilityId that = (LikelihoodProbabilityId) o;

        if (word != null ? !word.equals(that.word) : that.word != null) return false;
        return targetClass != null ? targetClass.equals(that.targetClass) : that.targetClass == null;
    }

    @Override
    public int hashCode() {
        int result = word != null ? word.hashCode() : 0;
        result = 31 * result + (targetClass != null ? targetClass.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LikelihoodProbabilityId{" +
                "word=" + word +
                ", targetClass=" + targetClass +
                '}';
    }
}
