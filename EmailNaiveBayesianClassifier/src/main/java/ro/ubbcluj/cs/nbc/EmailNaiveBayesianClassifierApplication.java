package ro.ubbcluj.cs.nbc;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbc.service.classifier.AbstractNaiveBayesClassificationService;
import ro.ubbcluj.cs.nbc.service.learning.AbstractNaiveBayesLearningService;
import ro.ubbcluj.cs.nbc.service.vocabulary.AbstractVocabularyCollectorService;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ramona on 14.05.2017.
 */
public class EmailNaiveBayesianClassifierApplication {

    private AbstractVocabularyCollectorService englishVocabularyCollectorService;

    private AbstractNaiveBayesLearningService englishNaiveBayesLearningService;

    private AbstractNaiveBayesClassificationService englishNaiveBayesClassificationService;

    public EmailNaiveBayesianClassifierApplication() throws MessagingException, ReaderException, ServiceException, FileNotFoundException {
        setUpClassifier();
    }

    private void setUpClassifier() throws MessagingException, ReaderException, ServiceException, FileNotFoundException {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

        englishVocabularyCollectorService = (AbstractVocabularyCollectorService)
                context.getBean("englishVocabularyCollectorService");

        englishNaiveBayesLearningService = (AbstractNaiveBayesLearningService)
                context.getBean("englishNaiveBayesLearningService");

        englishNaiveBayesClassificationService = (AbstractNaiveBayesClassificationService)
                context.getBean("englishNaiveBayesClassificationService");

        englishVocabularyCollectorService.startBuildingVocabulary();
        englishNaiveBayesLearningService.startTrainingNaiveBayesClassifier();
    }


    public String classifyNewInstance(DataSetEntry dataSetEntry) throws MessagingException, FileNotFoundException,
            ServiceException, ReaderException {
        return englishNaiveBayesClassificationService.startPredictingTargetClassForDataSetEntry(dataSetEntry);
    }

    public List<String> getAvailableTargetValues() throws ServiceException {
        return englishNaiveBayesClassificationService.determineTargetClassesForVocabulary();
    }
}
