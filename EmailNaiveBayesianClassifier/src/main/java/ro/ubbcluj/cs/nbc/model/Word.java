package ro.ubbcluj.cs.nbc.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @OneToMany(mappedBy = "word")
    private List<WordOccurrence> wordOccurrences;

    @OneToMany(mappedBy = "word")
    private List<LikelihoodProbability> likelihoodProbabilities;

    @ManyToOne(fetch = FetchType.EAGER)
    private Vocabulary vocabulary;

    public Word() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WordOccurrence> getWordOccurrences() {
        return wordOccurrences;
    }

    public void setWordOccurrences(List<WordOccurrence> wordOccurrences) {
        this.wordOccurrences = wordOccurrences;
    }

    public List<LikelihoodProbability> getLikelihoodProbabilities() {
        return likelihoodProbabilities;
    }

    public void setLikelihoodProbabilities(List<LikelihoodProbability> likelihoodProbabilities) {
        this.likelihoodProbabilities = likelihoodProbabilities;
    }

    public Vocabulary getVocabulary() {
        return vocabulary;
    }

    public void setVocabulary(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (id != null ? !id.equals(word.id) : word.id != null) return false;
        return name != null ? name.equals(word.name) : word.name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Word{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
