package ro.ubbcluj.cs.nbc.repository.likelihoodprobability;

import ro.ubbcluj.cs.nbc.model.LikelihoodProbability;

import java.util.Optional;

/**
 * Created by sabau on 14/06/2017.
 */
public interface LikelihoodProbabilityDao {
    Optional<LikelihoodProbability> findOneWithWordAndTargetClass(String word, String targetClass);

    LikelihoodProbability save(LikelihoodProbability likelihoodProbability);
}
