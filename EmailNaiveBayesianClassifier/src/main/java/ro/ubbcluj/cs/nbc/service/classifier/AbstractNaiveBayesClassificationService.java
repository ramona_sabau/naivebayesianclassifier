package ro.ubbcluj.cs.nbc.service.classifier;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.common.io.reader.AbstractFileReader;
import ro.ubbcluj.cs.common.io.reader.AbstractEmlReader;
import ro.ubbcluj.cs.common.processor.LemmatizerProcessor;
import ro.ubbcluj.cs.common.processor.Processor;
import ro.ubbcluj.cs.common.utils.text.TextUtils;
import ro.ubbcluj.cs.nbc.model.EmailMessage;
import ro.ubbcluj.cs.nbc.model.LikelihoodProbability;
import ro.ubbcluj.cs.nbc.model.PriorProbability;
import ro.ubbcluj.cs.nbc.model.Word;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbc.repository.likelihoodprobability.LikelihoodProbabilityDao;
import ro.ubbcluj.cs.nbc.repository.priorprobability.PriorProbabilityRepository;
import ro.ubbcluj.cs.nbc.repository.targetclass.TargetClassDao;
import ro.ubbcluj.cs.nbc.repository.word.WordDao;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static java.lang.Math.abs;

public abstract class AbstractNaiveBayesClassificationService {

    @Autowired
    private AbstractFileReader<DataSetEntry> dataSetEntryCsvReader;

    @Autowired
    private AbstractFileReader<String> contentFileReader;

    @Autowired
    protected WordDao wordDao;

    @Autowired
    protected LikelihoodProbabilityDao likelihoodProbabilityDao;

    @Autowired
    protected PriorProbabilityRepository priorProbabilityRepository;

    @Autowired
    protected TargetClassDao targetClassDao;

    public String startPredictingTargetClassForDataSetEntry(DataSetEntry dataSetEntry) throws MessagingException,
            FileNotFoundException, ServiceException, ReaderException {
        getLogger().info("Start predicting target class for given data set entry " + dataSetEntry);

        boolean areWordsImported = importWordsFromDocument(dataSetEntry);
        if (areWordsImported) {
            clearIrrelevantWordsForClassification(dataSetEntry);
            HashMap<String, Long> groupsOfWordsWithOccurrences = groupWords(dataSetEntry.getListOfWords());
            List<String> targetClasses = determineTargetClassesForVocabulary();
            HashMap<String, Float> likelihoodProbabilities = determineLikelihoodProbabilities(groupsOfWordsWithOccurrences,
                    targetClasses);
            HashMap<String, Float> priorProbabilities = determinePriorProbabilities(targetClasses);

            String targetClass = determineMaxProbability(likelihoodProbabilities, priorProbabilities);
            getLogger().info("Target class found for document " + dataSetEntry.getEmailFileName() + " is " + targetClass);

            return targetClass;
        } else {
            getLogger().warn("Document " + dataSetEntry.getEmailPath() + dataSetEntry.getEmailFileName() + " is ignored. " +
                    "Document content could not be extracted");
            return null;
        }
    }

    private boolean importWordsFromDocument(DataSetEntry dataSetEntry) throws FileNotFoundException,
            ReaderException, MessagingException, ServiceException {
        getLogger().info("Start importing words for each document form testing set");

        Processor<String, List<String>> emailContentProcessor = new LemmatizerProcessor();

        List<String> messageContent = contentFileReader.readFromFile(
                dataSetEntry.getEmailPath() + dataSetEntry.getEmailFileName());

        if (messageContent == null || messageContent.isEmpty()) {
            String errorMessage = "E-mail could not be extracted from eml file " + dataSetEntry.getEmailPath() +
                    dataSetEntry.getEmailFileName();
            getLogger().info(errorMessage);
            return false;
        }

        StringBuilder sb = new StringBuilder();
        final String separator = System.getProperty("line.separator");
        messageContent.forEach(line -> sb.append(line).append(separator));

        List<String> wordsFromEmailContent = emailContentProcessor.process(sb.toString());
        wordsFromEmailContent = TextUtils.removePunctuationMarks(wordsFromEmailContent);
        dataSetEntry.setListOfWords(wordsFromEmailContent);
        return true;
    }

    private void clearIrrelevantWordsForClassification(DataSetEntry dataSetEntry) {
        getLogger().info("Start clearing all words that are not present in the vocabulary");

        List<String> words = dataSetEntry.getListOfWords();
        List<String> remainingWords = new ArrayList<>();

        for (String word : words) {
            word = word.toUpperCase();

            if (!isCommonWordForVocabulary(word)) {
                Optional<Word> foundWord = wordDao.findOneByName(word);
                if (foundWord.isPresent()) {
                    remainingWords.add(word);
                } else {
                    getLogger().info(word + " is being removed from the list with relevant words");
                }
            }
        }

        dataSetEntry.setListOfWords(remainingWords);
    }

    private HashMap<String, Long> groupWords(List<String> listOfWords) {
        getLogger().info("Start making groups of words");

        HashMap<String, Long> groupsOfWordsWithOccurrences = new HashMap<>();
        for (String wordFromDocument : listOfWords) {

            Long existingWordOccurrence = groupsOfWordsWithOccurrences.get(wordFromDocument);
            if (existingWordOccurrence == null) {
                groupsOfWordsWithOccurrences.put(wordFromDocument, 1L);
            } else {
                groupsOfWordsWithOccurrences.put(wordFromDocument, existingWordOccurrence + 1);
            }
        }
        return groupsOfWordsWithOccurrences;
    }

    private HashMap<String, Float> determineLikelihoodProbabilities(HashMap<String, Long> groupsOfWordsWithOccurrences,
                                                                    List<String> targetClasses) {
        getLogger().info("Start calculating likelihood probabilities");

        HashMap<String, Float> likelihoodProbabilities = new HashMap<>();
        List<String> words = new ArrayList<>(groupsOfWordsWithOccurrences.keySet());
        for (String targetClass : targetClasses) {
            Float finalLikelihoodProbability = 0F;
            for (String word : words) {
                Optional<LikelihoodProbability> likelihoodProbability =
                        likelihoodProbabilityDao.findOneWithWordAndTargetClass(word, targetClass);
                if (likelihoodProbability.isPresent() &&
                        !likelihoodProbability.get().getLikelihoodProbability().equals(0F)) {
                    Long occurrence = groupsOfWordsWithOccurrences.get(word);
                    finalLikelihoodProbability = finalLikelihoodProbability +
                            Float.valueOf(String.valueOf(Math.log(likelihoodProbability.get().getLikelihoodProbability()))) * occurrence;
                }
            }
            likelihoodProbabilities.put(targetClass, finalLikelihoodProbability);
        }

        return likelihoodProbabilities;
    }

    private HashMap<String, Float> determinePriorProbabilities(List<String> targetClasses) {
        getLogger().info("Start calculating prior probabilities");

        HashMap<String, Float> priorProbabilities = new HashMap<>();
        for (String targetClass : targetClasses) {
            Optional<PriorProbability> probability = priorProbabilityRepository.findPriorProbabilityForTargetClass(targetClass);
            if (probability.isPresent()) {
                priorProbabilities.put(targetClass, probability.get().getPriorProbability());
            }
        }
        return priorProbabilities;
    }

    private String determineMaxProbability(HashMap<String, Float> likelihoodProbabilities,
                                          HashMap<String, Float> classPriorProbabilities) {
        getLogger().info("Determine maximum probability");
        Float maxProbability = null;
        String targetValueFound = null;
        for (String targetValue : likelihoodProbabilities.keySet()) {
            Float likelihoodProbability = likelihoodProbabilities.get(targetValue);
            Float classPriorProbability = classPriorProbabilities.get(targetValue);
            Float probability = abs(likelihoodProbability + Float.valueOf(String.valueOf(Math.log(classPriorProbability))));

            if (maxProbability == null) {
                maxProbability = probability;
                targetValueFound = targetValue;
            } else {
                if (probability > maxProbability) {
                    maxProbability = probability;
                    targetValueFound = targetValue;
                }
            }
        }

        return targetValueFound;
    }

    protected abstract Logger getLogger();

    public abstract List<String> determineTargetClassesForVocabulary() throws ServiceException;

    protected abstract boolean isCommonWordForVocabulary(String word);
}
