package ro.ubbcluj.cs.nbc.repository.document;

import ro.ubbcluj.cs.nbc.model.Document;
import ro.ubbcluj.cs.nbc.model.TargetClass;
import ro.ubbcluj.cs.nbc.model.WordOccurrence;

import java.util.List;

public interface DocumentDao {

    Document findOneById(Long id);

    Document save(Document document,
                  List<WordOccurrence> wordOccurrenceList,
                  TargetClass targetClass);

    Long countAll();

    Long countAllWithTargetClass(Long targetClassId);
}
