package ro.ubbcluj.cs.nbc.repository.word;

import ro.ubbcluj.cs.nbc.model.Word;
import ro.ubbcluj.cs.nbc.model.dto.WordWithFrequency;

import java.util.List;
import java.util.Optional;

/**
 * Created by ramona on 15.05.2017.
 */
public interface WordDao {

    List<Word> findAllFromVocabularyWithTargetClass(String vocabulary, String targetClass);

    Optional<Word> findOneByName(String name);

    List<WordWithFrequency> findAllWordsWithFrequencies();

    Long countAllFromVocabulary(String vocabulary);

    Long countAllFromDocumentsWithTargetClass(String targetClass);

    Long countWordAppearancesFromDocumentsWithTargetClass(String targetClass, String word);

    void delete(Long id);
}
