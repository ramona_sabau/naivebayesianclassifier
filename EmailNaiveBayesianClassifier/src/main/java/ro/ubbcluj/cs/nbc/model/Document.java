package ro.ubbcluj.cs.nbc.model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by ramona on 14.05.2017.
 */
@Entity
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column
    private String path;

    @OneToMany(mappedBy = "document", fetch = FetchType.LAZY,
        cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    private List<WordOccurrence> wordOccurrences;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private TargetClass targetClass;

    public Document() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<WordOccurrence> getWordOccurrences() {
        return wordOccurrences;
    }

    public void setWordOccurrences(List<WordOccurrence> wordOccurrences) {
        this.wordOccurrences = wordOccurrences;
    }

    public TargetClass getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(TargetClass targetClass) {
        this.targetClass = targetClass;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (id != null ? !id.equals(document.id) : document.id != null) return false;
        if (name != null ? !name.equals(document.name) : document.name != null) return false;
        return path != null ? path.equals(document.path) : document.path == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
