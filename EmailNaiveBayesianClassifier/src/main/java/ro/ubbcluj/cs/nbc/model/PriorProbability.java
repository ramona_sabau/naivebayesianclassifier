package ro.ubbcluj.cs.nbc.model;

/**
 * Created by sabau on 14/06/2017.
 */
public class PriorProbability {

    private String targetClass;

    private Float priorProbability;

    public PriorProbability() {
    }

    public String getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(String targetClass) {
        this.targetClass = targetClass;
    }

    public Float getPriorProbability() {
        return priorProbability;
    }

    public void setPriorProbability(Float priorProbability) {
        this.priorProbability = priorProbability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriorProbability that = (PriorProbability) o;

        if (targetClass != null ? !targetClass.equals(that.targetClass) : that.targetClass != null) return false;
        return priorProbability != null ? priorProbability.equals(that.priorProbability) : that.priorProbability == null;

    }

    @Override
    public int hashCode() {
        int result = targetClass != null ? targetClass.hashCode() : 0;
        result = 31 * result + (priorProbability != null ? priorProbability.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PriorProbability{" +
                "targetClass='" + targetClass + '\'' +
                ", priorProbability=" + priorProbability +
                '}';
    }
}
