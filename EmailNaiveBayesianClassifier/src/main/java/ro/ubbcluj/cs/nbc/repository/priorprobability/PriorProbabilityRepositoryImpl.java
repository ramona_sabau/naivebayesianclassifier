package ro.ubbcluj.cs.nbc.repository.priorprobability;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.model.PriorProbability;

import java.util.HashMap;
import java.util.Optional;

/**
 * Created by sabau on 14/06/2017.
 */
public class PriorProbabilityRepositoryImpl implements PriorProbabilityRepository {

    private static final Logger logger = LoggerFactory.getLogger(PriorProbabilityRepositoryImpl.class);

    private HashMap<String, PriorProbability> priorProbabilities = new HashMap<>();

    public PriorProbabilityRepositoryImpl() {
    }

    @Override
    public void save(PriorProbability priorProbability) {
        logger.info("Start adding prior probability " + priorProbability);
        if (priorProbability.getTargetClass() == null) {
            logger.info("Prior probability cannot be added because target class is null");
            return;
        }

        priorProbabilities.put(priorProbability.getTargetClass(), priorProbability);
    }

    @Override
    public Optional<PriorProbability> findPriorProbabilityForTargetClass(String targetClass) {
        logger.info("Start finding prior probability for target class " + targetClass);

        PriorProbability priorProbability = priorProbabilities.get(targetClass);
        if (priorProbability == null) {
            return Optional.empty();
        } else {
            return Optional.of(priorProbability);
        }
    }
}
