package ro.ubbcluj.cs.nbc.model;

import java.util.List;

public class EmailMessage {

    private String sender;

    private List<String> receivers;

    private String subject;

    private String content;

    public EmailMessage() {
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public List<String> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<String> receivers) {
        this.receivers = receivers;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        receivers.forEach(receiver -> stringBuilder.append(receiver).append(";"));
        return "EmailMessage{" +
                "sender='" + sender + '\'' +
                ", receivers='" + stringBuilder.toString() + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
