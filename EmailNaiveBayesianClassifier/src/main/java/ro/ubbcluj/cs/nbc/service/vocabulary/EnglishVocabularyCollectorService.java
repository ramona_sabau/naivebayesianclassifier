package ro.ubbcluj.cs.nbc.service.vocabulary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbc.model.EnglishCommonWord;
import ro.ubbcluj.cs.nbc.model.ExistingVocabulary;
import ro.ubbcluj.cs.nbc.model.Vocabulary;
import ro.ubbcluj.cs.nbc.model.dto.WordWithFrequency;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EnglishVocabularyCollectorService
        extends AbstractVocabularyCollectorService {

    private static final Logger logger = LoggerFactory.getLogger(EnglishVocabularyCollectorService.class);

    private static final String ENGLISH_VOCABULARY = ExistingVocabulary.ENGLISH.toString();

    @Override
    protected boolean isCommonWordForVocabulary(String word) {
        logger.info("Find if word " + word + " is common word in english vocabulary");

        try {
            String existingWord = EnglishCommonWord.valueOf(word).toString();
            logger.info("Word " + word + " is not saved in the document because it is a common word");
            return true;
        } catch (IllegalArgumentException e) {
            logger.info("Word " + word + " is not a common word");
            return false;
        }
    }

    @Override
    protected Vocabulary createVocabularyForLanguage() {
        Optional<Vocabulary> foundVocabulary = vocabularyDao.findOneByName(ENGLISH_VOCABULARY);
        Vocabulary vocabulary;
        if (!foundVocabulary.isPresent()) {
            logger.warn("No vocabulary found");

            Vocabulary newVocabulary = new Vocabulary();
            newVocabulary.setName(ENGLISH_VOCABULARY);
            vocabulary = vocabularyDao.save(newVocabulary);
        } else {
            vocabulary = foundVocabulary.get();
        }

        return vocabulary;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }
}
