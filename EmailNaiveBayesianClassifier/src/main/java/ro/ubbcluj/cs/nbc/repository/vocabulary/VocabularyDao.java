package ro.ubbcluj.cs.nbc.repository.vocabulary;

import ro.ubbcluj.cs.nbc.model.Vocabulary;

import java.util.Optional;

/**
 * Created by ramona on 15.05.2017.
 */
public interface VocabularyDao {
    Optional<Vocabulary> findOneByName(String name);

    Vocabulary save(Vocabulary vocabulary);
}
