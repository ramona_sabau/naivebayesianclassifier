package ro.ubbcluj.cs.nbc;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ro.ubbcluj.cs.common.configuration.db.HibernateConfiguration;
import ro.ubbcluj.cs.common.io.reader.AbstractFileReader;
import ro.ubbcluj.cs.common.io.reader.AbstractEmlReader;
import ro.ubbcluj.cs.nbc.common.io.reader.ContentFileReader;
import ro.ubbcluj.cs.nbc.common.io.reader.DataSetEntryCsvReader;
import ro.ubbcluj.cs.nbc.common.io.reader.EmailMessageEmlReader;
import ro.ubbcluj.cs.nbc.model.EmailMessage;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbc.repository.priorprobability.PriorProbabilityRepository;
import ro.ubbcluj.cs.nbc.repository.priorprobability.PriorProbabilityRepositoryImpl;
import ro.ubbcluj.cs.nbc.service.classifier.AbstractNaiveBayesClassificationService;
import ro.ubbcluj.cs.nbc.service.classifier.EnglishNaiveBayesClassificatorService;
import ro.ubbcluj.cs.nbc.service.learning.AbstractNaiveBayesLearningService;
import ro.ubbcluj.cs.nbc.service.learning.EnglishNaiveBayesLearningService;
import ro.ubbcluj.cs.nbc.service.vocabulary.AbstractVocabularyCollectorService;
import ro.ubbcluj.cs.nbc.service.vocabulary.EnglishVocabularyCollectorService;

/**
 * Created by ramona on 14.05.2017.
 */
@Configuration
@ComponentScan(basePackages = "ro.ubbcluj.cs")
@Import(HibernateConfiguration.class)
public class ApplicationConfiguration {

    @Bean
    public PriorProbabilityRepository priorProbabilityRepository() {
        return new PriorProbabilityRepositoryImpl();
    }

    @Bean
    public AbstractVocabularyCollectorService englishVocabularyCollectorService() {
        return new EnglishVocabularyCollectorService();
    }

    @Bean
    public AbstractFileReader<DataSetEntry> dataSetEntryCsvReader() {
        return new DataSetEntryCsvReader();
    }

    @Bean
    public AbstractFileReader<String> contentFileReader() {
        return new ContentFileReader();
    }

    @Bean
    public AbstractEmlReader<EmailMessage> emailMessageEmlReader() {
        return new EmailMessageEmlReader();
    }

    @Bean
    public AbstractNaiveBayesLearningService englishNaiveBayesLearningService() {
        return new EnglishNaiveBayesLearningService();
    }

    @Bean
    public AbstractNaiveBayesClassificationService englishNaiveBayesClassificationService() {
        return new EnglishNaiveBayesClassificatorService();
    }
}
