package ro.ubbcluj.cs.nbc.model.dto;

import java.util.List;

public class DataSetEntry {

    private String emailFileName;

    private String emailPath;

    private String targetClass;

    private List<String> listOfWords;

    public DataSetEntry() {
    }

    public String getEmailFileName() {
        return emailFileName;
    }

    public void setEmailFileName(String emailFileName) {
        this.emailFileName = emailFileName;
    }

    public String getEmailPath() {
        return emailPath;
    }

    public void setEmailPath(String emailPath) {
        this.emailPath = emailPath;
    }

    public String getTargetClass() {
        return targetClass;
    }

    public void setTargetClass(String targetClass) {
        this.targetClass = targetClass;
    }

    public List<String> getListOfWords() {
        return listOfWords;
    }

    public void setListOfWords(List<String> listOfWords) {
        this.listOfWords = listOfWords;
    }

    @Override
    public String toString() {
        return "DataSetEntry{" +
                "emailFileName='" + emailFileName + '\'' +
                ", emailPath='" + emailPath + '\'' +
                ", targetClass='" + targetClass + '\'' +
                '}';
    }
}
