package ro.ubbcluj.cs.nbctest.persistance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.nbctest.model.ConfusionMatrixCell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramona on 20.04.2017.
 */
public class ConfusionMatrixRepository {

    private static final Logger log = LoggerFactory.getLogger(ConfusionMatrixRepository.class);

    private List<ConfusionMatrixCell> confusionMatrix;

    public ConfusionMatrixRepository() {
        this.confusionMatrix = new ArrayList<>();
    }

    public List<ConfusionMatrixCell> getConfusionMatrix() {
        log.info("Start fetching confusion matrix");
        return confusionMatrix;
    }

    public void save(ConfusionMatrixCell confusionMatrixCell) {
        log.info("Start saving a new confusion matrix cell");
        confusionMatrix.add(confusionMatrixCell);
    }

    public ConfusionMatrixCell findOneWithActualAndPrediction(String actual, String prediction) {
        log.info("Start finding confusion matrix cell with actual = " + actual +
                " and precision = " + prediction);
        return confusionMatrix.stream()
                .filter(confusionMatrixCell -> confusionMatrixCell.getActual().equals(actual) &&
                        confusionMatrixCell.getPrediction().equals(prediction))
                .findFirst()
                .orElse(null);
    }
}
