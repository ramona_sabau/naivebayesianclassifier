package ro.ubbcluj.cs.nbctest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ro.ubbcluj.cs.nbc.ApplicationConfiguration;
import ro.ubbcluj.cs.nbc.service.classifier.AbstractNaiveBayesClassificationService;
import ro.ubbcluj.cs.nbc.service.learning.AbstractNaiveBayesLearningService;
import ro.ubbcluj.cs.nbc.service.vocabulary.AbstractVocabularyCollectorService;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;
import ro.ubbcluj.cs.nbctest.service.ClassificationMetricsService;
import ro.ubbcluj.cs.nbctest.service.DataSetSplitterService;
import ro.ubbcluj.cs.nbctest.service.ReportEntriesWriterService;
import ro.ubbcluj.cs.nbctest.service.TestingSetEntryReaderService;

/**
 * Created by ramona on 19.04.2017.
 */
public class EmailNaiveBayesianClassifierTestApplication {

    private static final Logger logger = LoggerFactory.getLogger(EmailNaiveBayesianClassifierTestApplication.class);

    private TestingSetEntryReaderService testingSetEntryReaderService;

    private DataSetSplitterService dataSetSplitterService;

    private ClassificationMetricsService classificationMetricsService;

    private ReportEntriesWriterService reportEntriesWriterService;

    public static void main(String[] args) {
        EmailNaiveBayesianClassifierTestApplication testApplication = new EmailNaiveBayesianClassifierTestApplication();
        testApplication.setUp();
        testApplication.startTestingNaiveBayesianClassifier();
    }

    private void setUp() {
        AbstractApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);

        testingSetEntryReaderService = (TestingSetEntryReaderService) context.getBean("testingSetEntryReaderService");

        dataSetSplitterService = (DataSetSplitterService) context.getBean("dataSetSplitterService");

        classificationMetricsService = (ClassificationMetricsService) context.getBean("classificationMetricsService");

        reportEntriesWriterService = (ReportEntriesWriterService) context.getBean("reportEntriesWriterService");
    }

    public void startTestingNaiveBayesianClassifier() {

        logger.info("Start testing k partitions of initial data set");
        try {
            int kCrossValidationNumber = 10;
//            dataSetSplitterService.prepareTrainingAndTestingDataSets(kCrossValidationNumber);

//            for (int i = 1; i <= kCrossValidationNumber; i++) {
            classificationMetricsService.generateReportForTestPartition(10);
            reportEntriesWriterService.writeReport(10);
//        }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
