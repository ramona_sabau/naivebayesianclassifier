package ro.ubbcluj.cs.nbctest.persistance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramona on 20.04.2017.
 */
public class ReportEntriesRepository {

    private static final Logger log = LoggerFactory.getLogger(ReportEntriesRepository.class);

    private List<String> report;

    public ReportEntriesRepository() {
        this.report = new ArrayList<>();
    }

    public List<String> getReport() {
        log.info("Start fetching report data");
        return report;
    }

    public void save(String line) {
        log.info("Start adding a new line in the report");
        report.add(line);
    }
}
