package ro.ubbcluj.cs.nbctest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubbcluj.cs.common.configuration.properties.NBCConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by ramona on 27.04.2017.
 */
public class TestingSetEntryReaderService {

    private static final Logger logger = LoggerFactory.getLogger(TestingSetEntryReaderService.class);

    @Autowired
    private Reader<DataSetEntry> dataSetEntryReader;

    public TestingSetEntryReaderService() {
    }

    public List<DataSetEntry> importInstances() throws ReaderException, ServiceException,
            FileNotFoundException, MessagingException {
        logger.info("Start importing data set entries from testing set");
        String filename = NBCConfigurationManager.getTestingSetFilePath();
        List<DataSetEntry> testingEntries = dataSetEntryReader.readFromFile(filename);
        if (testingEntries == null || testingEntries.isEmpty()) {
            String errorMessage = "The file = " + filename + " does not contain any entries in the testing set";
            logger.warn(errorMessage);
            throw new ServiceException(errorMessage);
        }

        return testingEntries;
    }
}
