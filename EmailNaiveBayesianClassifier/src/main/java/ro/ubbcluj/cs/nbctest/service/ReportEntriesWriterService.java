package ro.ubbcluj.cs.nbctest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubbcluj.cs.common.configuration.properties.NBCConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.io.writer.Writer;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;

public class ReportEntriesWriterService {

    private static final Logger logger = LoggerFactory.getLogger(ReportEntriesWriterService.class);

    @Autowired
    private ReportEntriesRepository reportEntriesRepository;

    @Autowired
    private Writer<String> reportWriter;

    public ReportEntriesWriterService() {
    }

    public void writeReport(int partitionNumber) throws WriterException {
        logger.info("Start writing report for test partition " + partitionNumber);
        String filename = NBCConfigurationManager.getReportFilePath();
        reportWriter.writeInFile(filename, reportEntriesRepository.getReport(), true);
    }
}
