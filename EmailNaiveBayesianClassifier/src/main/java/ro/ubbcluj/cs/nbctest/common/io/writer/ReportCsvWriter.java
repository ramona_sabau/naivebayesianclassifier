package ro.ubbcluj.cs.nbctest.common.io.writer;

import ro.ubbcluj.cs.common.io.writer.AbstractCsvWriter;

/**
 * Created by ramona on 20.04.2017.
 */
public class ReportCsvWriter extends AbstractCsvWriter<String> {

    public ReportCsvWriter() {
        super();
    }

    @Override
    public String convertEntityToString(String entity) {
        return entity;
    }

    @Override
    public String getSeparator() {
        return ",";
    }
}
