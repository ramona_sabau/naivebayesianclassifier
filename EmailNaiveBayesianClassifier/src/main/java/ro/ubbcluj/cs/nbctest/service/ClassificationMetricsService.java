package ro.ubbcluj.cs.nbctest.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubbcluj.cs.common.configuration.properties.NBCConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.exception.service.ServiceException;
import ro.ubbcluj.cs.nbc.EmailNaiveBayesianClassifierApplication;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbctest.model.ConfusionMatrixCell;
import ro.ubbcluj.cs.nbctest.persistance.ConfusionMatrixRepository;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramona on 19.04.2017.
 */
public class ClassificationMetricsService {

    private static final Logger logger = LoggerFactory.getLogger(ClassificationMetricsService.class);

    @Autowired
    private TestingSetEntryReaderService testingSetEntryReaderService;

    @Autowired
    private ConfusionMatrixRepository confusionMatrixRepository;

    @Autowired
    private ReportEntriesRepository reportEntriesRepository;

    private EmailNaiveBayesianClassifierApplication application;

    public ClassificationMetricsService() {
    }

    public void generateReportForTestPartition(int partitionNumber) throws ServiceException,
            ReaderException, WriterException, FileNotFoundException, MessagingException {
        logger.info("Start generating report for current partition");
        setUpNaiveBayesianClassifier(partitionNumber);
        List<DataSetEntry> instances = prepareTestInstances();
        List<String> targetClassValues = application.getAvailableTargetValues();
        generateConfusionMatrix(targetClassValues);
        startCalculatingConfusionMatrixForTestPartition(instances);
        createReportForTestPartition(targetClassValues);
    }

    private void setUpNaiveBayesianClassifier(int partitionIdentifier) throws ServiceException, ReaderException, FileNotFoundException, MessagingException {
        logger.info("Start making configurations for Naive Bayesian Classifier");

        String dataSetPath = NBCConfigurationManager.getDataSetPath();
        String fileFormat = NBCConfigurationManager.getDataSetFileFormat();

        logger.info("Set filename for training set");
        NBCConfigurationManager.setTrainingSetFilename("trainingSet" + partitionIdentifier + fileFormat);

        logger.info("Set filename for testing set");
        NBCConfigurationManager.setTestingSetFilename("testingSet" + partitionIdentifier + fileFormat);

        logger.info("Create the file path for report file");
        NBCConfigurationManager.setReportFilename("report" + partitionIdentifier + NBCConfigurationManager.getReportFileFormat());

        logger.info("Set up the NBC application");
        this.application = new EmailNaiveBayesianClassifierApplication();
    }

    private List<DataSetEntry> prepareTestInstances() throws ReaderException, ServiceException,
            FileNotFoundException, MessagingException {
        logger.info("Start reading all test instances");
        return testingSetEntryReaderService.importInstances();
    }

    private void generateConfusionMatrix(List<String> targetClassValues) throws ServiceException {
        logger.info("Start generating cells for confusion matrix");
        for (String actual : targetClassValues) {
            for (String prediction : targetClassValues) {
                ConfusionMatrixCell cell = new ConfusionMatrixCell();
                cell.setActual(actual);
                cell.setPrediction(prediction);
                cell.setValue(0);

                confusionMatrixRepository.save(cell);
            }
        }
    }

    private void startCalculatingConfusionMatrixForTestPartition(List<DataSetEntry> instances) throws MessagingException,
            FileNotFoundException, ServiceException, ReaderException {
        logger.info("Start calculating confusion matrix for test partition");
        for (DataSetEntry instance : instances) {
            String actualTargetValue = instance.getTargetClass();
            String predictedTargetClass = application.classifyNewInstance(instance);
            if (predictedTargetClass == null) {
                logger.info("No prediction was made for instance " + instance.getEmailPath() +
                        instance.getEmailFileName());
            } else {
                if (predictedTargetClass.equals(actualTargetValue)) {
                    logger.info("Predicted class is correct. Predicted class is " + predictedTargetClass);
                } else {
                    logger.info("Predicted class is not correct. Predicted class is " + predictedTargetClass
                            + " and actual class is " + actualTargetValue);
                }

                ConfusionMatrixCell cell = confusionMatrixRepository.findOneWithActualAndPrediction(actualTargetValue,
                        predictedTargetClass);
                if (cell != null) {
                    cell.setValue(cell.getValue() + 1);
                }
            }
        }
    }

    private void createReportForTestPartition(List<String> targetClassValues) {
        logger.info("Start creating report for current partition");

        logger.info("Write confusion matrix");
        reportEntriesRepository.save("Confusion matrix");
        List<ConfusionMatrixCell> confusionMatrix = confusionMatrixRepository.getConfusionMatrix();
        confusionMatrix.forEach(confusionMatrixCell ->
                reportEntriesRepository.save(confusionMatrixCell.toString()));


        reportEntriesRepository.save(System.getProperty("line.separator") + "Accuracy");
        String accuracy = calculateAccuracy(confusionMatrix, targetClassValues);
        reportEntriesRepository.save(accuracy);

        reportEntriesRepository.save(System.getProperty("line.separator") + "Precision");
        List<String> precisions = calculatePrecisionForEachClass(confusionMatrix, targetClassValues);
        precisions.forEach(reportEntriesRepository::save);

        reportEntriesRepository.save(System.getProperty("line.separator") + "Recall");
        List<String> recallResults = calculateRecallForEachClass(confusionMatrix, targetClassValues);
        recallResults.forEach(reportEntriesRepository::save);

        reportEntriesRepository.save(System.getProperty("line.separator") + "F1 score");
        List<String> f1ScoreResults = calculateF1ScoreForEachClass(confusionMatrix, targetClassValues);
        f1ScoreResults.forEach(reportEntriesRepository::save);

    }

    public List<String> calculatePrecisionForEachClass(List<ConfusionMatrixCell> confusionMatrix, List<String> targetClassValues) {
        logger.info("Start calculating precision for each class");

        List<String> precisions = new ArrayList<>();
        for (String targetClassValue : targetClassValues) {
            Float result = calculatePrecisionForClass(confusionMatrix, targetClassValue);
            precisions.add("Precision for target class " + targetClassValue + " = " + result);
        }

        return precisions;
    }

    private Float calculatePrecisionForClass(List<ConfusionMatrixCell> confusionMatrix, String targetClass) {
        logger.info("Start calculating precision for class " + targetClass);

        Float truePositives = 0F;
        Float truePositivesAndFalsePositives = 0F;

        for (ConfusionMatrixCell cell : confusionMatrix) {
            if (targetClass.equals(cell.getActual())) {
                if (cell.getActual().equals(cell.getPrediction())) {
                    truePositives = truePositives + cell.getValue();
                }
                truePositivesAndFalsePositives = truePositivesAndFalsePositives + cell.getValue();
            }
        }

        Float result = truePositives / truePositivesAndFalsePositives;
        logger.info("Calculated precision for class " + targetClass + " is " + result);
        return result;
    }

    private String calculateAccuracy(List<ConfusionMatrixCell> confusionMatrix, List<String> targetClassValues) {
        logger.info("Start calculating accuracy");

        Float correctPredictions = 0F;
        Float allPredictions = 0F;

        for (ConfusionMatrixCell cell : confusionMatrix) {
            if (cell.getActual().equals(cell.getPrediction())) {
                correctPredictions = correctPredictions + cell.getValue();
            }
            allPredictions = allPredictions + cell.getValue();
        }

        Float result = correctPredictions / allPredictions * 100;
        return "Accuracy = " + result;
    }

    public List<String> calculateRecallForEachClass(List<ConfusionMatrixCell> confusionMatrix, List<String> targetClassValues) {
        logger.info("Start calculating recall for each class");

        List<String> recalls = new ArrayList<>();
        for (String targetClassValue : targetClassValues) {
            Float result = calculateRecallForClass(confusionMatrix, targetClassValue);
            recalls.add("Recall for target class " + targetClassValue + " = " + result);
        }

        return recalls;
    }

    private Float calculateRecallForClass(List<ConfusionMatrixCell> confusionMatrix, String targetClass) {
        logger.info("Start calculating recall for class " + targetClass);

        Float truePositives = 0F;
        Float truePositivesAndFalseNegatives = 0F;

        for (ConfusionMatrixCell cell : confusionMatrix) {
            if (targetClass.equals(cell.getPrediction())) {
                if (cell.getActual().equals(cell.getPrediction())) {
                    truePositives = truePositives + cell.getValue();
                }
                truePositivesAndFalseNegatives = truePositivesAndFalseNegatives + cell.getValue();
            }
        }

        Float result = truePositives / truePositivesAndFalseNegatives;
        logger.info("Calculated recall for class " + targetClass + " is " + result);
        return result;
    }

    public List<String> calculateF1ScoreForEachClass(List<ConfusionMatrixCell> confusionMatrix, List<String> targetClassValues) {
        logger.info("Start calculating F1 score for each class");

        List<String> f1Scores = new ArrayList<>();
        for (String targetClassValue : targetClassValues) {
            Float precision = calculatePrecisionForClass(confusionMatrix, targetClassValue);
            Float recall = calculateRecallForClass(confusionMatrix, targetClassValue);

            Float result = 2 * recall * precision / (recall + precision);
            f1Scores.add("F1 score for target class " + targetClassValue + " = " + result);
        }

        return f1Scores;
    }
}
