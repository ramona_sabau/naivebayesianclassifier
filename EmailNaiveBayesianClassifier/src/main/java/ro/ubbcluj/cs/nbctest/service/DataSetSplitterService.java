package ro.ubbcluj.cs.nbctest.service;

import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ro.ubbcluj.cs.common.configuration.properties.NBCConfigurationManager;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.common.exception.io.writer.WriterException;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.common.io.writer.Writer;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;

import javax.mail.MessagingException;
import javax.mail.search.SearchException;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.List;

/**
 * Created by ramona on 19.04.2017.
 */
public class DataSetSplitterService {

    private static final Logger logger = LoggerFactory.getLogger(DataSetSplitterService.class);

    @Autowired
    private Reader<DataSetEntry> dataSetEntryReader;

    @Autowired
    private Writer<DataSetEntry> dataSetEntryWriter;

    public DataSetSplitterService() {

    }

    public void prepareTrainingAndTestingDataSets(int numberOfPartitions) throws ReaderException, WriterException,
            FileNotFoundException, MessagingException {
        logger.info("Start importing all entries from data set");
        String dataSetFilePath = NBCConfigurationManager.getDataSetFilePath();
        String dataSetPath = NBCConfigurationManager.getDataSetPath();
        String fileFormat = NBCConfigurationManager.getDataSetFileFormat();
        List<DataSetEntry> dataSet = dataSetEntryReader.readFromFile(dataSetFilePath);

        if (dataSet == null || dataSet.isEmpty()) {
            String errorMessage = "Data set is empty";
            logger.warn(errorMessage);
            throw new SearchException(errorMessage);
        }

        Collections.shuffle(dataSet);

        int partitionSize;
        if (dataSet.size() % numberOfPartitions == 0)
            partitionSize = dataSet.size() / numberOfPartitions;
        else
            partitionSize = dataSet.size() / numberOfPartitions + 1;

        logger.info("Start splitting data set into k partitions");
        List<List<DataSetEntry>> partitions = ListUtils.partition(dataSet, partitionSize);

        int currentPartition = 1;
        for (List<DataSetEntry> partition : partitions) {

            logger.info("Start writing partition in corresponding files");
            for (int i = 1; i <= numberOfPartitions; i++) {
                if (currentPartition == i) {
                    logger.info("Start writing partition " + currentPartition + " in test set file");
                    String testingFilename = "testingSet" + currentPartition + fileFormat;
                    dataSetEntryWriter.writeInFile(dataSetPath + testingFilename,
                            partition, true);
                } else {
                    logger.info("Start writing partition " + currentPartition + " in training set number " + i);
                    String trainingFilename = "trainingSet" + i + fileFormat;
                    dataSetEntryWriter.writeInFile(dataSetPath + trainingFilename,
                            partition, false);
                }
            }

            currentPartition++;
        }
    }
}
