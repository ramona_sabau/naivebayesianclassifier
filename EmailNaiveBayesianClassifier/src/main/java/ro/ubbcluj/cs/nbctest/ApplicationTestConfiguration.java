package ro.ubbcluj.cs.nbctest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ro.ubbcluj.cs.common.io.reader.Reader;
import ro.ubbcluj.cs.common.io.writer.Writer;
import ro.ubbcluj.cs.nbc.common.io.reader.DataSetEntryCsvReader;
import ro.ubbcluj.cs.nbc.common.io.writer.DataSetEntryCsvWriter;
import ro.ubbcluj.cs.nbc.model.dto.DataSetEntry;
import ro.ubbcluj.cs.nbctest.common.io.writer.ReportCsvWriter;
import ro.ubbcluj.cs.nbctest.persistance.ConfusionMatrixRepository;
import ro.ubbcluj.cs.nbctest.persistance.ReportEntriesRepository;
import ro.ubbcluj.cs.nbctest.service.ClassificationMetricsService;
import ro.ubbcluj.cs.nbctest.service.DataSetSplitterService;
import ro.ubbcluj.cs.nbctest.service.ReportEntriesWriterService;
import ro.ubbcluj.cs.nbctest.service.TestingSetEntryReaderService;

/**
 * Created by ramona on 14.05.2017.
 */
@Configuration
@ComponentScan(basePackages = "ro.ubbcluj.cs")
public class ApplicationTestConfiguration {

    @Bean
    public ReportEntriesRepository reportEntriesRepository() {
        return new ReportEntriesRepository();
    }

    @Bean
    public ConfusionMatrixRepository confusionMatrixRepository() {
        return new ConfusionMatrixRepository();
    }

    @Bean
    public Reader<DataSetEntry> dataSetEntryReader() {
        return new DataSetEntryCsvReader();
    }

    @Bean
    public Writer<DataSetEntry> dataSetEntryWriter() {
        return new DataSetEntryCsvWriter();
    }

    @Bean
    public Writer<String> reportWriter() {
        return new ReportCsvWriter();
    }

    @Bean
    public DataSetSplitterService dataSetSplitterService() {
        return new DataSetSplitterService();
    }

    @Bean
    public TestingSetEntryReaderService testingSetEntryReaderService() {
        return new TestingSetEntryReaderService();
    }

    @Bean
    public ReportEntriesWriterService reportEntriesWriterService() {
        return new ReportEntriesWriterService();
    }

    @Bean
    public ClassificationMetricsService classificationMetricsService() {
        return new ClassificationMetricsService();
    }
}
