package ro.ubbcluj.cs.common.io.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ro.ubbcluj.cs.common.exception.io.reader.ReaderException;
import ro.ubbcluj.cs.nbc.model.EmailMessage;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ramona on 14.05.2017.
 */
public abstract class AbstractEmlReader<T> implements Reader<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractEmlReader.class);

    @Override
    public List<T> readFromFile(String fileName) throws ReaderException, FileNotFoundException, MessagingException {
        List<T> readObjects = new ArrayList<>();

        if (fileName == null) {
            throw new ReaderException("The given file name is not valid.");
        }

        fileName = fileName.trim();

        if (fileName.isEmpty()) {
            throw new ReaderException("The given file name must be provided.");
        }

        File currentFile = new File(fileName);
        if (!currentFile.exists()) {
            String errorMessage = "File " + fileName + " does not exist";
            logger.warn(errorMessage);
            throw new ReaderException(errorMessage);
        }

        InputStream source = new FileInputStream(currentFile);
        MimeMessage message = new MimeMessage(null, source);

        T readObject = createEntity(message);
        if (readObject != null) {
            readObjects.add(readObject);
        }

        return readObjects;
    }

    public abstract T createEntity(MimeMessage mimeMessage) throws ReaderException;
}
