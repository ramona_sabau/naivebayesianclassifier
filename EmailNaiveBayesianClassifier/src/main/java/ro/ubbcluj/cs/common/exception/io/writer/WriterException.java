package ro.ubbcluj.cs.common.exception.io.writer;

/**
 * Created by Ramona Sabau on 08.11.2016.
 */
public class WriterException extends Exception {
    public WriterException(Exception e) {
        super(e);
    }

    public WriterException(String message) {
        super(message);
    }

    public WriterException(String message, Throwable cause) {
        super(message, cause);
    }
}
