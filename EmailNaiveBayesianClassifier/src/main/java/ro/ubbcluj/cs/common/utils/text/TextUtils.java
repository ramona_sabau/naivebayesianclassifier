package ro.ubbcluj.cs.common.utils.text;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TextUtils {

    private static final Logger logger = LoggerFactory.getLogger(TextUtils.class);

    public static List<String> removePunctuationMarks(List<String> words) {
        logger.info("Start removing all punctuation marks from words.");
        List<String> processedWords = new ArrayList<>();
        for (String word : words) {
            String processedWord = removePunctuationMarks(word);

            if (processedWord.isEmpty()) {
                logger.info(word + " is not included because it contains only punctuation" +
                        " marks");
            } else {
                if (!processedWord.equals(word)) {
                    logger.info("Add " + processedWord + " instead of " + word);
                }
                processedWords.add(processedWord);
            }
        }

        return processedWords;
    }

    public static String removePunctuationMarks(String word) {
        final StringBuilder builder = new StringBuilder();

        for(final char character : word.toCharArray())
            if(Character.isLetterOrDigit(character))
                builder.append(character);

        return builder.toString();
    }

}
