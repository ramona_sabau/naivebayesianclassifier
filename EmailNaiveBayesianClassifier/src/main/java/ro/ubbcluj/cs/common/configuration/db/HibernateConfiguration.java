package ro.ubbcluj.cs.common.configuration.db;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by ramona on 14.05.2017.
 */
@Configuration
@EnableTransactionManagement
@PropertySource(value = "classpath:application.properties")
public class HibernateConfiguration {

    private static final String DATASOURCE_DRIVER_CLASS_NAME = "datasource.driver-class-name";
    private static final String DATASOURCE_URL = "datasource.url";
    private static final String DATASOURCE_USERNAME = "datasource.username";
    private static final String DATASOURCE_PASSWORD = "datasource.password";

    private static final String HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
    private static final String HIBERNATE_DDL_AUTO = "hibernate.hbm2ddl.auto";
    private static final String HIBERNATE_C3P0_MIN_SIZE = "hibernate.c3p0.min_size";
    private static final String HIBERNATE_C3P0_MAX_SIZE = "hibernate.c3p0.max_size";
    private static final String HIBERNATE_C3P0_TIMEOUT = "hibernate.c3p0.timeout";
    private static final String HIBERNATE_C3P0_MAX_STATEMENTS = "hibernate.c3p0.max_statements";

    @Autowired
    private Environment environment;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("ro.ubbcluj.cs.nbc.model");
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty(DATASOURCE_DRIVER_CLASS_NAME));
        dataSource.setUrl(environment.getRequiredProperty(DATASOURCE_URL));
        dataSource.setUsername(environment.getRequiredProperty(DATASOURCE_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(DATASOURCE_PASSWORD));
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put(HIBERNATE_DIALECT, environment.getRequiredProperty(HIBERNATE_DIALECT));
        properties.put(HIBERNATE_SHOW_SQL, environment.getRequiredProperty(HIBERNATE_SHOW_SQL));
        properties.put(HIBERNATE_FORMAT_SQL, environment.getRequiredProperty(HIBERNATE_FORMAT_SQL));
        properties.put(HIBERNATE_DDL_AUTO, environment.getRequiredProperty(HIBERNATE_DDL_AUTO));
        properties.put(HIBERNATE_C3P0_MIN_SIZE, environment.getRequiredProperty(HIBERNATE_C3P0_MIN_SIZE));
        properties.put(HIBERNATE_C3P0_MAX_SIZE, environment.getRequiredProperty(HIBERNATE_C3P0_MAX_SIZE));
        properties.put(HIBERNATE_C3P0_TIMEOUT, environment.getRequiredProperty(HIBERNATE_C3P0_TIMEOUT));
        properties.put(HIBERNATE_C3P0_MAX_STATEMENTS, environment.getRequiredProperty(HIBERNATE_C3P0_MAX_STATEMENTS));
        return properties;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }
}