Confusion matrix
ConfusionMatrixCell{actual='ham, prediction='ham, value=280}
ConfusionMatrixCell{actual='ham, prediction='spam, value=21}
ConfusionMatrixCell{actual='spam, prediction='ham, value=72}
ConfusionMatrixCell{actual='spam, prediction='spam, value=56}

Accuracy
Accuracy = 78.32168

Precision
Precision for target class ham = 93.02326
Precision for target class spam = 43.75

Recall
Recall for target class ham = 79.545456
Recall for target class spam = 72.72727
