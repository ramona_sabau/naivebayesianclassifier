Confusion matrix
ConfusionMatrixCell{actual='alt.atheism, prediction='alt.atheism, value=42}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='soc.religion.christian, value=9}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='alt.atheism, prediction='talk.religion.misc, value=4}
ConfusionMatrixCell{actual='comp.graphics, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.graphics, value=25}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.os.ms-windows.misc, value=4}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='comp.graphics, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.crypt, value=2}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='sci.space, value=3}
ConfusionMatrixCell{actual='comp.graphics, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='comp.graphics, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.graphics, value=3}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.os.ms-windows.misc, value=49}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.crypt, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.electronics, value=6}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='sci.space, value=2}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='comp.os.ms-windows.misc, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.graphics, value=2}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.os.ms-windows.misc, value=9}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.sys.ibm.pc.hardware, value=33}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='misc.forsale, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.crypt, value=2}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='soc.religion.christian, value=2}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.ibm.pc.hardware, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.graphics, value=5}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.os.ms-windows.misc, value=3}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.sys.mac.hardware, value=32}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='misc.forsale, value=4}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.sport.baseball, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.electronics, value=3}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='comp.sys.mac.hardware, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.graphics, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.os.ms-windows.misc, value=2}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='comp.windows.x, value=56}
ConfusionMatrixCell{actual='comp.windows.x, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='rec.sport.hockey, value=2}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.politics.guns, value=2}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='comp.windows.x, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.graphics, value=4}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.sys.ibm.pc.hardware, value=3}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='misc.forsale, value=31}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.autos, value=2}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='rec.sport.hockey, value=3}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='misc.forsale, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='misc.forsale, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.autos, value=40}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.motorcycles, value=5}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='rec.autos, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='comp.windows.x, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='misc.forsale, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.motorcycles, value=52}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='rec.motorcycles, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='misc.forsale, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.sport.baseball, value=49}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.med, value=2}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.baseball, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='misc.forsale, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.sport.baseball, value=2}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='rec.sport.hockey, value=42}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='rec.sport.hockey, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='sci.crypt, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.crypt, value=63}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.space, value=2}
ConfusionMatrixCell{actual='sci.crypt, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.politics.guns, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='sci.crypt, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.graphics, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.os.ms-windows.misc, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='comp.windows.x, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='misc.forsale, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.motorcycles, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.electronics, value=36}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.space, value=6}
ConfusionMatrixCell{actual='sci.electronics, prediction='soc.religion.christian, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.politics.guns, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='sci.electronics, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='sci.electronics, value=2}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=50}
ConfusionMatrixCell{actual='sci.med, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='sci.med, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='talk.religion.misc, value=2}
ConfusionMatrixCell{actual='sci.space, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='sci.space, value=62}
ConfusionMatrixCell{actual='sci.space, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='sci.space, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='sci.space, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='alt.atheism, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.motorcycles, value=2}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='soc.religion.christian, value=52}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='soc.religion.christian, prediction='talk.religion.misc, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.med, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='soc.religion.christian, value=2}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.politics.guns, value=63}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.politics.mideast, value=0}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.politics.misc, value=1}
ConfusionMatrixCell{actual='talk.politics.guns, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.graphics, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.sys.ibm.pc.hardware, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='sci.space, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='soc.religion.christian, value=1}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.politics.mideast, value=61}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.politics.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.mideast, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='alt.atheism, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.autos, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='rec.sport.hockey, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='soc.religion.christian, value=0}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.politics.guns, value=2}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.politics.mideast, value=2}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.politics.misc, value=37}
ConfusionMatrixCell{actual='talk.politics.misc, prediction='talk.religion.misc, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='alt.atheism, value=2}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.graphics, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.os.ms-windows.misc, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.sys.ibm.pc.hardware, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.sys.mac.hardware, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='comp.windows.x, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='misc.forsale, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.autos, value=1}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.motorcycles, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.sport.baseball, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='rec.sport.hockey, value=1}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.crypt, value=1}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='sci.space, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='soc.religion.christian, value=4}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.politics.guns, value=0}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.politics.mideast, value=1}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.politics.misc, value=2}
ConfusionMatrixCell{actual='talk.religion.misc, prediction='talk.religion.misc, value=37}

Accuracy
Accuracy = 80.922806

Precision
Precision for target class alt.atheism = 0.76363635
Precision for target class comp.graphics = 0.5813953
Precision for target class comp.os.ms-windows.misc = 0.74242425
Precision for target class comp.sys.ibm.pc.hardware = 0.6111111
Precision for target class comp.sys.mac.hardware = 0.61538464
Precision for target class comp.windows.x = 0.8115942
Precision for target class misc.forsale = 0.6888889
Precision for target class rec.autos = 0.754717
Precision for target class rec.motorcycles = 0.9122807
Precision for target class rec.sport.baseball = 0.9245283
Precision for target class rec.sport.hockey = 0.89361703
Precision for target class sci.crypt = 0.9130435
Precision for target class sci.electronics = 0.62068963
Precision for target class sci.med = 0.86206895
Precision for target class sci.space = 0.93939394
Precision for target class soc.religion.christian = 0.8965517
Precision for target class talk.politics.guns = 0.9130435
Precision for target class talk.politics.mideast = 0.93846154
Precision for target class talk.politics.misc = 0.902439
Precision for target class talk.religion.misc = 0.75510204

Recall
Recall for target class alt.atheism = 0.93333334
Recall for target class comp.graphics = 0.5813953
Recall for target class comp.os.ms-windows.misc = 0.69014084
Recall for target class comp.sys.ibm.pc.hardware = 0.80487806
Recall for target class comp.sys.mac.hardware = 1.0
Recall for target class comp.windows.x = 0.8235294
Recall for target class misc.forsale = 0.75609756
Recall for target class rec.autos = 0.8333333
Recall for target class rec.motorcycles = 0.852459
Recall for target class rec.sport.baseball = 0.9245283
Recall for target class rec.sport.hockey = 0.8235294
Recall for target class sci.crypt = 0.8630137
Recall for target class sci.electronics = 0.7058824
Recall for target class sci.med = 0.89285713
Recall for target class sci.space = 0.75609756
Recall for target class soc.religion.christian = 0.6933333
Recall for target class talk.politics.guns = 0.8630137
Recall for target class talk.politics.mideast = 0.85915494
Recall for target class talk.politics.misc = 0.8043478
Recall for target class talk.religion.misc = 0.8043478

F1 score
F1 score for target class alt.atheism = 0.84
F1 score for target class comp.graphics = 0.5813953
F1 score for target class comp.os.ms-windows.misc = 0.7153285
F1 score for target class comp.sys.ibm.pc.hardware = 0.69473684
F1 score for target class comp.sys.mac.hardware = 0.76190484
F1 score for target class comp.windows.x = 0.81751823
F1 score for target class misc.forsale = 0.7209303
F1 score for target class rec.autos = 0.79207915
F1 score for target class rec.motorcycles = 0.8813559
F1 score for target class rec.sport.baseball = 0.9245283
F1 score for target class rec.sport.hockey = 0.85714287
F1 score for target class sci.crypt = 0.8873239
F1 score for target class sci.electronics = 0.6605505
F1 score for target class sci.med = 0.877193
F1 score for target class sci.space = 0.8378378
F1 score for target class soc.religion.christian = 0.78195494
F1 score for target class talk.politics.guns = 0.8873239
F1 score for target class talk.politics.mideast = 0.89705884
F1 score for target class talk.politics.misc = 0.8505747
F1 score for target class talk.religion.misc = 0.7789473
