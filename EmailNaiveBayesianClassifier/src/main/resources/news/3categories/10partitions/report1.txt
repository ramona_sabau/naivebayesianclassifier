Confusion matrix
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.crypt, value=43}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.electronics, value=1}
ConfusionMatrixCell{actual='sci.crypt, prediction='sci.med, value=0}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.crypt, value=11}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.electronics, value=49}
ConfusionMatrixCell{actual='sci.electronics, prediction='sci.med, value=5}
ConfusionMatrixCell{actual='sci.med, prediction='sci.crypt, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='sci.electronics, value=0}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=68}

Accuracy
Accuracy = 90.39548

Precision
Precision for target class sci.crypt = 0.97727275
Precision for target class sci.electronics = 0.75384617
Precision for target class sci.med = 1.0

Recall
Recall for target class sci.crypt = 0.7962963
Recall for target class sci.electronics = 0.98
Recall for target class sci.med = 0.9315069

F1 score
F1 score for target class sci.crypt = 0.87755096
F1 score for target class sci.electronics = 0.8521739
F1 score for target class sci.med = 0.964539
