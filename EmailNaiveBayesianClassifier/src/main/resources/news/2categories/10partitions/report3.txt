Confusion matrix
ConfusionMatrixCell{actual='rec.autos, prediction='rec.autos, value=54}
ConfusionMatrixCell{actual='rec.autos, prediction='sci.med, value=5}
ConfusionMatrixCell{actual='sci.med, prediction='rec.autos, value=3}
ConfusionMatrixCell{actual='sci.med, prediction='sci.med, value=56}

Accuracy
Accuracy = 93.220345

Precision
Precision for target class rec.autos = 0.91525424
Precision for target class sci.med = 0.9491525

Recall
Recall for target class rec.autos = 0.94736844
Recall for target class sci.med = 0.91803277

F1 score
F1 score for target class rec.autos = 0.93103445
F1 score for target class sci.med = 0.93333334
